/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "API.h"
#include <boost/filesystem.hpp>

#define CHECK_ARGS(i, n, args) \
	if((i) + 1 >= (n)) \
	{ \
		Render::Warning("Unexpected end of arguments at \"%s\", ignoring.", (args)[i]); \
		break; \
	}

struct ArgOpts
{
	const char *output;
	const char *input;
	unsigned int saveInterval;
	unsigned int nPassesMax;
};

ArgOpts ReadArgs(int n, const char** args)
{
	ArgOpts argsObj = { "scene.exr", NULL, 5, UINT_MAX };
	for(int i = 1; i < n; ++i)
	{
		if(args[i][0] == '-')
		{
			if(strcmp(&args[i][1], "o") == 0)
			{
				//Output file.
				CHECK_ARGS(i, n, args)
				argsObj.output = args[++i];
			}
			else if(strcmp(&args[i][1], "i") == 0) 
			{
				//Number of intervals before saving.
				CHECK_ARGS(i, n, args)
				argsObj.saveInterval = (unsigned int)(atoll(args[++i]));

				if(argsObj.saveInterval == 0)
					argsObj.saveInterval = 5;
			}
			else if(strcmp(&args[i][1], "p") == 0)
			{
				//Number of passes.
				CHECK_ARGS(i, n, args)
				argsObj.nPassesMax =  atoll(args[++i]);
				if(argsObj.nPassesMax == 0)
					argsObj.nPassesMax = 1;
			}
			else
				Render::Warning("Ignoring unrecognized command \"%s\".", args[i]);
		}
		else
			argsObj.input = args[i]; 
	}

	if(argsObj.nPassesMax == 0)
		argsObj.nPassesMax = UINT_MAX;

	return argsObj;	
}

int main(int n, const char** args)
{
	Render::RenderInstance instance;
	memset(&Render::opts, 0, sizeof(Render::Options));

	ArgOpts argsObj = ReadArgs(n, args);

	if(argsObj.input == NULL)
	{
		printf("Usage: renderer scene_file [-o out_image] [-i save_interval] [-p max_passes]\n");
		return 0;
	}

	boost::filesystem::path startDir = boost::filesystem::current_path();
	boost::filesystem::path inputDir = boost::filesystem::path(argsObj.input[0] == '/' ? 
		argsObj.input : startDir.string() + '/' + argsObj.input).parent_path();

	try
	{
		boost::filesystem::current_path(inputDir);
		if(!argsObj.input)
		{
			printf("Usage: renderer scene_file [-o output_image] [-i save_interal] [-p passes]");	
			return 0;
		}
		instance.Parse(argsObj.input);

		if(!instance.IsValid())
			Render::Severe("Scene is not valid.", "", "", 0);
	}
	catch(int code)
	{
		exit(0);
	}

	boost::filesystem::current_path(startDir);

	Render::Renderer *r = instance.GetRenderer();

	Render::ProgressiveStatus status(r->camera->film, argsObj.output, argsObj.saveInterval, argsObj.nPassesMax);

	Render::Info("%d cores detected.", Render::NumThreads());

	r->Render(&status);
	return 0;
}
