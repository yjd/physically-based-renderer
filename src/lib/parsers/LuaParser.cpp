/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "parsers/LuaParser.h"
#include "API.h"

#define NewUserData(LuaStateName, ClassName, VarName) ClassName **VarName = (ClassName **)lua_newuserdata(LuaStateName, sizeof(ClassName *))
#define CheckUserData(LuaStateName, ClassName, Index, MetaTableString) ((ClassName **)luaL_checkudata(LuaStateName, Index, MetaTableString))

#define GETRENDERINSTANCE(L, INST) \
	{ \
		lua_getglobal((L), "_RINST"); \
		if(!lua_islightuserdata((L), -1)) \
			luaL_error((L), "The render instance was tampered with before this point."); \
		INST = ((RenderInstance *)lua_touserdata(L, -1)); \
		lua_pop(L, 1); \
	}

namespace Render
{
	extern "C"
	{
		inline bool luaL_checkbool(lua_State *L, int index)
		{
			if(!lua_isboolean( L, index ))
			{
				const char *msg =	lua_pushfstring(L, "%s expected, got %s",
									lua_typename(L, LUA_TBOOLEAN), luaL_typename(L, index));

				luaL_argerror(L, index, msg);
			}

			return lua_toboolean( L, index ) != 0;
		}
	}

	template<typename T>
	void get_tuple(lua_State* L, int arg, T* data, unsigned int n)
	{
		luaL_checktype(L, arg, LUA_TTABLE);
		char err[256];
		sprintf(err, "%d-tuple expected, got %d-tuple.", n, luaL_len(L, arg));
		luaL_argcheck(L, luaL_len(L, arg) == n, arg, err);

		for (unsigned int i = 1; i <= n; ++i)
		{
			lua_rawgeti(L, arg, i);
			data[i - 1] = (T) luaL_checknumber(L, -1);
			lua_pop(L, 1);
		}
	}

	Spectrum GetSpectrumColor(lua_State *L, int arg)
	{
		if(lua_isnumber(L, arg))
		{
			float c = float(lua_tonumber(L, arg));
			float color[3] = {c, c, c};
			return Spectrum::FromRGB(color);
		}

		if(lua_isstring(L, arg))
			return ReadSPD(lua_tostring(L, arg));

		luaL_checktype(L, arg, LUA_TTABLE);

		int n = luaL_len(L, arg);

		if(n == 3)
		{
			float color[3];
			get_tuple(L, arg, color, 3);
			return Spectrum::FromRGB(color);
		}

		luaL_argcheck(L, n % 2 == 0, arg, "An even number of elements is expected.");

		lua_rawgeti(L, arg, 1);

		if(lua_isnumber(L, -1))
		{
			float check = float(luaL_checknumber(L, -1));
			if(check < 0)
			{
				SpectrumColorType type = SpectrumColorType(int(check));
				lua_pop(L, 1);
				lua_rawgeti(L, arg, 2);
			
				float color[3];
				if(lua_isnumber(L, -1))
				{
					float c = float(luaL_checknumber(L, -1));
					color[0] = color[1] = color[2] = c;
				}
				else
					get_tuple(L, -1, color, 3);
				
				lua_pop(L, 1);

				switch(type)
				{
				case SpectrumColorType::RGB:
					return Spectrum::FromRGB(color);
				default:
					return Spectrum::FromXYZ(color);
				}
			}

			lua_pop(L, 1);
			int nSamples = n / 2;
			float *spectralSampledWavelengths = new float[nSamples];
			float *spectralPowers = new float[nSamples];

			for(int i = 0; i < nSamples; ++i)
			{
				lua_rawgeti(L, arg, 2 * i + 1);
				spectralSampledWavelengths[i] = float(luaL_checknumber(L, -1));
				lua_pop(L, 1);
				lua_rawgeti(L, arg, 2 * i + 2);
				spectralPowers[i] = float(luaL_checknumber(L, -1));
				lua_pop(L, 1);
			}

			Spectrum s = Spectrum::FromSampled(spectralSampledWavelengths, spectralPowers, nSamples);
			float rgb[3];
			s.ToRGB(rgb);

			delete[] spectralSampledWavelengths;
			delete[] spectralPowers;
			return s;
		}
		else
		{
			luaL_checktype(L, -1, LUA_TTABLE);
			int nSamples = luaL_len(L, -1);
			float *spectralSampledWavelengths = new float[nSamples];
			float *spectralPowers = new float[nSamples];
			
			get_tuple(L, -1, spectralSampledWavelengths, nSamples);
			lua_pop(L, 1);

			lua_rawgeti(L, arg, 2);
			get_tuple(L, -1, spectralPowers, nSamples);
			lua_pop(L, 1);

			Spectrum s = Spectrum::FromSampled(spectralSampledWavelengths, spectralPowers, nSamples);
			delete[] spectralSampledWavelengths;
			delete[] spectralPowers;

			return s;
		}
	}

	RSpectrumTexture GetSpectrumTexture(lua_State *L, int n, bool nullIfBlack)
	{
		RSpectrumTexture r;

		if( lua_isuserdata(L, n) )
			r = **CheckUserData(L, RSpectrumTexture, n, "SpectrumTexture");
		else if( lua_isnumber(L, n) )
		{
			int isnum;
			float intensity = (float) lua_tonumberx(L, n, &isnum);
			float rgb[3] = {intensity, intensity, intensity};

			if(!nullIfBlack || intensity > 0.f)
				r = new ConstantTexture<Spectrum>(Spectrum::FromRGB(rgb));
		}
		else if( lua_istable(L, n) )
		{
			Spectrum value = GetSpectrumColor(L, n);
			if(!nullIfBlack || !value.IsBlack())
				r = new ConstantTexture<Spectrum>(value);
		}
		else if( lua_isstring(L, n) )
		{
			Spectrum value = ReadSPD(lua_tostring(L, n));
			if(!nullIfBlack || !value.IsBlack())
				r = new ConstantTexture<Spectrum>(value);
		}
		else
		{
			string err = "Texture parameters must be a color type or texture object.";
			luaL_argerror(L, n, err.c_str());
		}

		return r;
	}

	RFloatTexture GetFloatTexture(lua_State *L, int n, bool nullIfBlack)
	{
		RFloatTexture r;

		if( lua_isuserdata(L, n) )
			r = **CheckUserData(L, RFloatTexture, n, "FloatTexture");
		else if( lua_isnumber(L, n) )
		{
			int isnum;
			float intensity = (float) lua_tonumberx(L, n, &isnum);

			if(!nullIfBlack || intensity > 0.f)
				r = new ConstantTexture<float>(intensity);
		}
		else if(lua_isstring(L, n))
		{
			float intensity = ReadSPD(lua_tostring(L, n)).y();
			if(!nullIfBlack || intensity > 0.f)
				r = new ConstantTexture<float>(intensity);
		}
		else if( lua_istable(L, n) )
		{
			Spectrum value = GetSpectrumColor(L, n);
			if(!nullIfBlack || !value.IsBlack())
				r = new ConstantTexture<float>(value.y());
		}
		else
		{
			string err = "Texture parameters must be a color type or texture object.";
			luaL_argerror(L, n, err.c_str());
		}

		return r;
	}

	//Returns true if the argument is consumed.  Otherwise returns false.
	bool GetTextureMap2D(lua_State *L, int arg, TextureMapping2D **mapping)
	{
		if(lua_isstring(L, arg) && !lua_isnumber(L, arg))
		{
			const char *str = lua_tostring(L, arg);
			if(strcmp(str, "UV") == 0)
			{
				*mapping = new UVMapping2D();
				return true;
			}
			if(strcmp(str, "SPHERICAL") == 0)
			{
				*mapping = new SphericalMapping2D(Transform());
				return true;
			}
			if(strcmp(str, "CYLINDRICAL") == 0)
			{
				*mapping = new CylindricalMapping2D(Transform());
				return true;
			}
			if(strcmp(str, "PLANAR") == 0)
			{
				*mapping = new PlanarMapping2D(Vector(1,0,0), Vector(0,1,0));
				return true;
			}

			char err[256];
			::sprintf(err, "%s is not a valid 2D texture mapping type.", str);
		
			luaL_argerror(L, arg, err);

			return false;
		}
		else if(!lua_istable(L, arg))
		{
			*mapping = new UVMapping2D();
			return false;
		}

		int length = luaL_len(L, arg);
		
		lua_rawgeti(L, arg, 1);
		if(!lua_isstring(L, -1) || lua_isnumber(L, -1))
		{
			lua_pop(L, 1);
			*mapping = new UVMapping2D();
			return false;
		}
		const char *type = luaL_checkstring(L, -1);
		lua_pop(L, 1);

		int n = 2;

		if(strcmp(type, "PLANAR") == 0)
		{
			Vector vu(1,0,0), vv(0,1,0);
			float du = 0.f, dv = 0.f;
			if(length > 1)
			{
				lua_rawgeti(L, arg, n);
				if(lua_istable(L, -1))
				{
					get_tuple(L, -1, &vu.x, 3);
					lua_pop(L, 1);
					++n;

					lua_rawgeti(L, arg, n);
					get_tuple(L, -1, &vv.x, 3);
					lua_pop(L, 1);
					++n;

					lua_rawgeti(L, arg, n);
				}

				if(length >= n)
				{
					du = float(luaL_checknumber(L, -1));
					lua_pop(L, 1);
					++n;
				}

				if(length >= n)
				{
					lua_rawgeti(L, arg, n);
					dv = float(luaL_checknumber(L, -1));
					lua_pop(L, 1);
					++n;
				}
			}
				
			*mapping = new PlanarMapping2D(vu, vv, du, dv);
			return true;
		}
		
		Transform *toTex = NULL;
		if(strcmp(type, "UV") &&
			length >= n)
		{
			lua_rawgeti(L, arg, n);
			if(lua_isuserdata(L, -1))
			{
				toTex = *CheckUserData(L, Transform, -1, "Transform");
				++n;
			}
				
			lua_pop(L, 1);
		}

		float su = 1.f, sv = 1.f, du = 0.f, dv = 0.f;

		if(length >= n)
		{
			lua_rawgeti(L, arg, n);
			su = float(luaL_checknumber(L, -1));
			lua_pop(L, 1);
		}

		if(length >= n + 1)
		{
			lua_rawgeti(L, arg, n + 1);
			sv = float(luaL_checknumber(L, -1));
			lua_pop(L, 1);
		}

		if(length >= n + 2)
		{
			lua_rawgeti(L, arg, n + 2);
			du = float(luaL_checknumber(L, -1));
			lua_pop(L, 1);
		}

		if(length >= n + 3)
		{
			lua_rawgeti(L, arg, n + 3);
			dv = float(luaL_checknumber(L, -1));
			lua_pop(L, 1);
		}

		if(strcmp(type, "UV") == 0)
		{
			*mapping = new UVMapping2D(su, sv, du, dv);
			return true;
		}

		if(strcmp(type, "SPHERICAL") == 0)
		{
			*mapping = new SphericalMapping2D(toTex ? *toTex : Transform(), su, sv, du, dv);
			return true;
		}

		if(strcmp(type, "CYLINDRICAL") == 0)
		{
			*mapping = new CylindricalMapping2D(toTex ? *toTex : Transform(), su, sv, du, dv);
			return true;
		}
		
		char err[256];
		::sprintf(err, "%s is not a valid 2D texture mapping type.", type);
		
		luaL_argerror(L, n, err);

		return NULL;
	}

	int l_FloatBilerp(lua_State *L)
	{
		int top = lua_gettop(L);
		TextureMapping2D *mapping;

		int n = GetTextureMap2D(L, 2, &mapping) ? 3 : 2;

		NewUserData(L, RFloatTexture, data);
		*data = new RFloatTexture(new BilerpTexture<float>(	mapping,
									top >= n ? float(luaL_checknumber(L, n)) : 0.f,
									top >= n + 1 ? float(luaL_checknumber(L, n + 1)) : 1.f,
									top >= n + 2 ? float(luaL_checknumber(L, n + 2)) : 0.f,
									top >= n + 3 ? float(luaL_checknumber(L, n + 3)) : 1.f));
		
		luaL_getmetatable(L, "FloatTexture");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_SpectrumBilerp(lua_State *L)
	{
		int top = lua_gettop(L);
		TextureMapping2D *mapping;

		int n = GetTextureMap2D(L, 2, &mapping) ? 3 : 2;

		NewUserData(L, RSpectrumTexture, data);
		*data = new RSpectrumTexture(new BilerpTexture<Spectrum>(	mapping,
									top >= n ? GetSpectrumColor(L, n) : 0.f,
									top >= n + 1 ? GetSpectrumColor(L, n + 1) : 1.f,
									top >= n + 2 ? GetSpectrumColor(L, n + 2) : 0.f,
									top >= n + 3 ? GetSpectrumColor(L, n + 3) : 1.f));
		
		luaL_getmetatable(L, "SpectrumTexture");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_FloatImage(lua_State *L)
	{
		int top = lua_gettop(L);
		TextureMapping2D *mapping;

		int n = GetTextureMap2D(L, 2, &mapping) ? 3 : 2;
		TexInfo *info = *CheckUserData(L, TexInfo, n, "TextureInfo");

		RenderInstance *inst;
		GETRENDERINSTANCE(L, inst);

		NewUserData(L, RFloatTexture, data);
		*data = new RFloatTexture(new ImageTexture<float, float>(	mapping, 
																	inst->GetFloatImage(*info), 
																	top >= n + 1 ? ImageWrap(luaL_checkint(L, n + 1)) : REPEAT,
																	top >= n + 2 ? ImageFilter(luaL_checkint(L, n + 2)) : BILINEAR));

		luaL_getmetatable(L, "FloatTexture");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_SpectrumImage(lua_State *L)
	{
		int top = lua_gettop(L);
		TextureMapping2D *mapping;

		int n = GetTextureMap2D(L, 2, &mapping) ? 3 : 2;
		TexInfo *info = *CheckUserData(L, TexInfo, n, "TextureInfo");

		RenderInstance *inst;
		GETRENDERINSTANCE(L, inst);

		NewUserData(L, RSpectrumTexture, data);
		*data = new RSpectrumTexture(new ImageTexture<RGBSpectrum, Spectrum>(	mapping,
																				inst->GetSpectrumImage(*info), 
																				top >= n + 1 ? ImageWrap(luaL_checkint(L, n + 1)) : REPEAT,
																				top >= n + 2 ? ImageFilter(luaL_checkint(L, n + 2)) : BILINEAR));

		luaL_getmetatable(L, "SpectrumTexture");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_SceneSetAccelType(lua_State *L)
	{
		RenderInstance *inst;
		GETRENDERINSTANCE(L, inst);

		inst->SetAccelType(AccelType(luaL_checkunsigned(L, 2)));
		
		return 0;
	}
	int l_SceneAddChildren(lua_State *L)
	{
		RenderInstance *inst;
		GETRENDERINSTANCE(L, inst);

		if(lua_isuserdata(L, 2))
			inst->AddChild(**CheckUserData(L, Reference<SceneNode>, 2, "SceneNode"));
		else if(lua_istable(L, 2))
		{
			unsigned int n = luaL_len(L, 2);
			for(unsigned int i = 1; i <= n; ++i)
			{
				lua_rawgeti(L, 2, i);

				void *obj = luaL_testudata(L, -1, "SceneNode");
				if(obj == NULL)
					luaL_error(L, "Expected SceneNode at index %d, got %s.", i, luaL_typename(L, -1));

				inst->AddChild(**((Reference<SceneNode>**) obj));
				lua_pop(L, 1);
			}
		}
		else
			luaL_argerror(L, 2, "SceneNode or array of SceneNodes expected.");

		return 0;
	}
	int l_SceneSetTransform(lua_State *L)
	{
		RenderInstance *inst;
		GETRENDERINSTANCE(L, inst);

		if(lua_isuserdata(L, 2))
			inst->AddTransform(*CheckUserData(L, Transform, 2, "Transform"), lua_gettop(L) < 3 ? 0.f : (float) luaL_checknumber(L, 3));
		else if(lua_istable(L, 2))
		{
			unsigned int n = luaL_len(L, 2) / 2;
			luaL_argcheck(L, n * 2 == luaL_len(L, 2), 2, "The list of transforms must include transform-time pairs.");
			for(unsigned int i = 1; i <= n; ++i)
			{
				lua_rawgeti(L, 2, 2 * i - 1);
				void *obj = luaL_testudata(L, -1, "Transform");
				if(!obj)
				{
					char err[256];
					::sprintf(err, "Expected Transform at index %d, got %s.", 2 * i - 1, luaL_typename(L, -1));
					luaL_argerror(L, 2, err);
				}
				Transform *transform = *(Transform **) obj;
				lua_pop(L, 1);

				lua_rawgeti(L, 2, 2 * i);
				if(LUA_TNUMBER != lua_type(L, -1))
				{
					char err[256];
					::sprintf(err, "Expected number at index %d, got %s.", 2 * i, luaL_typename(L, -1));
					luaL_argerror(L, 2, err);
				}
				float time = float(lua_tonumber(L, -1));
				lua_pop(L, 1);

				inst->AddTransform(transform, time);
			}
		}
		else
			luaL_argerror(L, 2, "Transform or array of transform-time pairs expected.");

		lua_pushvalue(L, 1);
		luaL_getmetatable(L, "SceneNode");
		lua_setmetatable(L, -2);

		return 1;
	}
	int l_IntegratorPath(lua_State *L)
	{
		RenderInstance *inst;
		GETRENDERINSTANCE(L, inst);

		int top = lua_gettop(L);
		inst->PathIntegrator(top >= 2 ? luaL_checkunsigned(L, 2) : 5);

		return 0;
	}
	int l_IntegratorBidir(lua_State *L)
	{
		RenderInstance *inst;
		GETRENDERINSTANCE(L, inst);

		int top = lua_gettop(L);
		inst->BidirIntegrator(top >= 2 ? luaL_checkunsigned(L, 2) : 5, top >= 3 ? luaL_checkunsigned(L, 3) : 5);

		return 0;
	}
	int l_IntegratorSingle(lua_State *L)
	{
		RenderInstance *inst;
		GETRENDERINSTANCE(L, inst);

		int top = lua_gettop(L);
		inst->SingleIntegrator(top >= 2 ? float(luaL_checknumber(L, 2)) : 0.5f);

		return 0;
	}
	int l_ImageFilm(lua_State *L)
	{
		RenderInstance *inst;
		GETRENDERINSTANCE(L, inst);
		int width = int(luaL_checkinteger(L, 2));
		int height = int(luaL_checkinteger(L, 3));
		inst->ImageFilm(width, height);
		return 0;
	}

	int l_PerspectiveCamera(lua_State *L)
	{
		int params = lua_gettop(L);
		
		float fov = params >= 2 ? float(luaL_checknumber(L, 2)) : 90.f;
		float sOpen = params >= 3 ? float(luaL_checknumber(L, 3)) : 0.f;
		float sClose = params >= 4 ? float(luaL_checknumber(L, 4)) : 0.f;
		float focald = params >= 5 ? float(luaL_checknumber(L, 5)) : 0.f;
		float lensr = params >= 6 ? float(luaL_checknumber(L, 6)) : 0.f;

		RenderInstance *inst;
		GETRENDERINSTANCE(L, inst);

		inst->PerspectiveCamera(fov, sOpen, sClose, focald, lensr);
		return 0;
	}

	int l_AddCameraTransform(lua_State *L)
	{
		RenderInstance *inst;
		GETRENDERINSTANCE(L, inst);

		if(lua_isuserdata(L, 2))
			inst->AddCameraTransform(*CheckUserData(L, Transform, 2, "Transform"), lua_gettop(L) < 3 ? 0.f : (float) luaL_checknumber(L, 3));
		else if(lua_istable(L, 2))
		{
			unsigned int n = luaL_len(L, 2) / 2;
			luaL_argcheck(L, n * 2 == luaL_len(L, 2), 2, "The list of transforms must include transform-time pairs.");
			for(unsigned int i = 1; i <= n; ++i)
			{
				lua_rawgeti(L, 2, 2 * i - 1);
				void *obj = luaL_testudata(L, -1, "Transform");
				if(!obj)
				{
					char err[256];
					::sprintf(err, "Expected Transform at index %d, got %s.", 2 * i - 1, luaL_typename(L, -1));
					luaL_argerror(L, 2, err);
				}
				Transform *transform = *(Transform **) obj;
				lua_pop(L, 1);

				lua_rawgeti(L, 2, 2 * i);
				if(LUA_TNUMBER != lua_type(L, -1))
				{
					char err[256];
					::sprintf(err, "Expected number at index %d, got %s.", 2 * i, luaL_typename(L, -1));
					luaL_argerror(L, 2, err);
				}
				float time = float(lua_tonumber(L, -1));
				lua_pop(L, 1);

				inst->AddCameraTransform(transform, time);
			}
		}
		else
			luaL_argerror(L, 2, "Transform or array of transform-time pairs expected.");

		return 0;
	}

	int l_RandomSampler(lua_State *L)
	{
		RenderInstance *inst;
		GETRENDERINSTANCE(L, inst);

		inst->UseRandomSampling();

		return 0;
	}

	int l_SobolSampler(lua_State *L)
	{
		RenderInstance *inst;
		GETRENDERINSTANCE(L, inst);

		inst->UseSobolSequence();

		return 0;
	}

	int l_NewTransform(lua_State *L)
	{
		int top = lua_gettop(L);

		NewUserData(L, Transform, data);
		*data = new Transform();

		if(top >= 2)
			**data = **CheckUserData(L,Transform,2,"Transform");
		if(top >= 3)
			**data = **data * **CheckUserData(L,Transform,3,"Transform");

		luaL_getmetatable(L, "Transform");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_Translate(lua_State *L)
	{
		Transform **data = CheckUserData(L, Transform, 1, "Transform");

		Vector delta((float) luaL_checknumber(L, 2), (float) luaL_checknumber(L, 3), (float) luaL_checknumber(L, 4));
		**data = **data * Translate(delta);

		lua_pushvalue(L, 1);
		luaL_getmetatable(L, "Transform");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_RotateX(lua_State *L)
	{
		Transform **data = CheckUserData(L, Transform, 1, "Transform");

		**data = **data * RotateX((float) luaL_checknumber(L, 2));

		lua_pushvalue(L, 1);
		luaL_getmetatable(L, "Transform");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_RotateY(lua_State *L)
	{
		Transform **data = CheckUserData(L, Transform, 1, "Transform");

		**data = **data * RotateY((float) luaL_checknumber(L, 2));

		lua_pushvalue(L, 1);
		luaL_getmetatable(L, "Transform");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_RotateZ(lua_State *L)
	{
		Transform **data = CheckUserData(L, Transform, 1, "Transform");

		**data = **data * RotateZ((float) luaL_checknumber(L, 2));

		lua_pushvalue(L, 1);
		luaL_getmetatable(L, "Transform");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_Rotate(lua_State *L)
	{
		Transform **data = CheckUserData(L, Transform, 1, "Transform");

		Vector axis;
		get_tuple(L, 2, &axis.x, 3);
		**data = **data * Rotate(axis, (float) luaL_checknumber(L, 3));

		lua_pushvalue(L, 1);
		luaL_getmetatable(L, "Transform");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_Scale(lua_State *L)
	{
		Transform **data = CheckUserData(L, Transform, 1, "Transform");

		**data = **data * Scale((float) luaL_checknumber(L, 2), (float) luaL_checknumber(L, 3), (float) luaL_checknumber(L, 4));

		lua_pushvalue(L, 1);
		luaL_getmetatable(L, "Transform");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_LookAt(lua_State *L)
	{
		Transform **data = CheckUserData(L, Transform, 1, "Transform");

		Point p, at;
		Vector up;
		get_tuple(L, 2, &p.x, 3);
		get_tuple(L, 3, &at.x, 3);
		get_tuple(L, 4, &up.x, 3);

		**data = **data * LookAt(p, at, up);

		lua_pushvalue(L, 1);
		luaL_getmetatable(L, "Transform");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_Invert(lua_State *L)
	{
		Transform **data = CheckUserData(L, Transform, 1, "Transform");

		**data = (*data)->Inverse();

		lua_pushvalue(L, 1);
		luaL_getmetatable(L, "Transform");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_AnimatedNode(lua_State *L)
	{
		int nParams = lua_gettop(L);
		vector<Reference<SceneNode>> nodes;

		if(lua_isuserdata(L, 2))
			nodes.push_back(**CheckUserData(L, Reference<SceneNode>, 2, "SceneNode"));
		else if(lua_istable(L, 2))
		{
			unsigned int n = luaL_len(L, 2);
			luaL_argcheck(L, n > 0, 2, "No child nodes specified.");
			for(unsigned int i = 1; i <= n; ++i)
			{
				lua_rawgeti(L, 2, i);
				void *obj = luaL_testudata(L, -1, "SceneNode");
				if(!obj)
				{
					char err[256];
					::sprintf(err, "Expected SceneNode at index %d, got %s.", i, luaL_typename(L, -1));
					luaL_argerror(L, 2, err);
				}

				nodes.push_back(**((Reference<SceneNode>**) obj));
				lua_pop(L, 1);
			}
		}
		else
			luaL_argerror(L, 2, "SceneNode or array of SceneNodes expected.");

		NewUserData(L, Reference<SceneNode>, data);
		if(nParams >= 3)
			*data = new Reference<SceneNode>( new AnimatedSceneNode(&nodes, AccelType(luaL_checkunsigned(L, 3))) );
		else
			*data = new Reference<SceneNode>( new AnimatedSceneNode(&nodes) );

		luaL_getmetatable(L, "SceneNode");
		lua_setmetatable(L, -2);

		return 1;
	}
	int l_NodeAddTransform(lua_State *L)
	{
		Reference<SceneNode> **data = CheckUserData(L, Reference<SceneNode>, 1, "SceneNode");
		AnimatedSceneNode *node = dynamic_cast<AnimatedSceneNode *>(const_cast<SceneNode *>((*data)->GetPtr()));

		luaL_argcheck(L, node != NULL, 1, "Only animated nodes can be transformed.");

		if(lua_isuserdata(L, 2))
			node->AddTransform(*CheckUserData(L, Transform, 2, "Transform"), lua_gettop(L) < 3 ? 0.f : (float) luaL_checknumber(L, 3));
		else if(lua_istable(L, 2))
		{
			unsigned int n = luaL_len(L, 2) / 2;
			luaL_argcheck(L, n * 2 == luaL_len(L, 2), 2, "The list of transforms must include transform-time pairs.");
			for(unsigned int i = 1; i <= n; ++i)
			{
				lua_rawgeti(L, 2, 2 * i - 1);
				void *obj = luaL_testudata(L, -1, "Transform");
				if(!obj)
				{
					char err[256];
					::sprintf(err, "Expected Transform at index %d, got %s.", 2 * i - 1, luaL_typename(L, -1));
					luaL_argerror(L, 2, err);
				}
				Transform *transform = *(Transform **) obj;
				lua_pop(L, 1);

				lua_rawgeti(L, 2, 2 * i);
				if(LUA_TNUMBER != lua_type(L, -1))
				{
					char err[256];
					::sprintf(err, "Expected number at index %d, got %s.", 2 * i, luaL_typename(L, -1));
					luaL_argerror(L, 2, err);
				}
				float time = float(lua_tonumber(L, -1));
				lua_pop(L, 1);

				node->AddTransform(transform, time);
			}
		}
		else
			luaL_argerror(L, 2, "Transform or array of transform-time pairs expected.");

		lua_pushvalue(L, 1);
		luaL_getmetatable(L, "SceneNode");
		lua_setmetatable(L, -2);

		return 1;
	}
	int l_NodeSetAccelType(lua_State *L)
	{
		Reference<SceneNode> **data = CheckUserData(L, Reference<SceneNode>, 1, "SceneNode");
		AnimatedSceneNode *node = dynamic_cast<AnimatedSceneNode *>(const_cast<SceneNode *>((*data)->GetPtr()));
		if(node == NULL)
		{
			GeometricNode *geomNode = dynamic_cast<GeometricNode *>(const_cast<SceneNode *>((*data)->GetPtr()));
			luaL_argcheck(L, geomNode != NULL, 1, "Only animated and geometric nodes can specify accelerator types.");
			geomNode->SetAccelType(AccelType(luaL_checkunsigned(L, 2)));
		}
		else
			node->SetAccelType(AccelType(luaL_checkunsigned(L, 2)));

		lua_pushvalue(L, 1);
		luaL_getmetatable(L, "SceneNode");
		lua_setmetatable(L, -2);

		return 1;
	}
	int l_GeometricNode(lua_State *L)
	{
		int top = lua_gettop(L);

		auto shape = **CheckUserData(L, Reference<ShapeNode>, 2, "Shape");

		Reference<Material> mat = NULL;
		GeometricNode::AreaLightData *arealight = NULL;
		Reference<Volume> inner = NULL;
		Reference<Volume> outer = NULL;

		int nParam = 3;
		if(top >= nParam)
		{
			void *param = luaL_testudata(L, nParam, "Material");
			if(param != NULL)
			{
				mat = **((Reference<Material> **) param);
				++nParam;
			}
		}

		if(top >= nParam)
		{
			void *param = luaL_testudata(L, nParam, "AreaLight");
			if(param != NULL)
			{
				 arealight = *((GeometricNode::AreaLightData **) param);
				 ++nParam;
			}
			else if(!mat)
				luaL_error(L, "At least a material or area light is required at argument 3.");
		}

		if(top >= nParam)
		{
			void *param = luaL_testudata(L, nParam, "Volume");
			if(param != NULL)
				inner = **((Reference<Volume> **) param);
		}

		if(top >= nParam + 1)
		{
			void *param = luaL_testudata(L, nParam, "Volume");
			if(param != NULL)
				outer = **((Reference<Volume> **) param);
		}

		NewUserData(L, Reference<SceneNode>, data);
		*data = new Reference<SceneNode>(new GeometricNode(shape, mat, arealight, inner, outer));
		luaL_getmetatable(L, "SceneNode");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_TriangleMesh(lua_State *L)
	{
		int nParams = lua_gettop(L);
		int nParam = 2;

		Transform *toWorld = NULL;
		if(lua_isuserdata(L, nParam))
		{
			toWorld = *CheckUserData(L, Transform, nParam, "Transform");
			++nParam;
		}

		Point *v = NULL;
		unsigned int *vi = NULL;
		float *uv = NULL;
		Normal *n = NULL;
		Vector *s = NULL;
		unsigned int nV = 0;
		unsigned int nT = 0;
		bool hasUV = false, hasNormals = false, hasTangents = false;
		unsigned int smooth = 0;
		if(lua_isstring(L, nParam) && !lua_isnumber(L, nParam))
		{
			if(nParams >= nParam + 1)
				hasUV = lua_istable(L, nParam + 1);
			if(nParams >= nParam + 2)
			{
				if(lua_isnumber(L, nParam + 2))
				{
					smooth = lua_tounsigned(L, nParam + 2);
					hasNormals = smooth != 0;
				}
				else
					hasNormals = lua_istable(L, nParam + 2);
			}
			if(nParams >= nParam + 3)
				hasTangents = lua_istable(L, nParam + 3);
			
			ReadMeshGeometry(lua_tostring(L, nParam), &nV, &nT, &vi, &v, hasUV ? NULL : &uv, hasNormals ? NULL : &n, hasTangents ? NULL : &s);
			if(nV == 0 || nT == 0 || vi == NULL || v == NULL)
				luaL_argerror(L, nParam, "Cannot continue due to errors within the parsed file.");
			++nParam;
		}
		else
		{
			luaL_checktype(L, nParam, LUA_TTABLE);
			nV = luaL_len(L, nParam) / 3;
			luaL_argcheck(L, nV * 3 == luaL_len(L, nParam), nParam, "Vertices must be a multiple of 3.");

			v = AllocAligned<Point>(nV);
			get_tuple(L, nParam, (float *) &v[0].x, nV * 3);
			++nParam;

			luaL_checktype(L, nParam, LUA_TTABLE);
			nT = luaL_len(L, nParam) / 3;
			luaL_argcheck(L, nT * 3 == luaL_len(L, nParam), nParam, "Vertex indices must be a multiple of 3.");

			vi = AllocAligned<unsigned int>(nT * 3);
			for (unsigned int i = 1; i <= nT * 3; ++i) 
			{
				lua_rawgeti(L, nParam, i);
				vi[i - 1] = luaL_checkunsigned(L, -1);
				lua_pop(L, 1);
				luaL_argcheck(L, vi[i-1] < nV && vi[i-1] >= 0, nParam, "Out of bounds vertex index detected.");
			}
			if(nParams >= nParam + 1)
				hasUV = lua_istable(L, nParam + 1);
			if(nParams >= nParam + 2)
			{
				if(lua_isnumber(L, nParam + 2))
				{
					smooth = lua_tounsigned(L, nParam + 2);
					hasNormals = smooth != 0;
				}
				else
					hasNormals = lua_istable(L, nParam + 2);
			}
			if(nParams >= nParam + 3)
				hasTangents = lua_istable(L, nParam + 3);

			++nParam;
		}
		
		if(hasUV)
		{
			luaL_argcheck(L, nV * 2 == luaL_len(L, nParam), nParam, "The number of UV parameters is not equal to the number of vertices.");
			uv = AllocAligned<float>(nV * 2);
			get_tuple(L, nParam, uv, nV * 2);
		}
		
		++nParam;

		if(hasNormals && smooth == 0)
		{
			luaL_argcheck(L, nV * 3 == luaL_len(L, nParam), nParam, "The number of normals is not equal to the number of vertices.");
			n = AllocAligned<Normal>(nV);
			get_tuple(L, nParam, (float *) &n[0].x, nV * 3);
		}
		else
			n = GetSmoothNormals(v, vi, nV, nT, smooth, uv);

		++nParam;

		if(hasTangents)
		{
			luaL_argcheck(L, nV * 3 == luaL_len(L, nParam), nParam, "The number of tangents is not equal to the number of vertices.");
			s = AllocAligned<Vector>(nV);
			get_tuple(L, nParam, (float *) &s[0].x, nV * 3);
		}

		NewUserData(L, Reference<ShapeNode>, data);
		*data = new Reference<ShapeNode>(new ShapeNode(Reference<Shape>(new TriangleMesh(toWorld == NULL ? Transform() : *toWorld, nV, nT, vi, v, uv, n, s))));

		luaL_getmetatable(L, "Shape");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_PointLight(lua_State *L)
	{
		Point p;
		get_tuple(L, 2, &p.x, 3);

		Spectrum emit = GetSpectrumColor(L, 3);

		Reference<Light> light;

		if(lua_gettop(L) >= 4)
			light = new PointLight(p, emit, *CheckUserData(L, Transform, 4, "Transform"));
		else
			light = new PointLight(p, emit);
		
		NewUserData(L, Reference<LightNode>, data);
		*data = new Reference<LightNode>(new LightNode(light));
		luaL_getmetatable(L, "SceneNode");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_DirectionalLight(lua_State *L)
	{
		Reference<Light> light;
		if(lua_istable(L, 3) && luaL_len(L, 3) == 3)
		{
			Point from, to;
			get_tuple(L, 2, &from.x, 3);
			get_tuple(L, 3, &to.x, 3);
			Spectrum emit = GetSpectrumColor(L, 4);
			if(lua_gettop(L) >= 5)
				light = new DirectionalLight(from, to, emit, *CheckUserData(L, Transform, 5, "Transform"));
			else
				light = new DirectionalLight(from, to, emit);
		}
		else
		{
			Vector dir;
			get_tuple(L, 2, &dir.x, 3);
			Spectrum emit = GetSpectrumColor(L, 3);
			if(lua_gettop(L) >= 4)
				light = new DirectionalLight(dir, emit, *CheckUserData(L, Transform, 4, "Transform"));
			else
				light = new DirectionalLight(dir, emit);
		}
		NewUserData(L, Reference<LightNode>, data);
		*data = new Reference<LightNode>(new LightNode(light));
		luaL_getmetatable(L, "SceneNode");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_InfiniteAreaLight(lua_State *L)
	{
		int top = lua_gettop(L);
		EnvironmentMapping *mapping;
		int n = 2;
		if(lua_isstring(L, 2) && !lua_isnumber(L, 2))
		{
			const char *strEnv = lua_tostring(L, 2);
			if(strcmp(strEnv, "LATLONG") == 0)
				mapping = new LatLongMapping();
			else if(strcmp(strEnv, "VERTICALCROSS") == 0)
				mapping = new VerticalCrossMapping();
			else
				luaL_argerror(L, 2, "Unrecognized environment mapping.");
			
			n = 3;
		}
		else
			mapping = new LatLongMapping();

		TexInfo *info = *CheckUserData(L, TexInfo, n, "TextureInfo");
		
		RenderInstance *inst;
		GETRENDERINSTANCE(L, inst);

		Reference<Light> light = new InfiniteAreaLight(mapping, inst->GetSpectrumImage(*info), top >= n + 1 ? luaL_checkunsigned(L, n + 1) : 512);

		NewUserData(L, Reference<LightNode>, data);
		*data = new Reference<LightNode>(new LightNode(light));

		luaL_getmetatable(L, "SceneNode");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_DiffuseLightData(lua_State *L)
	{
		DiffuseLightData *diffuseData = new DiffuseLightData();
		diffuseData->emit = GetSpectrumTexture(L, 2, false);

		NewUserData(L, GeometricNode::AreaLightData, data);
		*data = diffuseData;
		luaL_getmetatable(L, "AreaLight");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_GlassMaterial(lua_State *L)
	{
		int top = lua_gettop(L);
		NewUserData(L, Reference<Material>, data);
		*data = new Reference<Material>(new GlassMaterial(	GetSpectrumTexture(L, 2, false),
															GetSpectrumTexture(L, 3, false),
															top >= 4 ? GetFloatTexture(L, 4, true) : (Texture<float> *) NULL,
															top >= 5 ? GetFloatTexture(L, 5, true) : (Texture<float> *) NULL));
		luaL_getmetatable(L, "Material");
		lua_setmetatable(L, -2);

		return 1;
	}
	int l_GlossyMaterial(lua_State *L)
	{
		int top = lua_gettop(L);
		NewUserData(L, Reference<Material>, data);
		*data = new Reference<Material>(new GlossyMaterial(	GetSpectrumTexture(L, 2, false),					//diffuse
															GetSpectrumTexture(L, 3, false),					//specular
															top >= 4 ? GetFloatTexture(L, 4, true) : (Texture<float> *) NULL,		//nu
															top >= 5 ? GetFloatTexture(L, 5, true) : (Texture<float> *) NULL,		//nv
															top >= 6 ? GetSpectrumTexture(L, 6, true) : (Texture<Spectrum> *) NULL,	//alpha
															top >= 7 ? GetFloatTexture(L, 7, true) : (Texture<float> *)NULL,		//depth
															top >= 8 ? GetFloatTexture(L, 8, true) : (Texture<float> *) NULL,		//ior
															top >= 9 ? luaL_checkbool(L, 9) : false,			//multibounce
															top >= 10 ? GetFloatTexture(L, 10, true) : (Texture<float> *) NULL		//bump
															));

		luaL_getmetatable(L, "Material");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_MatteMaterial(lua_State *L)
	{
		int top = lua_gettop(L);
		NewUserData(L, Reference<Material>, data);
		*data = new Reference<Material>(new MatteMaterial(	GetSpectrumTexture(L, 2, false),
															top >= 3 ? GetFloatTexture(L, 3, true) : (Texture<float> *) NULL,
															top >= 4 ? GetFloatTexture(L, 4, true) : (Texture<float> *) NULL));
		luaL_getmetatable(L, "Material");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_NullMaterial(lua_State *L)
	{
		int top = lua_gettop(L);
		NewUserData(L, Reference<Material>, data);
		*data = new Reference<Material>(new NullMaterial( top >= 2 ? GetSpectrumTexture(L, 2, false) : new ConstantTexture<Spectrum>(1.f) ));
		luaL_getmetatable(L, "Material");
		lua_setmetatable(L, -2);

		return 1;
	}
	
	int l_MetalMaterial(lua_State *L)
	{
		int top = lua_gettop(L);
		NewUserData(L, Reference<Material>, data);
		*data = new Reference<Material>(new MetalMaterial(	top >= 2 ? GetSpectrumTexture(L, 2, false) : (Texture<Spectrum> *) NULL,
															top >= 3 ? GetSpectrumTexture(L, 3, false) : (Texture<Spectrum> *) NULL,
															top >= 4 ? GetFloatTexture(L, 4, true) : (Texture<float> *) NULL,
															top >= 5 ? GetFloatTexture(L, 5, true) : (Texture<float> *) NULL,
															top >= 6 ? GetFloatTexture(L, 6, true) : (Texture<float> *) NULL));
		luaL_getmetatable(L, "Material");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_SchlickMetalMaterial(lua_State *L)
	{
		int top = lua_gettop(L);
		NewUserData(L, Reference<Material>, data);
		*data = new Reference<Material>(new SchlickMetalMaterial(	top >= 2 ? GetSpectrumTexture(L, 2, false) : (Texture<Spectrum> *) NULL,
																	top >= 3 ? GetSpectrumTexture(L, 3, false) : (Texture<Spectrum> *) NULL,
																	top >= 4 ? GetFloatTexture(L, 4, true) : (Texture<float> *) NULL,
																	top >= 5 ? GetFloatTexture(L, 5, true) : (Texture<float> *) NULL,
																	top >= 6 ? GetFloatTexture(L, 6, true) : (Texture<float> *) NULL));
		luaL_getmetatable(L, "Material");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_SubstrateMaterial(lua_State *L)
	{
		int top = lua_gettop(L);
		NewUserData(L, Reference<Material>, data);
		*data = new Reference<Material>(new SubstrateMaterial(	GetSpectrumTexture(L, 2, false),
																GetSpectrumTexture(L, 3, false),
																top >= 4 ? GetFloatTexture(L, 4, true) : (Texture<float> *) NULL,		//roughness - u
																top >= 5 ? GetFloatTexture(L, 5, true) : (Texture<float> *) NULL,		//roughness - v
																top >= 6 ? GetSpectrumTexture(L, 6, true) : (Texture<Spectrum> *) NULL,	//alpha
																top >= 7 ? GetFloatTexture(L, 7, true) : (Texture<float> *) NULL,		//depth
																top >= 8 ? GetFloatTexture(L, 8, true) : (Texture<float> *) NULL,		//ior
																top >= 9 ? GetFloatTexture(L, 9, true) : (Texture<float> *) NULL		//bump
																));
		luaL_getmetatable(L, "Material");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_MirrorMaterial(lua_State *L)
	{
		int top = lua_gettop(L);
		NewUserData(L, Reference<Material>, data);
		*data = new Reference<Material>(new MirrorMaterial(	GetSpectrumTexture(L, 2, false),
															top >= 3 ? GetFloatTexture(L, 3, true) : (Texture<float> *) NULL));
		luaL_getmetatable(L, "Material");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_MixedMaterial(lua_State *L)
	{
		int top = lua_gettop(L);
		NewUserData(L, Reference<Material>, data);
		*data = new Reference<Material>(new MixedMaterial(	**CheckUserData(L, Reference<Material>, 2, "Material"),
															**CheckUserData(L, Reference<Material>, 3, "Material"),
															top >= 4 ? GetFloatTexture(L, 3, false) : Reference<Texture<float>>(new ConstantTexture<float>(0.5f))));
		luaL_getmetatable(L, "Material");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_NewTextureInfo(lua_State *L)
	{
		int top = lua_gettop(L);
		NewUserData(L, TexInfo, data);
		*data = new TexInfo();
		(*data)->filename = luaL_checkstring(L, 2);
		(*data)->scale = top >= 3 ? float(luaL_checknumber(L, 3)) : 1.f;
		(*data)->gamma = top >= 4 ? float(luaL_checknumber(L, 4)) : 1.f;

		luaL_getmetatable(L, "TextureInfo");
		lua_setmetatable(L, -2);

		return 1;
	}

	int l_HomogeneousVolume(lua_State *L)
	{
		int top = lua_gettop(L);
		NewUserData(L, Reference<Volume>, data);

		*data = new Reference<Volume>(new HomogeneousVolume(
			GetSpectrumTexture(L, 2, false),
			GetSpectrumTexture(L, 3, false),
			GetSpectrumTexture(L, 4, false)
		));
		
		luaL_getmetatable(L, "Volume");
		lua_setmetatable(L, -2);
		 
		return 1;
	}

	template <typename T>
	int l_GenericDelete(lua_State *L, const char *metatableName)
	{
		T *data = *CheckUserData(L, T, 1, metatableName);
		delete data;
		return 0;
	}

	int l_DeleteNode(lua_State *L){ return l_GenericDelete<Reference<SceneNode>>(L, "SceneNode"); }
	int l_DeleteTransform(lua_State *L){ return l_GenericDelete<Transform>(L, "Transform"); }
	int l_DeleteShape(lua_State *L){ return l_GenericDelete<Reference<ShapeNode>>(L, "Shape"); }
	int l_DeleteAreaLight(lua_State *L){ return l_GenericDelete<GeometricNode::AreaLightData>(L, "AreaLight"); }
	int l_DeleteMaterial(lua_State *L){ return l_GenericDelete<Reference<Material>>(L, "Material"); }
	int l_DeleteVolume(lua_State *L){ return l_GenericDelete<Reference<Volume>>(L, "Volume"); }
	int l_DeleteFloatTexture(lua_State *L){ return l_GenericDelete<RFloatTexture>(L, "FloatTexture"); }
	int l_DeleteSpectrumTexture(lua_State *L){ return l_GenericDelete<RSpectrumTexture>(L, "SpectrumTexture"); }
	int l_DeleteTextureInfo(lua_State *L){ return l_GenericDelete<TexInfo>(L, "TextureInfo"); }

	LuaParser::LuaParser()
	{
	}

	LuaParser::~LuaParser()
	{
	}

	void LuaParser::Parse(const char *filename, RenderInstance *instance)
	{
		instance->SceneInit();

		lua_State *L = luaL_newstate();
		luaL_openlibs(L);
		registerGlobals(L);
		initMetatables(L);

		lua_pushlightuserdata(L, instance);
		lua_setglobal(L, "_RINST");

		Info("Parsing file %s.", filename);

		if(luaL_loadfile(L, filename) || lua_pcall(L, 0, 0, 0))
			Error(lua_tostring(L, -1), "LuaParser", "Parse", 1);
		else
			instance->SceneFinish();

		lua_close(L);
	}

	void LuaParser::initMetatable(lua_State *L, const char *name, luaL_Reg *methods) const
	{
		luaL_newmetatable(L, name);

		luaL_setfuncs (L, methods, 0);
		lua_pushvalue(L, -1);
		lua_setfield(L, -1, "__index");
		lua_setglobal(L, name);
	}

	void LuaParser::registerGlobals(lua_State *L)
	{
		luaL_newmetatable(L, "Accelerator");
		lua_pushunsigned(L, AccelType::BINNEDKDTREE);
		lua_setfield(L, 1, "BINNEDKDTREE");
		lua_pushunsigned(L, AccelType::BVH);
		lua_setfield(L, 1, "BVH");
		lua_pushunsigned(L, AccelType::NONE);
		lua_setfield(L, 1, "NONE");
		lua_setglobal(L, "Accelerator");

		luaL_newmetatable(L, "Color");
		lua_pushinteger(L, SpectrumColorType::RGB);
		lua_setfield(L, 1, "RGB");
		lua_pushinteger(L, SpectrumColorType::XYZ);
		lua_setfield(L, 1, "XYZ");
		lua_setglobal(L, "Color");

		luaL_newmetatable(L, "TexMap");
		lua_pushstring(L, "UV");
		lua_setfield(L, 1, "UV");
		lua_pushstring(L, "SPHERICAL");
		lua_setfield(L, 1, "SPHERICAL");
		lua_pushstring(L, "CYLINDRICAL");
		lua_setfield(L, 1, "CYLINDRICAL");
		lua_pushstring(L, "PLANAR");
		lua_setfield(L, 1, "PLANAR");
		lua_setglobal(L, "TexMap");

		luaL_newmetatable(L, "EnvMap");
		lua_pushstring(L, "LATLONG");
		lua_setfield(L, 1, "LATLONG");
		lua_pushstring(L, "ANGULAR");
		lua_setfield(L, 1, "ANGULAR");
		lua_pushstring(L, "VERTICALCROSS");
		lua_setfield(L, 1, "VERTICALCROSS");
		lua_setglobal(L, "EnvMap");

		luaL_newmetatable(L, "ImageFilter");
		lua_pushinteger(L, ImageFilter::BILINEAR);
		lua_setfield(L, 1, "BILINEAR");
		lua_pushinteger(L, ImageFilter::NEAREST);
		lua_setfield(L, 1, "NEAREST");
		lua_setglobal(L, "ImageFilter");

		luaL_newmetatable(L, "ImageWrap");
		lua_pushinteger(L, ImageWrap::BLACK);
		lua_setfield(L, 1, "BLACK");
		lua_pushinteger(L, ImageWrap::CLAMP);
		lua_setfield(L, 1, "CLAMP");
		lua_pushinteger(L, ImageWrap::REPEAT);
		lua_setfield(L, 1, "REPEAT");
		lua_setglobal(L, "ImageWrap");
	}

	void LuaParser::initMetatables(lua_State *L)
	{
		luaL_Reg transform_mehods[] =
		{
			{"New", l_NewTransform},
			{"Translate", l_Translate},
			{"RotateX", l_RotateX},
			{"RotateY", l_RotateY},
			{"RotateZ", l_RotateZ},
			{"Rotate", l_Rotate},
			{"Scale", l_Scale},
			{"LookAt", l_LookAt},
			{"Invert", l_Invert},
			{"__gc", l_DeleteTransform},
			{NULL, NULL}
		};
		initMetatable(L, "Transform", transform_mehods);

		luaL_Reg film_methods[] =
		{
			{"Image", l_ImageFilm},
			{NULL, NULL}
		};
		initMetatable(L, "Film", film_methods);

		luaL_Reg camera_methods[] =
		{
			{"Perspective", l_PerspectiveCamera},
			{"SetTransform", l_AddCameraTransform},
			{NULL, NULL}
		};
		initMetatable(L, "Camera", camera_methods);

		luaL_Reg sampler_methods[] =
		{
			{"Random", l_RandomSampler},
			{"Sobol", l_SobolSampler},
			{NULL, NULL}
		};
		initMetatable(L, "Sampler", sampler_methods);

		luaL_Reg scene_methods[] =
		{
			{"AddChildren", l_SceneAddChildren},
			{"SetAccelerator", l_SceneSetAccelType},
			{"SetTransform", l_SceneSetTransform},
			{NULL, NULL}
		};
		initMetatable(L, "Scene", scene_methods);

		luaL_Reg integrator_methods[] =
		{
			{"Bidir", l_IntegratorBidir},
			{"Path", l_IntegratorPath},
			{"Single", l_IntegratorSingle},
			{NULL, NULL}
		};
		initMetatable(L, "Integrator", integrator_methods);

		luaL_Reg node_methods[] =
		{
			{"PointLight", l_PointLight},
			{"DirectionalLight", l_DirectionalLight},
			{"InfiniteAreaLight", l_InfiniteAreaLight},
			{"GeometricNode", l_GeometricNode},
			{"AnimatedNode", l_AnimatedNode},
			{"SetTransform", l_NodeAddTransform},
			{"SetAccelerator", l_NodeSetAccelType},
			{"__gc", l_DeleteNode},
			{NULL, NULL}
		};
		initMetatable(L, "SceneNode", node_methods);

		luaL_Reg shape_methods[] =
		{
			{"TriangleMesh", l_TriangleMesh},
			{"__gc", l_DeleteShape},
			{NULL, NULL}
		};
		initMetatable(L, "Shape", shape_methods);

		luaL_Reg arealight_methods[] =
		{
			{"Diffuse", l_DiffuseLightData},
			{"__gc", l_DeleteAreaLight},
			{NULL, NULL}
		};
		initMetatable(L, "AreaLight", arealight_methods);

		luaL_Reg material_methods[] =
		{
			{"Glass", l_GlassMaterial},
			{"Glossy", l_GlossyMaterial},
			{"Matte", l_MatteMaterial},
			{"Metal", l_MetalMaterial},
			{"Mirror", l_MirrorMaterial},
			{"Mixed", l_MixedMaterial},
			{"Null", l_NullMaterial},
			{"SchlickMetal", l_SchlickMetalMaterial},
			{"Substrate", l_SubstrateMaterial},
			{"__gc", l_DeleteMaterial},
			{NULL, NULL}
		};
		initMetatable(L, "Material", material_methods);

		//Define all texture variants so that Lua can handle type discrepancies and nothing blows up.
		luaL_Reg floatTexture_methods[] =
		{
			{"Bilerp", l_FloatBilerp},
			{"Image", l_FloatImage},
			{"__gc", l_DeleteFloatTexture},
			{NULL, NULL}
		};
		initMetatable(L, "FloatTexture", floatTexture_methods);

		luaL_Reg spectrumTexture_methods[] =
		{
			{"Bilerp", l_SpectrumBilerp},
			{"Image", l_SpectrumImage},
			{"__gc", l_DeleteSpectrumTexture},
			{NULL, NULL}
		};
		initMetatable(L, "SpectrumTexture", spectrumTexture_methods);

		luaL_Reg textureInfo_methods[] =
		{
			{"New", l_NewTextureInfo},
			{"__gc", l_DeleteTextureInfo},
			{NULL, NULL}
		};
		initMetatable(L, "TextureInfo", textureInfo_methods);
		luaL_Reg volume_methods[] =
		{
			{"Homogeneous", l_HomogeneousVolume},
			{"__gc", l_DeleteVolume},
			{NULL, NULL}
		};
		initMetatable(L, "Volume", volume_methods);
	}
}
