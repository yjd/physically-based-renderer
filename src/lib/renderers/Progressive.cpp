/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "renderers/Progressive.h"

namespace Render
{
	struct ProgressiveRenderTask : public Task
	{
		unsigned int taskNum;
		ProgressiveRenderer *renderer;
		Sample *samples;
		MemoryArena *arenas;
		Sampler::SampleRegionData region;
		Ray ray;
		Intersection isect;
	public:
		ProgressiveRenderTask(ProgressiveRenderer *renderer, Sample *samples, MemoryArena *arenas, unsigned int taskNum)
			:	renderer(renderer), samples(samples), region(samples->sampler->GetSampleRegionData(taskNum)), arenas(arenas), taskNum(taskNum){}
		void Run(unsigned int threadIndex)
		{
			Sample *sample = &samples[threadIndex];
			MemoryArena *arena = &arenas[threadIndex];
			while(sample->sampler->GetNextSample(sample, &region))
			{
				ray.time = renderer->time;
				renderer->camera->GenerateRay(sample, &ray);
				Spectrum li = renderer->Li(&isect, sample, ray, *arena);
				renderer->camera->film->AddSample(sample, li);
				arena->FreeAll();
				sample->sampler->IncrementPass(sample->samplerData);
			}
		}
	};
	ProgressiveRenderer::ProgressiveRenderer(Scene *scene, Camera *camera, Sampler *sampler, SurfaceIntegrator *surf, VolumeIntegrator *vol)
		:	Renderer(scene, camera, sampler, surf, vol), passes(0)
	{
		int nTasks = sampler->ComputeNumTasks();
		tasks.reserve(nTasks);
		for(unsigned int i = 0; i < nTasks; ++i)
			tasks.push_back(new ProgressiveRenderTask(this, samples, arenas, i));

		taskset = new TaskSet(&tasks[0], tasks.size(), opts.nThreads);
	}
	ProgressiveRenderer::~ProgressiveRenderer()
	{
		for(unsigned int i = 0; i < tasks.size(); ++i)
			delete tasks[i];
	}
	Spectrum ProgressiveRenderer::Li(Intersection *isect, Sample *sample, Ray &ray, MemoryArena &arena) const
	{
		Spectrum L;
		/* The only advantage of doing an initial intersection test here is the avoidance of
		 * initializing path-related state variables when hitting material-less area lights or
		 * when not intersecting with anything.
		 */
		//Spectrum Lv;
		//Spectrum T;
		if(scene->Intersect(ray, isect))
		{
			BSDF *bsdf = isect->GetBSDF(ray, arena);
			isect->Le(-ray.d, L);
			//The surface integrator's Li method modifies the ray.
			//vol->Li(this, scene, sample, ray, Lv, T, arena);
			surf->Li(this, scene, camera, sample, isect, bsdf, ray, L, arena);
		}
		else
		{
			scene->Le(ray, L);
			//Bidirectional methods can still trace light rays.
			//vol->Li(this, scene, sample, ray, Lv, T, arena);
			surf->Li(this, scene, camera, sample, isect, NULL, ray, L, arena);
		}
		return L;//T * L + Lv;
	}
	void ProgressiveRenderer::Render(RenderStatus *status)
	{
		float prevTime = -1.0;
		status->RenderStart();
		do
		{
			time = camera->GetTime(sampler->GetSample(samples, 4, 0, 0));
			if(prevTime != time)
			{
				prevTime = time;
				scene->lightRoot->ToWorld(scene->lights, time);
			}
			taskset->Work();
			taskset->WaitForAllThreads();
		} while(status->Update(++passes));
		printf("\n\n");
	}
}
