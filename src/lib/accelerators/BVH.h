/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_ACCELERATORS_BVH_H
#define RENDERLIB_ACCELERATORS_BVH_H

#include "Primitive.h"
namespace Render
{
	struct BVHPrimitiveInfo;
	struct BVHNode;
	struct BVHLinearNode;

	enum BVHSplitMethod
	{
		MIDDLE,
		EQUAL,
		SAH
	};

	class BVHAccel : public Aggregate
	{
	uint32_t maxPrimsInNode;
	BVHSplitMethod sm;

	//Flattened array of nodes representing a tree
	BVHLinearNode *nodes;

	BVHNode *recursiveBuild(MemoryArena &arena, vector<BVHPrimitiveInfo> &buildData, uint32_t start, uint32_t end, uint32_t *totalNodes, vector<const Primitive *> &ordered);
	void initLeaf(BVHNode *node, vector<BVHPrimitiveInfo> &buildData, uint32_t start, uint32_t end, uint32_t n, vector<const Primitive *> &ordered, const BBox &b);
	uint32_t recursiveFlatten(BVHNode *buildNode, uint32_t *offset);
public:
	BVHAccel(const vector<Reference<Primitive>> &p, uint32_t maxPrimsInNode = 4, BVHSplitMethod sm = SAH);
	~BVHAccel();
	BBox Bounds() const;
	bool Intersect(const Ray &ray, Intersection *isect) const;
	bool IntersectP(const Ray &ray) const;
	float Pdf(const Ray &rayWorld, const Ray &ray, const Transform *toWorld, Intersection *isect = NULL) const;
};
}

#endif