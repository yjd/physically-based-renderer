/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "lights/Diffuse.h"

namespace Render
{
	DiffuseAreaLight::DiffuseAreaLight(const Reference<Shape> &shape, const Reference<Primitive> &accel, const RSpectrumTexture &emit)
		:	shapeSet(shape, accel), emit(emit){}

	float DiffuseAreaLight::Power(const Scene *scene, const Transform *t) const
	{
		return shapeSet.Area() * M_PI * emit->Avg().y();// * shapeSet.TransformWeight(t);
	}
	bool DiffuseAreaLight::L(const Vector &w, const DifferentialGeometry &dg, const Normal &ng, Spectrum &s) const
	{
		if(Dot(dg.n, w) > 0 && Dot(ng, w) > 0)
		{
			s = emit->Evaluate(dg);
			return true;
		}

		return false;
	}
	bool DiffuseAreaLight::SampleL(const Transform *t, const Point &p, Sample *sample, uint32_t offset, Vector *wi, Spectrum &s, float *pdf, Intersection *isect, Ray *visRay) const
	{
		Sampler *sampler = sample->sampler;
		float u[3];
		sampler->GetSamples(sample, offset, u, 3);
		Point pSample = t->operator()(shapeSet.Sample(u, isect));
		*wi = Normalize(pSample - p);
		*pdf = shapeSet.Pdf(p, *wi, isect, t);
		if(*pdf == 0.f)
			return false;

		if(visRay)
			SetSegment(visRay, p, isect->dg.p);

		return L(-*wi, isect->dg, isect->ng, s);
	}

	bool DiffuseAreaLight::SampleL(const Transform *t, const Scene *scene, Sample *sample, uint32_t offset, Ray *ray, Intersection *isect, Spectrum &s, float *pdf) const
	{
		Sampler *sampler = sample->sampler;
		//When sampling from the light, quasi-montecarlo may cause correlations between the chosen shape and the point on the shape.  This doesn't affect
		//backwards ray tracing.
		float u[3];
		sampler->GetSamples(sample, offset, u, 3);
		*ray = Ray(	t->operator()(shapeSet.Sample(u, isect)),
					UniformSampleSphere(sampler->GetSample(sample, offset + 3), sampler->GetSample(sample, offset + 4)),
					RAYEPSILON, 0, ray->time);

		isect->shape->GetDifferentialGeometry(*ray, isect, t);
		float cosi = Dot(isect->ng, ray->d);
		if(cosi < 0)
			ray->d *= -1.f;

		if(L(ray->d, isect->dg, isect->ng, s))//ng might not like the direction
		{
			s *= abs(cosi);
			ray->maxt = INFINITY;
			*pdf = shapeSet.Pdf(ray->o, t) * UniformHemispherePDF;
			return *pdf > 0.f;
		}
		return false;
	}

	float DiffuseAreaLight::Pdf(const Transform *t, const Point &p, const Vector &wi) const
	{
		return shapeSet.Pdf(p, wi, NULL, t);
	}

	float DiffuseAreaLight::Pdf(const Transform *t, const Scene *scene, const Point &p, const Vector &wi) const
	{
		return shapeSet.Pdf(p, t) * UniformHemispherePDF;
	}
}