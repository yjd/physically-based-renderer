/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "lights/Directional.h"
#include "Scene.h"

namespace Render
{
	DirectionalLight::DirectionalLight(const Point &from, const Point &to, const Spectrum &L, const Transform *t)
		:	dir(Normalize(t ? t->operator()(from - to) : from - to)), L(L)
	{
	}
	DirectionalLight::DirectionalLight(const Vector &dir, const Spectrum &L, const Transform *t)
		:	dir(Normalize(t ? t->operator()(dir) : dir)), L(L)
	{
	}
	
	float DirectionalLight::Power(const Scene *scene, const Transform *t) const
	{
		return L.y() * scene->Radius() * scene->Radius() * M_PI;
	}
	bool DirectionalLight::SampleL(const Transform *t, const Point &p, Sample *sample, uint32_t offset, Vector *wi, Spectrum &s, float *pdf, Intersection *isect, Ray *visRay) const
	{
		*wi = Normalize(t->operator()(dir));
		isect->ng = isect->dg.n = -*wi;
		*pdf = 1.f;
		if(visRay)
			*visRay = Ray(p, *wi, RAYEPSILON, INFINITY, visRay->time);
		s = L;
		return true;
	}
	bool DirectionalLight::SampleL(const Transform *t, const Scene *scene, Sample *sample, uint32_t offset, Ray *ray, Intersection *isect, Spectrum &s, float *pdf) const
	{
		Sampler *sampler = sample->sampler;
		Vector dir = Normalize(t->operator()(this->dir));
		isect->ng = isect->dg.n = dir;
		const Point &p = scene->Center();
		const float &r = scene->Radius();

		Vector su, sv;
		CoordinateSystem(ray->d, &su, &sv);

		float dx, dy;
		ConcentricSampleDisc(sampler->GetSample(sample, offset), sampler->GetSample(sample, offset + 1), &dx, &dy);

		*ray = Ray(p + r * (dir + dx * su + dy * sv), -dir, 0, INFINITY, ray->time);
		*pdf = 1.f / (r * r * M_PI);
		s = L;
		return true;
	}
	float DirectionalLight::Pdf(const Transform *t, const Scene *scene, const Point &p, const Vector &wi) const
	{
		return 1.f / (scene->Radius() * scene->Radius() * M_PI);
	}
}