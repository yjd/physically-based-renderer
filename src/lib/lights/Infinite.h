/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_LIGHTS_INFINITE_H
#define RENDERLIB_LIGHTS_INFINITE_H

#include "Light.h"
#include "Texture.h"
#include "TexImage.h"

namespace Render
{
	class InfiniteAreaLight : public SampleLight
	{
		TextureImage<RGBSpectrum> radianceMap;
		const EnvironmentMapping *mapping;
		Distribution2D *uvDistribution;
		float avg;
	public:
		InfiniteAreaLight(const EnvironmentMapping *mapping, BlockedArray<RGBSpectrum> *data, uint32_t maxres);
		~InfiniteAreaLight();
		bool IsEnvironment() const;
		void Le(const Transform *t, const Ray &ray, Spectrum &s) const;
		bool Le(const Transform *t, const Scene *scene, const Ray &ray, Spectrum &s, float *pdfDirect, float *pdfEmission, Normal *ng) const;
		float Power(const Scene *scene, const Transform *t) const;
		bool SampleL(const Transform *t, const Point &p, Sample *sample, uint32_t offset, Vector *wi, Spectrum &s, float *pdf, Intersection *isect, Ray *visRay = NULL) const;
		bool SampleL(const Transform *t, const Scene *scene, Sample *sample, uint32_t offset, Ray *ray, Intersection *isect, Spectrum &s, float *pdf) const;
		float Pdf(const Transform *t, const Point &p, const Vector &wi) const;
		float Pdf(const Transform *t, const Scene *scene, const Point &p, const Vector &wi) const;
	};
}
#endif