/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "lights/Infinite.h"
#include "Scene.h"

namespace Render
{
	InfiniteAreaLight::InfiniteAreaLight(const EnvironmentMapping *mapping, BlockedArray<RGBSpectrum> *data, uint32_t maxres)
		:	mapping(mapping), radianceMap(data, mapping->GetWrapMode(), ImageFilter::BILINEAR), avg(0.f)
	{
		maxres = max(1u, maxres);
		uint32_t nu, nv;
		mapping->GetMapExtent(data->uSize(), data->vSize(), &nu, &nv);

		uint32_t dnu = nu;
		uint32_t dnv = nv;
		if(dnu > maxres || dnv > maxres)
		{
			dnu = Ceil2UInt(float(maxres * dnu) / float(max(nu, nv)));
			dnv = Ceil2UInt(float(maxres * dnv) / float(max(nu, nv)));
		}

		float us = float(nu) / float(dnu);
		float vs = float(nv) / float(dnv);
		const uint32_t samples = Ceil2UInt(max(us, vs));
		const float invs = 1.f / float(samples);
		us *= invs;
		vs *= invs;

		const float invnu = 1.f / float(nu);
		const float invnv = 1.f / float(nv);

		float *img = new float[dnu * dnv];
		memset(img, 0, sizeof(float) * dnu * dnv);

		const uint32_t dn = dnu * dnv;
		const float invs2 = invs * invs;
		for(uint32_t y = 0; y < dnv * samples; ++y)
		{
			float yp = (0.5f + y) * vs * invnv;
			uint32_t row = uint32_t(y * invs) * dnu;
			for(uint32_t x = 0; x < dnu * samples; ++x)
			{
				float xp = (0.5f + x) * us * invnu;
				float u, v;
				float pdfMap = mapping->Pdf(xp, yp, &u, &v);
				uint32_t index = row + x * invs;
				if(pdfMap <= 0 || index >= dn)
					continue;

				float y = radianceMap.Lookup(u, v).y();
				avg += y;

				img[index] += y * invs2 / pdfMap;
			}
		}
		//TODO: Portals will likely require a change to how this is computed.  In fact if multiple portals can be attached to a light, then each may need their own distribution.
		//Finding the visible extent of the portal projected onto the environment map may be a requirement.

		uvDistribution = new Distribution2D(img, dnu, dnv);
		delete[] img;
	}
	InfiniteAreaLight::~InfiniteAreaLight()
	{
		delete mapping;
		delete uvDistribution;
	}
	bool InfiniteAreaLight::IsEnvironment() const { return true; }
	void InfiniteAreaLight::Le(const Transform *t, const Ray &ray, Spectrum &s) const
	{
		float u, v;
		mapping->Map((*t)(ray.d, true), &u, &v);
		s += Spectrum(radianceMap.Lookup(u, v), ILLUMINANT);
	}
	bool InfiniteAreaLight::Le(const Transform *t, const Scene *scene, const Ray &ray, Spectrum &s, float *pdfDirect, float *pdfEmission, Normal *ng) const
	{
		float u, v;
		mapping->Map((*t)(ray.d, true), &u, &v, pdfDirect);
		if(*pdfDirect == 0.f)
			return false;

		*pdfDirect *= uvDistribution->Pdf(u, v);
		if(*pdfDirect == 0.f)
			return false;

		if(*pdfEmission)
			*pdfEmission = *pdfDirect / (M_PI * scene->Radius() * scene->Radius());

		s = Spectrum(radianceMap.Lookup(u, v), ILLUMINANT);
		return true;
	}
	float InfiniteAreaLight::Power(const Scene *scene, const Transform *t) const
	{
		return scene->Radius() * scene->Radius() * M_PI * avg;
	}
	bool InfiniteAreaLight::SampleL(const Transform *t, const Point &p, Sample *sample, uint32_t offset, Vector *wi, Spectrum &s, float *pdf, Intersection *isect, Ray *visRay) const
	{
		float uv[2];
		uvDistribution->SampleContinuous(sample->sampler->GetSample(sample, offset), sample->sampler->GetSample(sample, offset + 1), uv, pdf);
		if(*pdf == 0.f)
			return false;

		float pdfMap;
		Vector w;
		mapping->Map(uv[0], uv[1], &uv[0], &uv[1], &w, &pdfMap);

		if(pdfMap == 0.f)
			return false;

		t->operator()(w, wi);
		wi->Normalize();
		isect->ng = -*wi;

		*pdf *= pdfMap;

		if(visRay)
			SetSegment(visRay, p, *wi, INFINITY);

		s = Spectrum(radianceMap.Lookup(uv[0], uv[1]), ILLUMINANT);

		return true;
	}
	bool InfiniteAreaLight::SampleL(const Transform *t, const Scene *scene, Sample *sample, uint32_t offset, Ray *ray, Intersection *isect, Spectrum &s, float *pdf) const
	{
		float u[4];
		uvDistribution->SampleContinuous(sample->sampler->GetSample(sample, offset), sample->sampler->GetSample(sample, offset + 1), u, pdf);
		if(*pdf == 0.f)
			return false;

		float mapPdf;
		Vector w;
		mapping->Map(u[0], u[1], &u[0], &u[1], &w, &mapPdf);
		if(mapPdf == 0.f)
			return false;

		*pdf *= mapPdf;

		ray->d = Normalize(t->operator()(w));

		Vector vu, vv;
		CoordinateSystem(ray->d, &vu, &vv);
		const float &r = scene->Radius();
		const Point &c = scene->Center();

		ConcentricSampleDisc(sample->sampler->GetSample(sample, offset + 3), sample->sampler->GetSample(sample, offset + 4), &u[2], &u[3]);
		ray->o = c + (ray->d + u[2] * vu + u[3] * vv) * r;
		ray->d *= -1.f;
		ray->mint = 0.f;
		ray->maxt = INFINITY;

		isect->ng = ray->d;

		*pdf /= M_PI * r * r;

		s = Spectrum(radianceMap.Lookup(u[0], u[1]), ILLUMINANT);

		return true;
	}
	float InfiniteAreaLight::Pdf(const Transform *t, const Point &p, const Vector &wi) const
	{
		float pdfMap, u, v;
		mapping->Map((*t)(wi, true), &u, &v, &pdfMap);
		if(pdfMap == 0.f)
			return 0.f;

		return uvDistribution->Pdf(u, v) * pdfMap;
	}
	float InfiniteAreaLight::Pdf(const Transform *t, const Scene *scene, const Point &p, const Vector &wi) const
	{
		float pdfMap, u, v;
		mapping->Map((*t)(wi, true), &u, &v, &pdfMap);
		if(pdfMap == 0.f)
			return 0.f;

		return uvDistribution->Pdf(u, v) * pdfMap / (scene->Radius() * scene->Radius() * M_PI);
	}
}