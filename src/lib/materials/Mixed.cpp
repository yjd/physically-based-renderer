/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "materials/Mixed.h"

namespace Render
{
	MixedMaterial::MixedMaterial(const Reference<Material> &m1, const Reference<Material> &m2, const RFloatTexture &scale)
		:	m1(m1), m2(m2), scale(scale){}

	BSDF *MixedMaterial::GetBSDF(DifferentialGeometry *dg, const Normal &ng, MemoryArena &arena) const
	{
		float w = scale->Evaluate(*dg);
		float *weights = (float *) arena.Alloc(sizeof(float) * 2);

		//Materials will reuse and update the differential geometry, so make a duplicate.
		DifferentialGeometry dg0 = *dg;
		unsigned int nBSDFs = 0;
		BSDF **bsdfs = (BSDF **) arena.Alloc(sizeof(BSDF *) * 2);
		BSDF *bsdf = m1->GetBSDF(&dg0, ng, arena);
		if(bsdf)
		{
			weights[nBSDFs] = w;
			bsdfs[nBSDFs++] = bsdf;
		}

		bsdf = m2->GetBSDF(dg, ng, arena);
		if(bsdf)
		{
			weights[nBSDFs] = 1.f - w;
			bsdfs[nBSDFs++] = bsdf;
		}

		if(nBSDFs == 0)
			return NULL;
		else if(nBSDFs == 1)
			return bsdfs[0];
		
		return ARENA_ALLOC(arena, MixedBSDF)(bsdfs, 2, weights, 1.f, bsdfs[0]->specularOnly && bsdfs[1]->specularOnly);
	}
}