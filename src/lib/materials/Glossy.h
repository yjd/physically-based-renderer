/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_MATERIALS_GLOSSY_H
#define RENDERLIB_MATERIALS_GLOSSY_H

#include "Material.h"

namespace Render
{
	class GlossyMaterial : public Material
	{
		RSpectrumTexture rd, rs, alpha;
		RFloatTexture nu, nv, bump, depth, ior;
		bool multiBounce;
		bool coatParams;
	public:
		GlossyMaterial(	const RSpectrumTexture &rd, const RSpectrumTexture &rs, const RFloatTexture &nu, const RFloatTexture &nv, 
						const RSpectrumTexture &alpha, const RFloatTexture &depth, const RFloatTexture &ior, bool multiBounce, const RFloatTexture &bump);
		BSDF *GetBSDF(DifferentialGeometry *dg, const Normal &ng, MemoryArena &arena) const;
	};
}

#endif