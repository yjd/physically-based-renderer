/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "materials/Mirror.h"

namespace Render
{
	MirrorMaterial::MirrorMaterial(const RSpectrumTexture &kr, const RFloatTexture &bump)
		:	kr(kr), bump(bump)
	{
	}

	BSDF *MirrorMaterial::GetBSDF(DifferentialGeometry *dg, const Normal &ng, MemoryArena &arena) const
	{
		Bump(bump, *dg, ng, dg);
		Spectrum r = kr->Evaluate(*dg);
		if(!r.IsBlack())
			return ARENA_ALLOC(arena, SingleBxDF)(ARENA_ALLOC(arena, SpecularReflection)(r, ARENA_ALLOC(arena, FresnelNoOp)()), *dg, ng, true);

		return NULL;
	}
}