/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "Null.h"

namespace Render
{
	NullMaterial::NullMaterial(const RSpectrumTexture &kt)
		:	kt(kt){}

	BSDF *NullMaterial::GetBSDF(DifferentialGeometry *dg, const Normal &ng, MemoryArena &arena) const
	{
		Spectrum t = kt->Evaluate(*dg);
		if(!t.IsBlack())
			return ARENA_ALLOC(arena, SingleBxDF)(ARENA_ALLOC(arena, NullTransmission)(t), *dg, ng);
		else
			return NULL;

	}
}
