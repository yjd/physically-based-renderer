/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_MATERIALS_NULL_H
#define RENDERLIB_MATERIALS_NULL_H

#include "Material.h"

namespace Render
{
	class NullMaterial : public Material
	{
		RSpectrumTexture kt;
	public:
		NullMaterial(const RSpectrumTexture &kt);
		BSDF *GetBSDF(DifferentialGeometry *dg, const Normal &ng, MemoryArena &arena) const;
	};
}

#endif
