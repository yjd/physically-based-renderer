/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "Glass.h"

namespace Render
{
	GlassMaterial::GlassMaterial(const RSpectrumTexture &kr, const RSpectrumTexture &kt, const RFloatTexture &ior, const RFloatTexture &bump)
		:	kr(kr), kt(kt), ior(ior), bump(bump)
	{
		if(!ior)
			this->ior = new ConstantTexture<float>(1.5f);
	}

	BSDF *GlassMaterial::GetBSDF(DifferentialGeometry *dg, const Normal &ng, MemoryArena &arena) const
	{
		Bump(bump, *dg, ng, dg);

		unsigned int nBxDFs = 0;
		BxDF **bxdfs = (BxDF **) arena.Alloc(sizeof(BxDF *) * 2);

		Spectrum r = kt->Evaluate(*dg);
		float index = ior->Evaluate(*dg);
		if(!r.IsBlack())
			bxdfs[nBxDFs++] = ARENA_ALLOC(arena, SpecularTransmission)(r, 1.f, index);

		r = kr->Evaluate(*dg);
		if(!r.IsBlack())
			bxdfs[nBxDFs++] = ARENA_ALLOC(arena, SpecularReflection)(r, ARENA_ALLOC(arena, FresnelDielectric(1.f, index)));

		if(nBxDFs == 2)
			return ARENA_ALLOC(arena, MultiBxDF)(bxdfs, nBxDFs, *dg, ng, true);
		if(nBxDFs == 1)
			return ARENA_ALLOC(arena, SingleBxDF)(bxdfs[0], *dg, ng, true);
		else
			return NULL;
	}
}