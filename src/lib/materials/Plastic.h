/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_MATERIALS_PLASTIC_H
#define RENDERLIB_MATERIALS_PLASTIC_H

#include "Material.h"

namespace Render
{
	//Deprecated in favor of glossy/substrate materials.
	class PlasticMaterial : public Material
	{
		RSpectrumTexture kd;
		RSpectrumTexture ks;
		RFloatTexture roughness;
		RFloatTexture bump;
	public:
		PlasticMaterial(const RSpectrumTexture &kd, const RSpectrumTexture &ks, RFloatTexture roughness, const RFloatTexture &bump = NULL);
		BSDF *GetBSDF(DifferentialGeometry *dg, const Normal &ng, MemoryArena &arena) const;
	};
}

#endif