/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_TEXTURES_BILERP_H
#define RENDERLIB_TEXTURES_BILERP_H

#include "Texture.h"

namespace Render
{
	template<typename T>
	class BilerpTexture : public Texture<T>
	{
		const TextureMapping2D *mapping;
		T v00, v01, v10, v11;
	public:
		BilerpTexture(const TextureMapping2D *mapping, const T &v00 = 0.f, const T &v01 = 1.f, const T &v10 = 0.f, const T &v11 = 1.f)
			:	mapping(mapping), v00(v00), v01(v01), v10(v10), v11(v11){}
		~BilerpTexture(){ delete mapping; }
		T Avg() const
		{
			return (v00 + v01 + v10 + v11) * 0.25f;
		}
		T Evaluate(const DifferentialGeometry &dg) const
		{
			float s, t;
			mapping->Map(dg, &s, &t);
			//Repeat by default.
			s -= floor(s);
			t -= floor(t);
			return (1.f - s) * (1.f - t) * v00 + (1.f - s) * t * v01 + s * (1.f - t) * v10 + s * t * v11;
		}
	};
}

#endif