/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_TEXTURES_IMAGEMAP_H
#define RENDERLIB_TEXTURES_IMAGEMAP_H

#include "Texture.h"
#include "TexImage.h"

namespace Render
{
	template<typename TMemory, typename T>
	class ImageTexture : public Texture<T>
	{
		TextureImage<TMemory> map;
		const TextureMapping2D *mapping;
	public:
		ImageTexture(const TextureMapping2D *mapping, const BlockedArray<TMemory> *data, ImageWrap wrapMode, ImageFilter filter)
			:	mapping(mapping), map(data, wrapMode, filter){}
		~ImageTexture(){ delete mapping; }
		T Evaluate(const DifferentialGeometry &dg) const
		{
			float s, t;
			mapping->Map(dg, &s, &t);
			return map.Lookup(s, t);
		}
		//Very costly, should not be called often.  If at all.
		T Avg() const
		{
			return T(map.Avg());
		}
	};
}

#endif