/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_GEOMETRYPARSERS_PLYPARSER_H
#define RENDERLIB_GEOMETRYPARSERS_PLYPARSER_H

#include "Algebra.h"
#include "rply.h"

namespace Render
{
	void ReadPLY(const char *filename, unsigned int *nV, unsigned int *nT, unsigned int **vi, Point **v, float **uv, Normal **n, Vector **s);
}

#endif
