/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "PLY.h"
#include "Memory.h"

namespace Render
{
	static void ErrorCB(p_ply ply, const char *message)
	{
		Severe("RPly: %s", STRNAMESPACE, "ReadMeshPLY", 1, message);
	}

	static int FaceCB(p_ply_argument argument)
	{
		void *userData = NULL;
		ply_get_argument_user_data(argument, &userData, NULL);

		unsigned int *fd = *static_cast<unsigned int**>(userData);

		long faceIndex;
		ply_get_argument_element(argument, NULL, &faceIndex);

		long length, valueIndex;
		ply_get_argument_property(argument, NULL, &length, &valueIndex);

		if(length != 3)
			Severe("Indices must be of length 3.",STRNAMESPACE,"FaceCB",1);

		if(valueIndex < 0)
			return 1;

		if(valueIndex < 3)
			fd[3 * faceIndex + valueIndex] = static_cast<unsigned int>(ply_get_argument_value(argument));

		return 1;
	}

	static int VertexCB(p_ply_argument arg)
	{
		long userIndex = 0;
		void *userData = NULL;
		ply_get_argument_user_data(arg, &userData, &userIndex);

		Point *p = *static_cast<Point **>(userData);

		long vertIndex;
		ply_get_argument_element(arg, NULL, &vertIndex);

		switch(userIndex)
		{
		case 0:
			p[vertIndex].x = static_cast<float>(ply_get_argument_value(arg));
			break;
		case 1:
			p[vertIndex].y = static_cast<float>(ply_get_argument_value(arg));
			break;
		case 2:
			p[vertIndex].z = static_cast<float>(ply_get_argument_value(arg));
		}

		return 1;
	}

	static int TexCoordCB(p_ply_argument arg)
	{
		long userIndex = 0;
		void *userData = NULL;
		ply_get_argument_user_data(arg, &userData, &userIndex);

		float *uv = *static_cast<float **>(userData);

		long uvIndex;
		ply_get_argument_element(arg, NULL, &uvIndex);

		uv[2 * uvIndex + userIndex] = static_cast<float>(ply_get_argument_value(arg));

		return 1;
	}

	static int NormalCB(p_ply_argument arg)
	{
		long userIndex = 0;
		void *userData = NULL;
		ply_get_argument_user_data(arg, &userData, &userIndex);

		Normal *n = *static_cast<Normal **>(userData);

		long normIndex;
		ply_get_argument_element(arg, NULL, &normIndex);

		switch(userIndex)
		{
		case 0:
			n[normIndex].x = static_cast<float>(ply_get_argument_value(arg));
			break;
		case 1:
			n[normIndex].y = static_cast<float>(ply_get_argument_value(arg));
			break;
		case 2:
			n[normIndex].z = static_cast<float>(ply_get_argument_value(arg));
		}

		return 1;
	}

	static int TangentCB(p_ply_argument arg)
	{
		long userIndex = 0;
		void *userData = NULL;
		ply_get_argument_user_data(arg, &userData, &userIndex);

		Vector *s = *static_cast<Vector **>(userData);

		long sIndex;
		ply_get_argument_element(arg, NULL, &sIndex);

		switch(userIndex)
		{
		case 0:
			s[sIndex].x = static_cast<float>(ply_get_argument_value(arg));
			break;
		case 1:
			s[sIndex].y = static_cast<float>(ply_get_argument_value(arg));
			break;
		case 2:
			s[sIndex].z = static_cast<float>(ply_get_argument_value(arg));
		}

		return 1;
	}

	void ReadPLY(const char *filename, unsigned int *nV, unsigned int *nT, unsigned int **vi, Point **v, float **uv, Normal **n, Vector **s)
	{
		*nV = *nT = 0;
		*vi = NULL;
		*v = NULL;

		if(uv)
			*uv = NULL;
		if(n)
			*n = NULL;
		if(s)
			*s = NULL;

		p_ply ply = NULL;
		try
		{
			if( !(ply = ply_open(filename, ErrorCB, 0, NULL)) )
				Severe("Error reading ply file.",STRNAMESPACE,"ReadPLY",1);

			if(!ply_read_header(ply))
				Severe("Error reading ply header.",STRNAMESPACE,"ReadPLY",1);

			*nV =	ply_set_read_cb(ply, "vertex", "x", VertexCB, v, 0);
					ply_set_read_cb(ply, "vertex", "y", VertexCB, v, 1);
					ply_set_read_cb(ply, "vertex", "z", VertexCB, v, 2);
			if(*nV == 0)
				Severe("No vertices detected in ply file.",STRNAMESPACE,"ReadPLY",1);
			*v = AllocAligned<Point>(*nV);

			*nT = ply_set_read_cb(ply, "face", "vertex_indices", FaceCB, vi, 0);
			if(*nT == 0)
				Severe("No triangle vertex indices detected in ply file.  Other types not supported.",STRNAMESPACE,"ReadPLY",1);
			*vi = AllocAligned<unsigned int>(*nT * 3);

			if(uv)
			{
				// Try both st and uv for texture coordinates.
				long nUVs =	ply_set_read_cb(ply, "vertex", "s", TexCoordCB, uv, 0);
							ply_set_read_cb(ply, "vertex", "t", TexCoordCB, uv, 1);

				if (nUVs <= 0)
				{
					nUVs =	ply_set_read_cb(ply, "vertex", "u", TexCoordCB, uv, 0);
							ply_set_read_cb(ply, "vertex", "v", TexCoordCB, uv, 1);
				}

				if(nUVs > 0)
					*uv = AllocAligned<float>(2 * *nV);
			}

			if(n)
			{
				long nNorms =	ply_set_read_cb(ply, "vertex", "nx", NormalCB, n, 0);
								ply_set_read_cb(ply, "vertex", "ny", NormalCB, n, 1);
								ply_set_read_cb(ply, "vertex", "nz", NormalCB, n, 2);

				if(nNorms > 0)
					*n = AllocAligned<Normal>(*nV);
			}

			if(s)
			{
				long nTangents =	ply_set_read_cb(ply, "vertex","sx", TangentCB, s, 0);
									ply_set_read_cb(ply, "vertex","sy", TangentCB, s, 1);
									ply_set_read_cb(ply, "vertex","sz", TangentCB, s, 2);

				if(nTangents > 0)
					*s = AllocAligned<Vector>(*nV);
			}

			if(!ply_read(ply))
				Severe("An error occured reading \"%s.\"",STRNAMESPACE, "ReadPLY", 1, filename);
		}
		catch(int)
		{
			*nV = *nT = 0;
			FreeAligned(*vi);
			*vi = NULL;
			FreeAligned(*v);
			*v = NULL;

			if(uv)
			{
				FreeAligned(*uv);
				*uv = NULL;
			}
			if(n)
			{
				FreeAligned(*n);
				*n = NULL;
			}
			if(s)
			{
				FreeAligned(*s);
				*s = NULL;
			}
		}

		if(ply)
			ply_close(ply);
	}
}