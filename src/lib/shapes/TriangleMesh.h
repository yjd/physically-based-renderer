/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_SHAPES_TRIANGLEMESH_H
#define RENDERLIB_SHAPES_TRIANGLEMESH_H

#include "Shape.h"

namespace Render
{
	void ReadMeshGeometry(const string &filename, unsigned int *nV, unsigned int *nT, unsigned int **vi, Point **v, float **uv, Normal **n, Vector **s);
	Normal *GetSmoothNormals(const Point *v, const unsigned int *vi, unsigned int nV, unsigned int nT, unsigned int smooth, const float *uv = NULL);

	class Mesh : public Shape
	{
	protected:
		Point *v;
		unsigned int *vi;
		float *uv;
		Normal *n;
		Vector *s;
		vector<Shape *> refinedShapes;
	public:
		Mesh(Point *v = NULL, unsigned int *vi = NULL, float *uv = NULL, Normal *n = NULL, Vector *s = NULL);
		virtual ~Mesh();
		virtual BBox Bounds() const;
		virtual bool CanIntersect() const;
		virtual void Refine(vector<const Shape *> &refined) const;
		friend class Triangle;
	};

	class Triangle : public Shape
	{
		const Mesh *mesh;
		unsigned int *vi;
	public:
		Triangle(const Mesh *mesh, unsigned int *vi);
		BBox Bounds() const;
		bool Intersect(const Ray &ray, Intersection *intersection) const;
		bool IntersectP(const Ray &ray) const;
		Point Sample(float u0, float u1, Intersection *isect) const;
		float Pdf(const Ray &rayWorld, const Ray &ray, const Transform *toWorld, Intersection *isect = NULL) const;
		void GetDifferentialGeometry(const Ray &ray, Intersection *isect, const Transform *toWorld = NULL) const;
		//void GetDifferentialGeometry(const RayDifferential &ray, Intersection *isect, DifferentialGeometry *dgShading, const Transform *toWorld = NULL) const;
		void GetUVs(float uv0[2], float uv1[2], float uv2[2]) const;
		float Area() const;
	};

	class TriangleMesh : public Mesh
	{
	public:
		TriangleMesh(const Transform &toWorld, unsigned int nV, unsigned int nT, unsigned int *vi, Point *v, float *uv, Normal *n, Vector *s, bool makeCopy = false);
	};
}

#endif