#include "volumes/Homogeneous.h"

namespace Render
{
	HomogeneousVolume::HomogeneousVolume(const RSpectrumTexture &sa, const RSpectrumTexture &ss, const RSpectrumTexture &g)
	:	sa(sa), ss(ss), g(g) {}
	Spectrum HomogeneousVolume::SigmaA(const DifferentialGeometry &dg) const
	{
		return sa->Evaluate(dg);
	}

	Spectrum HomogeneousVolume::SigmaS(const DifferentialGeometry &dg) const
	{
		return ss->Evaluate(dg);
	}

	Spectrum HomogeneousVolume::Tau(const Ray &r, float step, float offset) const
	{
		DifferentialGeometry dg(r.o, -r.d);
		Spectrum sigma = SigmaT(dg);
		if(sigma.IsBlack())
			return sigma;

		float dist = r.d.Length() * (r.mint - r.maxt);
		return sigma * dist;
	}
}
