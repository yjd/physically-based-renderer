/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_HOMOGENEOUS_H
#define RENDERLIB_CORE_HOMOGENEOUS_H

#include "Volume.h"
#include "Texture.h"

namespace Render
{
	class HomogeneousVolume : public Volume
	{
		RSpectrumTexture sa, ss, g;
		
	public:
		HomogeneousVolume(const RSpectrumTexture &sa, const RSpectrumTexture &ss, const RSpectrumTexture &g);
		virtual Spectrum SigmaA(const DifferentialGeometry &dg) const;
		virtual Spectrum SigmaS(const DifferentialGeometry &dg) const;
		virtual Spectrum Tau(const Ray &r, float step = 1.f, float offset = .5f) const;
	};
}

#endif
