/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "MonteCarlo.h"
namespace Render
{
	void StratifiedSample1D(float *sample, unsigned int nSamples, RNG &rng, bool jitter)
	{
		float invTotal = 1.f / (float) nSamples;
		for(unsigned int i = 0; i < nSamples; ++i)
		{
			float delta = jitter ? rng.RandF() : 0.5f;
			*sample++ = min((i + delta) * invTotal, ONEMINUSEPSILON);
		}
	}
	void StratifiedSample2D(float *sample, unsigned int nx, unsigned int ny, RNG &rng, bool jitter)
	{
		float invXTotal = 1.f / (float) nx;
		float invYTotal = 1.f / (float) ny;
		for(unsigned int y = 0; y < ny; ++y)
		{
			for(unsigned int x = 0; x < nx; ++x)
			{
				float jx = jitter ? (float) rng.RandF() : 0.5f;
				float jy = jitter ? (float) rng.RandF() : 0.5f;
				*sample++ = min((x + jx) * invXTotal, ONEMINUSEPSILON);
				*sample++ = min((y + jy) * invYTotal, ONEMINUSEPSILON);
			}
		}
	}

	void RejectionSampleDisc(float *x, float *y, RNG &rng)
	{
		do
		{
			*x = 1.f - 2.f * rng.RandF();
			*y = 1.f - 2.f * rng.RandF();
		}while(*x * *x + *y * *y > 1.f);
	}

	Vector UniformSampleHemisphere(float u1, float u2)
	{
		float z = u1;
		float r = sqrt(max(0.f, 1.f - z * z));
		float phi = 2 * M_PI * u2;
		float x = cos(phi) * r;
		float y = sin(phi) * r;
		return Vector(x, y, z);
	}

	Vector UniformSampleSphere(float u1, float u2)
	{
		float z = 1.f - 2.f * u1;
		float r = sqrt(max(0.f, 1.f - z * z));
		float phi = TWOPI * u2;
		float x = r * cos(phi);
		float y = r * sin(phi);
		return Vector(x, y, z);
	}

	void UniformSampleDisc(float u1, float u2, float *x, float *y)
	{
		float r = sqrt(u1);
		float theta = 2 * M_PI * u2;
		*x = r * cos(theta);
		*y = r * sin(theta);
	}

	void ConcentricSampleDisc(float u1, float u2, float *dx, float *dy)
	{
		float a = 2.f * u1 - 1.f;
		float b = 2.f * u2 - 1.f;
		float r, theta;
		if(a == 0.f && b == 0.f)
		{
			*dx = *dy = 0.f;
			return;
		}
		if(a * a > b * b)
		{
			r = a;
			theta = M_PI * 0.25f * b / a;
		}
		else
		{
			r = b;
			theta = M_PI * 0.5f - M_PI * 0.25f * a / b;
		}

		*dx = r * cos(theta);
		*dy = r * sin(theta);
		/*
		float r, theta;

		//Map random numbers to [-1,1]^2
		float sx = 2.f * u1 - 1.f;
		float sy = 2.f * u2 - 1.f;

		if(sx == 0 && sy == 0)
		{
			*dx = *dy = 0.f;
			return;
		}
		if(sx >= -sy)
		{
			if(sx > sy)
			{
				r = sx;
				theta = sy / r;
			}
			else
			{
				r = sy;
				theta = 2.f - sx / r;
			}
		}
		else
		{
			if(sx <= sy)
			{
				r = -sx;
				theta = 4.f - sy / r;
			}
			else
			{
				r = -sy;
				theta = 6.f + sx / r;
			}
		}
		theta *= M_PI * 0.25f;

		*dx = r * cos(theta);
		*dy = r * sin(theta);
		*/
	}

	void UniformSampleTriangle(float u1, float u2, float *u, float *v)
	{
		float su1 = sqrt(u1);
		*u = 1.f - su1;
		*v = u2 * su1;
	}

	Vector UniformSampleCone(float u1, float u2, float costhetamax)
	{
		float costheta = (1.f - u1) + u1 * costhetamax;
		float sintheta = sqrt(1.f - costheta * costheta);
		float phi = u2 * TWOPI;
		return Vector(cos(phi)*sintheta,sin(phi)*sintheta, costheta);
	}

	Distribution2D::Distribution2D(const float *func, int nu, int nv)
	{
		pConditionalV.reserve(nv);
		for(int v = 0; v < nv; ++v)
			pConditionalV.push_back(new Distribution1D(&func[v*nu], nu));

		vector<float> marginalFunc;
		marginalFunc.reserve(nv);
		for(int v = 0; v < nv; ++v)
			marginalFunc.push_back(pConditionalV[v]->funcInt);

		pMarginal = new Distribution1D(&marginalFunc[0], nv);
		invMarginalFuncInt = pMarginal->funcInt > 0.f ? 1.f / pMarginal->funcInt : 0.f;
	}

	Distribution2D::~Distribution2D()
	{
		delete pMarginal;
		for(unsigned int i = 0; i < pConditionalV.size(); ++i)
			delete pConditionalV[i];
	}
}