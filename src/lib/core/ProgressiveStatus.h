/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_PROGRESSIVESTATUS_H
#define RENDERLIB_CORE_PROGRESSIVESTATUS_H

#include "RenderStatus.h"
#include <limits>
#include <boost/date_time/posix_time/posix_time_types.hpp>

namespace Render
{
	class ProgressiveStatus : public RenderStatus
	{
		unsigned int saveInterval;
		unsigned int saveCounter;
		unsigned int nPassesMax;
		const char *filename;
		Film *film;
		boost::posix_time::ptime startTime;
		boost::posix_time::ptime elapsed;
		boost::mutex *statusLock;
	public:
		ProgressiveStatus(Film *film, const char *filename, unsigned int saveInterval = 5, unsigned int nPassesMax = UINT_MAX);
		void RenderStart();
		bool Update(unsigned int nPasses);
		void RequestSave();
		void RequestStop();
		void RequestSaveAndStop();
	};
}
#endif
