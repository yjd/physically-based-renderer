/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_VOLUME_H
#define RENDERLIB_CORE_VOLUME_H

#include "RNG.h"
#include "DifferentialGeometry.h"
#include "Spectrum.h"
#include "Texture.h"

//PhaseIsotropic - Since it's isotropic, is normalized, and must integrate over the entire surface of a sphere, it is equivalent to INV_FOURPI
#define PhaseIsotropic INV_FOURPI

namespace Render
{
	float PhaseRayleigh(const Vector &w, const Vector &wp);
	float PhaseMieHazy(const Vector &w, const Vector &wp);
	float PhaseMieMurky(const Vector &w, const Vector &wp);
	float PhaseHG(const Vector &w, const Vector &wp, float g);
	float PhaseSchlick(const Vector &w, const Vector &wp, float g);

	class Volume : public ReferenceCounted
	{
	public:
		virtual Spectrum SigmaA(const DifferentialGeometry &dg) const = 0;
		virtual Spectrum SigmaS(const DifferentialGeometry &dg) const = 0;
		virtual Spectrum SigmaT(const DifferentialGeometry &dg) const;
		virtual Spectrum Tau(const Ray &r, float step = 1.f, float offset = .5f) const = 0;
	};
}

#endif
