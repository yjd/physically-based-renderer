/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_INTERSECTION_H
#define RENDERLIB_CORE_INTERSECTION_H

#include "Transform.h"
#include "DifferentialGeometry.h"

namespace Render
{
	struct Intersection
	{
		DifferentialGeometry dg;
		Normal ng;
		Transform toWorld;
		const Primitive *prim;
		const Shape *shape;
		vector<float> primitiveData; //Can hold extra data that a primitive may need.
		Intersection();
		BSDF *GetBSDF(const Ray &ray, MemoryArena &arena);
		bool Le(const Vector &w, Spectrum &s) const;
	};
}

#endif