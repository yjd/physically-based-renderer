/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_ALGEBRA_H
#define RENDERLIB_CORE_ALGEBRA_H

#include "Global.h"
namespace Render
{
	struct Vector
	{
		float x,y,z;
		Vector():x(0),y(0),z(0){}
		inline Vector(float x, float y, float z):x(x),y(y),z(z){}
		inline Vector(const Vector &v):x(v.x),y(v.y),z(v.z){}
		explicit Vector(const Point &p);
		explicit Vector(const Normal &n);

		inline float operator[](unsigned int i) const{ return (&x)[i]; }
		inline float &operator[](unsigned int i){ return (&x)[i]; }

		Vector operator+(const Vector &v) const { return Vector(x + v.x, y + v.y, z + v.z); }
		Vector &operator+=(const Vector &v){ x += v.x; y += v.y; z += v.z; return *this; }
		Vector operator-(const Vector &v) const { return Vector(x - v.x, y - v.y, z - v.z); }
		Vector &operator-=(const Vector &v){ x -= v.x; y -= v.y; z -= v.z; return *this; }
		Vector operator*(float s) const { return Vector(s*x, s*y, s*z); }
		Vector &operator*=(float s) { x *= s; y *= s; z *= s; return *this; }
		Vector operator/(float s) const { float inv = 1.f/s;  return Vector(inv*x, inv*y, inv*z); }
		Vector &operator/=(float s) { float inv = 1.f/s;  x *= inv; y *= inv; z *= inv; return *this; }
		Vector operator-() const { return Vector(-x, -y, -z); }

		Vector &operator=(const Vector &v) { x = v.x; y = v.y; z = v.z; return *this; }
		template<typename T>
		void AddWeighted(float w, const T &p)
		{
			x += w * p.x;
			y += w * p.y;
			z += w * p.z;
		}
		float Length2() const { return x * x + y * y + z * z; }
		float Length() const { return sqrt(Length2()); }

		void Normalize() 
		{
			*this /= Length(); 
		}
	};

	inline Vector operator*(float s, const Vector &v) { return v * s; }
	inline float Dot(const Vector &v1, const Vector &v2){ return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z; }
	inline float AbsDot(const Vector &v1, const Vector &v2) { return abs(Dot(v1,v2)); }


	inline Vector Normalize(const Vector &v) { return v / v.Length(); }

	struct Point
	{
		float x,y,z;

		Point():x(0),y(0),z(0){}
		Point(float x, float y, float z):x(x),y(y),z(z){}
		Point(const Point &p){ x = p.x; y = p.y; z = p.z; }
		inline float operator[](unsigned int i) const{ return (&x)[i]; }
		inline float &operator[](unsigned int i){ return (&x)[i]; }

		Point operator+(const Vector &v) const{ return Point(x + v.x, y + v.y, z + v.z); }
		Point operator+(const Point &p) const{ return Point(x + p.x, y + p.y, z + p.z); }
		Point &operator+=(const Vector &v){ x += v.x; y += v.y; z += v.z; return *this; }
		Point &operator+=(const Point &p){ x += p.x; y += p.y; z += p.z; return *this; }
		Vector operator-(const Point &p) const{ return Vector(x - p.x, y - p.y, z - p.z); }
		Point operator-(const Vector &v) const{ return Point(x - v.x, y - v.y, z - v.z); }
		Point &operator-=(const Vector &v){ x -= v.x; y -= v.y; z -= v.z; return *this; }
		Point operator*(float s) const { return Point(s*x,s*y,s*z); }
		Point &operator*=(float s){ x *= s; y *= s; z *= s; return *this; }
		Point operator/(float s) const { float inv = 1.f/s; return Point(x*inv, y*inv, z*inv); }
		Point &operator/=(float s){ float inv = 1.f/s; x *= inv; y *=inv; z *= inv; return *this; }
		Point operator-(){ return Point(-x,-y,-z); }

		Point &operator=(const Point &p){ x = p.x; y = p.y; z = p.z; return *this; }
		template<typename T>
		void AddWeighted(float w, const T &p)
		{
			x += w * p.x;
			y += w * p.y;
			z += w * p.z;
		}
	};

	inline Point operator*(float s, const Point& p){ return Point(s*p.x, s*p.y, s*p.z); }

	inline float Distance2(const Point &p1, const Point &p2){ return (p1 - p2).Length2(); }
	
	inline float Distance(const Point &p1, const Point &p2){ return sqrt(Distance2(p1, p2)); }

	inline Vector::Vector(const Point &p):x(p.x),y(p.y),z(p.z){}

	struct Normal
	{
		float x,y,z;

		Normal():x(0),y(0),z(0){}
		Normal(float x, float y, float z):x(x),y(y),z(z){}
		Normal(const Normal &n):x(n.x),y(n.y),z(n.z){}
		Normal(const Vector &v):x(v.x),y(v.y),z(v.z){}

		Normal operator+(const Normal &n) const { return Normal(x + n.x, y + n.y, z + n.z); }
		Normal &operator+=(const Normal &n){ x += n.x; y += n.y; z += n.z; return *this; }
		Normal operator-(const Normal &n) const { return Normal(x - n.x, y - n.y, z - n.z); }
		Normal &operator-=(const Normal &n){ x -= n.x; y -= n.y; z -= n.z; return *this; }
		Normal operator*(float s) const { return Normal(s*x, s*y, s*z); }
		Normal &operator*=(float s) { x *= s; y *= s; z *= s; return *this; }
		Normal operator/(float s) const { float inv = 1.f/s;  return Normal(inv*x, inv*y, inv*z); }
		Normal &operator/=(float s) { float inv = 1.f/s;  x*=inv; y*=inv; z*=inv; return *this; }
		Normal operator-() const { return Normal(-x, -y, -z); }

		float Length2() const { return x * x + y * y + z * z; }
		float Length() const { return sqrt(Length2()); }

		void Normalize() { *this /= Length(); }
	};

	inline float Dot(const Normal &n1, const Normal &n2){ return n1.x * n2.x + n1.y * n2.y + n1.z * n2.z; }
	inline float AbsDot(const Normal &n1, const Normal &n2){ return abs(Dot(n1,n2)); }

	inline Normal Normalize (const Normal &n) { return n / n.Length(); }
	inline Normal operator*(float s, const Normal &n) { return n * s; }

	inline Vector::Vector(const Normal &n):x(n.x),y(n.y),z(n.z){}

	inline float Dot(const Vector &v, const Normal &n){ return v.x * n.x +  v.y * n.y + v.z * n.z; }
	inline float Dot(const Normal &n, const Vector &v){ return Dot(v,n); }
	inline float Dot(const Normal &n, const Point &p){ return n.x * p.x + n.y * p.y + n.z * p.z; }
	inline float Dot(const Point &p, const Normal &n){ return Dot(n, p); }
	inline float AbsDot(const Vector &v, const Normal &n) { return abs(Dot(v,n)); }
	inline float AbsDot(const Normal &n, const Vector &v) { return AbsDot(v,n); }
	inline float AbsDot(const Normal &n, const Point &p){ return abs(Dot(n, p)); }
	inline float AbsDot(const Point &p, const Normal &n){ return AbsDot(n, p); }

	struct Ray
	{
		Point o;
		Vector d;
		mutable float mint, maxt;
		float time;
		Ray():mint(0), maxt(INFINITY), time(0.f){}
		Ray(const Point &o, const Vector &d, float mint = 0.f, float maxt = INFINITY, float time = 0.f):o(o), d(d), mint(mint), maxt(maxt), time(time){}

		Point operator()(float t) const { Point p = o; p.AddWeighted(t, d); return p; }
	};

	struct RayDifferential : public Ray
	{
		Point rxOrigin, ryOrigin;
		Vector rxDirection, ryDirection;
		bool hasDifferentials;

		RayDifferential():hasDifferentials(false){}
		RayDifferential(const Point &o, const Vector &d, float mint = 0.f, float maxt = INFINITY, float time = 0.f)
			:Ray(o, d, mint, maxt, time), hasDifferentials(false){}

		explicit RayDifferential(const Ray &ray) : Ray(ray), hasDifferentials(false){ }

		void ScaleDifferentials(float s)
		{
			rxOrigin = o + (rxOrigin - o) * s;
			ryOrigin = o + (ryOrigin - o) * s;
			rxDirection = d + (rxDirection - d) * s;
			ryDirection = d + (ryDirection - d) * s;
		}
	};

	struct BBox
	{
		Point pMin, pMax;

		BBox():pMin(Point(INFINITY, INFINITY, INFINITY)), pMax(Point(-INFINITY, -INFINITY, -INFINITY)){}
		BBox(const Point &p):pMin(p),pMax(p){}
		BBox(const Point &p1, const Point &p2)
			:pMin(Point(min(p1.x, p2.x), min(p1.y, p2.y), min(p1.z, p2.z))),
			 pMax(Point(max(p1.x, p2.x), max(p1.y, p2.y), max(p1.z, p2.z))){}

		friend BBox Union(const BBox &b, const Point &p);

		friend BBox Union(const BBox &b1, const BBox &b2);

		bool Overlaps(const BBox &b) const
		{
			return	((pMax.x >= b.pMin.x) && (pMin.x <= b.pMax.y)) &&
					((pMax.y >= b.pMin.y) && (pMin.y <= b.pMax.y)) &&
					((pMax.z >= b.pMin.z) && (pMin.z <= b.pMax.z));
		}

		bool Inside(const Point &p) const
		{
			return	((p.x >= pMin.x) && (p.x <= pMax.x)) &&
					((p.y >= pMin.y) && (p.y <= pMax.y)) &&
					((p.z >= pMin.z) && (p.z <= pMax.z));
		}

		void Expand(float delta)
		{
			pMin -= Vector(delta, delta, delta);
			pMax += Vector(delta, delta, delta);
		}

		float SurfaceArea() const
		{
			Vector d = pMax - pMin;
			return 2.f * (d.x * d.y + d.x * d.z + d.y * d.z);
		}

		float Volume() const
		{
			Vector d = pMax - pMin;
			return d.x * d.y * d.z;
		}

		int MaximumExtent() const
		{
			Vector d = pMax - pMin;
			if(d.x > d.y && d.x > d.z)
				return 0;
			else if(d.y > d.z)
				return 1;
			else
				return 2;
		}

		inline const Point &operator[](int i) const { return (&pMin)[i]; }
		inline Point &operator[](int i) { return (&pMin)[i]; }

		Point Lerp(float tx, float ty, float tz) const
		{
			return Point(	Render::Lerp(tx, pMin.x, pMax.x), 
							Render::Lerp(ty, pMin.y, pMax.y), 
							Render::Lerp(tz, pMin.z, pMax.z));
		}

		Vector Offset(const Point &p) const
		{
			return Vector(	(p.x - pMin.x) / (pMax.x - pMin.x),
							(p.y - pMin.y) / (pMax.y - pMin.y),
							(p.z - pMin.z) / (pMax.z - pMin.z));
		}

		void BoundingSphere(Point *c, float *r) const
		{
			*c = 0.5f * pMin + 0.5f * pMax;
			*r = Inside(*c) ? Distance(*c, pMax) : 0;
		}

		bool IntersectP(const Ray &ray, float *hitt0 = NULL, float *hitt1 = NULL) const;
	};

	struct Mat4
	{
		static const Mat4 identity;
		
		union
		{
			float m[4][4];
			float mm[16];
		};

		Mat4(const float m[4][4])
		{
			memcpy(this->m, m, sizeof(float)*16);
		}

		Mat4(const float mm[16])
		{
			memcpy(this->mm, mm, sizeof(float)*16);
		}
	
		Mat4(const Mat4 &m)
		{
			memcpy(this->mm, m.mm, sizeof(float)*16);
		}

		Mat4()
		{
			memcpy(m, identity.m, sizeof(float) * 16);
		}

		Mat4(	float t00, float t01, float t02, float t03,
				float t10, float t11, float t12, float t13,
				float t20, float t21, float t22, float t23,
				float t30, float t31, float t32, float t33)
		{
			m[0][0] = t00; m[0][1] = t01; m[0][2] = t02; m[0][3] = t03;
			m[1][0] = t10; m[1][1] = t11; m[1][2] = t12; m[1][3] = t13;
			m[2][0] = t20; m[2][1] = t21; m[2][2] = t22; m[2][3] = t23;
			m[3][0] = t30; m[3][1] = t31; m[3][2] = t32; m[3][3] = t33;
		}

		Mat4 Transpose()
		{
			return Mat4(m[0][0], m[1][0], m[2][0], m[3][0],
						m[0][1], m[1][1], m[2][1], m[3][1],
						m[0][2], m[1][2], m[2][2], m[3][2],
						m[0][3], m[1][3], m[2][3], m[3][3]);
		}

		Mat4 operator*(const Mat4 &m) const
		{
			Mat4 r;
			for(int j = 0; j < 4; ++j)
			{
				for(int i = 0; i < 4; ++i)
				{
					r.m[i][j] = this->m[i][0] * m.m[0][j] +
								this->m[i][1] * m.m[1][j] +
								this->m[i][2] * m.m[2][j] +
								this->m[i][3] * m.m[3][j];
				}
			}
			return r;
		}

		inline float *operator[](unsigned int r)
		{
			return m[r];
		}

		inline const float *operator[](unsigned int r) const
		{
			return m[r];
		}

		inline bool operator==(const Mat4 &mOther) const
		{
			return memcmp(mm, mOther.mm, sizeof(float) * 16) == 0;
		}

		inline bool operator!=(const Mat4 &mOther) const
		{
			return !((*this) == mOther);
		}

		Mat4 &operator=(const Mat4 &m)
		{
			memcpy(this->m, m.m, sizeof(float)*16);
			return *this;
		}
		/* Assuming &v != vTrans. */
		inline void operator()(const Vector &v, Vector *vTrans) const;
		inline Vector operator()(const Vector &v) const;
		/* Assuming &p != pTrans. */
		inline void operator()(const Point &p, Point *pTrans) const;
		inline Point operator()(const Point &p) const;
		/* It is important to note that to properly transform a normal given matrix M, the transpose of the inverse of
		 * M, (M^-1)^T must be used.  This just multiplies the transpose of M.  I.E., the current matrix is the inverse
		 * of the desired matrix transform on n.  Assuming &n != nTrans.
		 */
		inline void operator()(const Normal &n, Normal *nTrans) const;
		inline Normal operator()(const Normal &n) const;
		/* Assuming &r != rTrans */
		inline void operator()(const Ray &r, Ray *rTrans) const;
		inline Ray operator()(const Ray &r) const;
		/* Assuming &r != rTrans */
		inline void operator()(const RayDifferential &r, RayDifferential *rTrans) const;
		inline RayDifferential operator()(const RayDifferential &r) const;
		/* Assuming &b != bTrans */
		void operator()(const BBox &b, BBox *bTrans) const;
		inline BBox operator()(const BBox &b) const;

		void Inverse(Mat4 &m) const;

		Mat4 Inverse() const;
	};

	inline void Mat4::operator()(const Vector &v, Vector *vTrans) const
	{
		vTrans->x = m[0][0] * v.x + m[0][1] * v.y + m[0][2] * v.z;
		vTrans->y = m[1][0] * v.x + m[1][1] * v.y + m[1][2] * v.z;
		vTrans->z = m[2][0] * v.x + m[2][1] * v.y + m[2][2] * v.z;
	}
	inline Vector Mat4::operator()(const Vector &v) const
	{
		return Vector(	m[0][0] * v.x + m[0][1] * v.y + m[0][2] * v.z,
						m[1][0] * v.x + m[1][1] * v.y + m[1][2] * v.z,
						m[2][0] * v.x + m[2][1] * v.y + m[2][2] * v.z);
	}
	inline void Mat4::operator()(const Point &p, Point *pTrans) const
	{
		pTrans->x = m[0][0] * p.x + m[0][1] * p.y + m[0][2] * p.z + m[0][3];
		pTrans->y = m[1][0] * p.x + m[1][1] * p.y + m[1][2] * p.z + m[1][3];
		pTrans->z = m[2][0] * p.x + m[2][1] * p.y + m[2][2] * p.z + m[2][3];
		float w	= m[3][0] * p.x + m[3][1] * p.y + m[3][2] * p.z + m[3][3];
		if(w != 1.f)
			(*pTrans) /= w;
	}
	inline Point Mat4::operator()(const Point &p) const
	{
		Point ret(	m[0][0] * p.x + m[0][1] * p.y + m[0][2] * p.z + m[0][3],
					m[1][0] * p.x + m[1][1] * p.y + m[1][2] * p.z + m[1][3],
					m[2][0] * p.x + m[2][1] * p.y + m[2][2] * p.z + m[2][3]);
		float w = m[3][0] * p.x + m[3][1] * p.y + m[3][2] * p.z + m[3][3];
		if(w != 1.f)
			ret /= w;

		return ret;
	}
	inline void Mat4::operator()(const Normal &n, Normal *nTrans) const
	{
		nTrans->x = m[0][0] * n.x + m[1][0] * n.y + m[2][0] * n.z;
		nTrans->y = m[0][1] * n.x + m[1][1] * n.y + m[2][1] * n.z;
		nTrans->z = m[0][2] * n.x + m[1][2] * n.y + m[2][2] * n.z;
	}
	inline Normal Mat4::operator()(const Normal &n) const
	{
		return Normal(	m[0][0] * n.x + m[1][0] * n.y + m[2][0] * n.z,
						m[0][1] * n.x + m[1][1] * n.y + m[2][1] * n.z,
						m[0][2] * n.x + m[1][2] * n.y + m[2][2] * n.z);
	}
	inline void Mat4::operator()(const Ray &r, Ray *rTrans) const
	{
		this->operator()(r.o, &rTrans->o);
		this->operator()(r.d, &rTrans->d);
		rTrans->mint = r.mint;
		rTrans->maxt = r.maxt;
		rTrans->time = r.time;
	}
	inline Ray Mat4::operator()(const Ray &r) const
	{
		return Ray(this->operator()(r.o), this->operator()(r.d), r.mint, r.maxt, r.time);
	}
	inline void Mat4::operator()(const RayDifferential &r, RayDifferential *rTrans) const
	{
		this->operator()(r.o, &rTrans->o);
		this->operator()(r.d, &rTrans->d);
		rTrans->mint = r.mint;
		rTrans->maxt = r.maxt;
		rTrans->time = r.time;
		rTrans->hasDifferentials = r.hasDifferentials;
		if(r.hasDifferentials)
		{
			this->operator()(r.rxOrigin, &rTrans->rxOrigin);
			this->operator()(r.ryOrigin, &rTrans->ryOrigin);
			this->operator()(r.rxDirection, &rTrans->rxDirection);
			this->operator()(r.ryDirection, &rTrans->ryDirection);
		}
	}
	inline RayDifferential Mat4::operator()(const RayDifferential &r) const
	{
		RayDifferential ret(this->operator()(r.o), this->operator()(r.d), r.mint, r.maxt, r.time);
		ret.hasDifferentials = r.hasDifferentials;
		if(r.hasDifferentials)
		{
			this->operator()(r.rxOrigin, &ret.rxOrigin);
			this->operator()(r.ryOrigin, &ret.ryOrigin);
			this->operator()(r.rxDirection, &ret.rxDirection);
			this->operator()(r.ryDirection, &ret.ryDirection);
		}
		return ret;
	}
	inline BBox Mat4::operator()(const BBox &b) const
	{
		BBox ret;
		this->operator()(b, &ret);
		return ret;
	}
	inline Vector Cross(const Vector &v1, const Vector &v2)
	{
		return Vector(	v1.y * v2.z - v1.z * v2.y,
						v1.z * v2.x - v1.x * v2.z,
						v1.x * v2.y - v1.y * v2.x );
	}
	inline Vector Cross(const Vector &v, const Normal &n)
	{
		return Vector(	v.y * n.z - v.z * n.y,
						v.z * n.x - v.x * n.z,
						v.x * n.y - v.y * n.x);
	}
	inline Vector Cross(const Normal &n, const Vector &v)
	{
		return Vector(	n.y * v.z - n.z * v.y,
						n.z * v.x - n.x * v.z,
						n.x * v.y - n.y * v.x);
	}

	inline void CoordinateSystem(const Vector &v1, Vector* v2, Vector* v3)
	{
		if(v1.x != 0 || v1.z != 0)
		{
			float invLen = 1.f / sqrt(v1.x*v1.x + v1.z*v1.z);
			*v2 = Vector(v1.z * invLen, 0, -v1.x * invLen);
		}
		else
			//At this point v1 can only be 0,y,0 or 0,-y,0.
			*v2 = Vector(v1.y > 0.f ? 1.f : -1.f, 0, 0);
		*v3 = Cross(v1, *v2);
	}

	inline Vector SphericalDirection(float sintheta, float costheta, float phi)
	{
		return Vector(sintheta * cos(phi), sintheta * sin(phi), costheta);
	}

	inline Vector SphericalDirection(float sintheta, float costheta, float phi, const Vector &x, const Vector &y, const Vector &z)
	{
		return sintheta * cos(phi) * x + sintheta * sin(phi) * y + costheta * z;
	}

	inline float SphericalTheta(const Vector &v)
	{
		return acos(Clamp(v.z, -1.f, 1.f));
	}

	inline float SphericalPhi(const Vector &v)
	{
		float p = atan2(v.y, v.x);
		return (p < 0) ? p + TWOPI : p;
	}

#ifdef _DEBUG
	ostream& operator<<(ostream &c, const Mat4 &m);
	ostream& operator<<(ostream &c, const Point &p);
	ostream& operator<<(ostream &c, const Vector &v);
	ostream& operator<<(ostream &c, const Normal &n);
#endif

	static inline bool QuickBoxIntersect(const BBox &bounds, const Ray &ray, const Vector &invDir, const uint32_t isNeg[3])
	{
		float tmin =	(bounds[	isNeg[0]].x - ray.o.x) * invDir.x;
		float tmax =	(bounds[1 -	isNeg[0]].x - ray.o.x) * invDir.x;
		float ttmin =	(bounds[	isNeg[1]].y - ray.o.y) * invDir.y;
		float ttmax =	(bounds[1 -	isNeg[1]].y - ray.o.y) * invDir.y;
	
		if((ttmin > tmax) || (ttmax < tmin))
			return false;

		if(tmin < ttmin) tmin = ttmin;
		if(tmax > ttmax) tmax = ttmax;

		ttmin =			(bounds[	isNeg[2]].z - ray.o.z) * invDir.z;
		ttmax =			(bounds[1 -	isNeg[2]].z - ray.o.z) * invDir.z;
	
		if((ttmin > tmax) || (ttmax < tmin))
			return false;

		if(tmin < ttmin) tmin = ttmin;
		if(tmax > ttmax) tmax = ttmax;

		return tmin < ray.maxt && tmax > ray.mint;
	}

	bool SolveLinearSystem2x2(const float A[2][2], const float B[2], float *x0, float *x1);

	inline void SetSegment(Ray *ray, const Point &p, const Vector &wi, float dist, float eps = RAYEPSILON)
	{
		if(dist == INFINITY)
			*ray = Ray(p, wi, eps, INFINITY, ray->time);
		else
			*ray = Ray(p, wi, dist * eps, dist * (1.f - eps), ray->time);
	}
	inline void SetSegment(Ray *ray, const Point &p0, const Point &p1, float eps = RAYEPSILON)
	{
		Vector d = p1 - p0;
		float dist = d.Length();
		d /= dist;
		SetSegment(ray, p0, d, dist, eps);
	}
}
#endif
