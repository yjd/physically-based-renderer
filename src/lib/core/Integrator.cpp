/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "Integrator.h"

namespace Render
{
	PathVertex::PathVertex(const Spectrum &alpha, const Point &p, const Vector &w, BSDF *bsdf, float misFactors, float misConnect, bool specular)
		:	alpha(alpha), p(p), w(w), bsdf(bsdf), misFactors(misFactors), misConnect(misConnect), specular(specular)
	{
	}

	Integrator::~Integrator(){}
	void Integrator::RequestSamples(Sampler *sampler, Integrator *surf, Integrator *vol, const Scene *scene)
	{
		unsigned int reqs[4];
		surf->requiredSamples(sampler, scene, &reqs[0], &reqs[2]);
		vol->requiredSamples(sampler, scene, &reqs[1], &reqs[3]);
		unsigned int offsets[4];
		sampler->RequestSamples(&reqs[0], &reqs[2], &offsets[0], &offsets[2], 2);
		surf->setSampleOffsets(offsets[0], offsets[2]);
		vol->setSampleOffsets(offsets[1], offsets[3]);
	}
	bool SampleOneLight(const Renderer *renderer, const Scene *scene, 
						Sample *sample, uint32_t offset, const BSDF *bsdf,
						const Point &p, Ray &ray, Intersection &isect, Spectrum &s, MemoryArena &arena)
	{
		unsigned int which = min((unsigned int)(sample->sampler->GetSample(sample, offset++) * scene->lightRoot->Size()), scene->lightRoot->Size() - 1);
		const TransformedLight *tLight = &scene->lights[which];
		if(tLight->light->EstimateDirect(renderer, scene, sample, offset, bsdf, BxDFType::NON_SPECULAR, tLight->toWorld, p, ray, isect, s, arena))
		{
			s *= scene->lightRoot->Size();
			return true;
		}

		return false;
	}
}