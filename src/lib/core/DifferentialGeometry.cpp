/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "DifferentialGeometry.h"

namespace Render
{
	DifferentialGeometry::DifferentialGeometry(const Point &p, const Normal &n)
		:	p(p), n(n), u(0), v(0){}//, dudx(0), dudy(0), dvdx(0), dvdy(0){}

	DifferentialGeometry::DifferentialGeometry(const Point &p, const Vector &dpdu, const Vector &dpdv, float u, float v)
		:	p(p), dpdu(dpdu), dpdv(dpdv), n(Normalize(Cross(dpdu, dpdv))), u(u), v(v){}/*,
				dudx(0), dudy(0), dvdx(0), dvdy(0){}*/

	DifferentialGeometry::DifferentialGeometry(const Point &p, const Vector &dpdu, const Vector &dpdv, const Normal &n, float u, float v)
		:	p(p), dpdu(dpdu), dpdv(dpdv), n(n), u(u), v(v){}/*,
				dudx(0), dudy(0), dvdx(0), dvdy(0){}*/
	/*
	void DifferentialGeometry::ComputeDifferentials(const RayDifferential &ray)
	{
		float d = -Dot(n, p);

		float tx = -(Dot(n, ray.rxOrigin) + d) / Dot(n, ray.rxDirection);
		float ty = -(Dot(n, ray.ryOrigin) + d) / Dot(n, ray.ryDirection);

		Point px = ray.rxOrigin + tx * ray.rxDirection;
		Point py = ray.ryOrigin + ty * ray.ryDirection;

		dpdx = px - p;
		dpdy = py - p;

		int axes[2];
		if(abs(n.x) > abs(n.y) && abs(n.x) > abs(n.z))
		{
			axes[0] = 1;
			axes[1] = 2;
		}
		else if(abs(n.y) > abs(n.z))
		{
			axes[0] = 0;
			axes[1] = 2;
		}
		else
		{
			axes[0] = 0;
			axes[1] = 1;
		}

		float A[2][2], B[2];
		
		A[0][0] = dpdu[axes[0]]; A[0][1] = dpdv[axes[0]];
		A[1][0] = dpdu[axes[1]]; A[1][1] = dpdv[axes[1]];

		B[0] = dpdx[axes[0]]; B[1] = dpdx[axes[1]];
		if(!SolveLinearSystem2x2(A, B, &dudx, &dvdx))
			dudx = dvdx = 0.f;

		B[0] = dpdy[axes[0]]; B[1] = dpdy[axes[1]];
		if(!SolveLinearSystem2x2(A, B, &dudy, &dvdy))
			dudy = dvdy = 0.f;
	}*/
}
