/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "Volume.h"

namespace Render
{
	float PhaseRayleigh(const Vector &w, const Vector &wp)
	{
		float costheta = Dot(w, wp);
		return  .75f * INV_FOURPI * (1.f + costheta * costheta);
	}
	float PhaseMieHazy(const Vector &w, const Vector &wp)
	{
		float costheta = Dot(w, wp);
		return (.5f + 4.5f * pow(.5f * (1.f + costheta), 8.f)) * INV_FOURPI;
	}
	float PhaseMieMurky(const Vector &w, const Vector &wp)
	{
		float costheta = Dot(w, wp);
		return (.5f + 16.5f * pow(.5f * (1.f + costheta), 32.f)) * INV_FOURPI;
	}
	float PhaseHG(const Vector &w, const Vector &wp, float g)
	{
		float costheta = Dot(w, wp);
		return INV_FOURPI * (1.f - g * g) / pow(1.f + g * g - 2.f * g * costheta, 1.5f);
	}
	float PhaseSchlick(const Vector &w, const Vector &wp, float g)
	{
		float k = 1.45352f * g - .45352f * g * g * g;
		float d = 1.f - k * Dot(w, wp);
		return INV_FOURPI * (1.f - k * k) / (d * d);
	}

	Spectrum Volume::SigmaT(const DifferentialGeometry &dg) const
	{
		return SigmaA(dg) + SigmaS(dg);
	}
}
