/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_SHAPE_H
#define RENDERLIB_CORE_SHAPE_H

#include "Memory.h"
#include "Intersection.h"

namespace Render
{
	class Shape : public ReferenceCounted
	{
	public:
		virtual ~Shape();
		virtual BBox Bounds() const = 0;
		virtual bool Intersect(const Ray &ray, Intersection *isect) const;
		virtual bool IntersectP(const Ray &ray) const;
		virtual bool CanIntersect() const;
		virtual Point Sample(float u0, float u1, Intersection *isect) const;
		virtual float Pdf(const Ray &rayWorld, const Ray &ray, const Transform *toWorld, Intersection *isect = NULL) const;
		virtual float Pdf(const Point &p, const Vector &wi, const Point &pShape, const Normal &n) const;
		virtual float Pdf(const Point &p) const;
		virtual void GetDifferentialGeometry(const Ray &ray, Intersection *isect, const Transform *toWorld = NULL) const;
		//virtual void GetDifferentialGeometry(const RayDifferential &ray, Intersection *isect, DifferentialGeometry *dgShading, const Transform *toWorld = NULL) const;
		virtual float Area() const;
		virtual void Refine(vector<const Shape *> &refined) const;
	};
};

#endif