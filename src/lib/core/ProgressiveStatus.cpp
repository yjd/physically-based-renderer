#include "ProgressiveStatus.h"
#include "Film.h"
#include <boost/thread.hpp>
#include <boost/interprocess/sync/interprocess_semaphore.hpp>

namespace Render
{
	ProgressiveStatus::ProgressiveStatus(Film *film, const char *filename, unsigned int saveInterval, unsigned int nPassesMax)
		:	film(film), filename(filename), saveInterval(saveInterval), nPassesMax(nPassesMax), statusLock(new boost::mutex()){}

	void ProgressiveStatus::RequestSave()
	{
		boost::lock_guard<boost::mutex>(*statusLock);
		saveCounter = 0;
	}

	void ProgressiveStatus::RequestStop()
	{
		boost::lock_guard<boost::mutex>(*statusLock);
		nPassesMax = 0;
	}

	void ProgressiveStatus::RequestSaveAndStop()
	{
		boost::lock_guard<boost::mutex>(*statusLock);
		nPassesMax = 0;
		saveCounter = 0;
	} 

	void ProgressiveStatus::RenderStart()
	{
		startTime = boost::posix_time::microsec_clock::local_time();
		saveCounter = saveInterval;
		elapsed = boost::posix_time::microsec_clock::local_time();
		Info("Rendering started %s %d, %d %02d:%02d:%02d.",
			startTime.date().month().as_short_string(),
			startTime.date().day().as_number(),
			(unsigned int)(startTime.date().year()),
			(unsigned int)(startTime.time_of_day().hours()),
			(unsigned int)(startTime.time_of_day().minutes()),
			(unsigned int)(startTime.time_of_day().seconds()));
	}

	bool ProgressiveStatus::Update(unsigned int nPasses)
	{
		auto now = boost::posix_time::microsec_clock::local_time();
		auto passElapsed = now - elapsed;
		auto totalElapsed = now - startTime;
		elapsed = now;
		
		{
			boost::lock_guard<boost::mutex>(*statusLock);
			--saveCounter;
			if(saveCounter == 0)
			{
				film->WriteImage(filename);
				saveCounter = saveInterval;
			}

			if(nPasses < nPassesMax)
			{
				printf("\rPass %d - Elapsed %02d.%06d seconds | Total: %d:%02d:%02d.%06d.%c", nPasses,
					(unsigned int)(passElapsed.total_seconds()),
					(unsigned int)(passElapsed.fractional_seconds()),
					(unsigned int)(totalElapsed.hours()),
					(unsigned int)(totalElapsed.minutes()),
					(unsigned int)(totalElapsed.seconds()),
					(unsigned int)(totalElapsed.fractional_seconds()),
					'\0');
				fflush(stdout);

				return true;
			}
			else
			{
				printf("Pass %d - Elapsed %02d.%06d seconds | Total: %d:%02d:%02d.%06d.\n", nPasses,
					(unsigned int)(passElapsed.total_seconds()),
					(unsigned int)(passElapsed.fractional_seconds()),
					(unsigned int)(totalElapsed.hours()),
					(unsigned int)(totalElapsed.minutes()),
					(unsigned int)(totalElapsed.seconds()),
					(unsigned int)(totalElapsed.fractional_seconds()));
				fflush(stdout);

				return false;
			}
		}
	}
}
