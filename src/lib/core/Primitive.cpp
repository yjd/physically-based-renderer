/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "Primitive.h"
#include "Material.h"
#include "Light.h"
#include "Volume.h"

namespace Render
{
	Primitive::~Primitive(){}
	bool Primitive::CanIntersect() const
	{
		return true;
	}
	void Primitive::Refine(vector<const Primitive *> &refined) const
	{
		Severe("Unimplemented function called.", "Primitive", "Refine", 1);
	}
	void Primitive::Refine(vector<Primitive *> &refined) const
	{
		Severe("Unimplemented function called.", "Primitive", "Refine", 1);
	}/*
	void Primitive::FullyRefine(vector<Reference<Primitive>> &refined) const
	{
		vector<Reference<Primitive>> todo;
		todo.push_back(const_cast<Primitive *>(this));
		while(todo.size() > 0)
		{
			Reference<Primitive> prim = todo.back();
			todo.pop_back();

			if(prim->CanIntersect())
				refined.push_back(prim);
			else
				prim->Refine(todo);
		}
	}*/
	BSDF *Primitive::GetBSDF(Intersection *intersection, MemoryArena &arena) const
	{
		return NULL;
	}
	const AreaLight *Primitive::GetAreaLight() const
	{
		return NULL;
	}
	float Primitive::Pdf(const Ray &rayWorld, const Ray &ray, const Transform *toWorld, Intersection *isect) const
	{
		Error("Unimplemented function called.", "Primitive", "Pdf", 1);
		return 0.f;
	}

	GeometricPrimitive::GeometricPrimitive(const Reference<Shape> &shape, const Reference<Material> &material, const Reference<Volume> &inner, const Reference<Volume> &outer)
		:	shape(shape), material(material), areaLight(NULL), inner(inner), outer(outer){}
	BBox GeometricPrimitive::Bounds() const
	{
		return shape->Bounds();
	}
	bool GeometricPrimitive::CanIntersect() const
	{
		return shape->CanIntersect();
	}
	bool GeometricPrimitive::Intersect(const Ray &ray, Intersection *intersection) const
	{
		if(shape->Intersect(ray, intersection))
		{
			intersection->shape = shape.GetPtr();
			intersection->prim = this;
			return true;
		}
		return false;
	}
	bool GeometricPrimitive::IntersectP(const Ray &ray) const
	{
		return shape->IntersectP(ray);
	}
	/*
	void GeometricPrimitive::Refine(vector<Reference<Primitive>> &refined) const
	{
		vector<Reference<Shape>> refinedShapes;
		shape->Refine(refinedShapes);
		for(size_t i = 0; i < refinedShapes.size(); ++i)
			refined.push_back(Reference<Primitive>(new GeometricPrimitive(refinedShapes[i], material, areaLight)));
	}
	*/

	float GeometricPrimitive::Pdf(const Ray &rayWorld, const Ray &ray, const Transform *toWorld, Intersection *isect) const
	{
		return shape->Pdf(rayWorld, ray, toWorld, isect);
	}

	BSDF *GeometricPrimitive::GetBSDF(Intersection *intersection, MemoryArena &arena) const
	{
		if(material)
			return material->GetBSDF(&intersection->dg, intersection->ng, arena);

		return NULL;
	}
	const AreaLight *GeometricPrimitive::GetAreaLight() const
	{
		return areaLight;
	}

	const Volume *GeometricPrimitive::InnerVolume() const
	{
		return inner.GetPtr();
	}

	const Volume *GeometricPrimitive::OuterVolume() const
	{
		return outer.GetPtr();
	}

	RefinedPrimitive::RefinedPrimitive(RefinablePrimitive *parent, const Shape *shape)
		:	parent(parent), shape(shape)
	{
	}
	BBox RefinedPrimitive::Bounds() const
	{
		return shape->Bounds();
	}
	bool RefinedPrimitive::CanIntersect() const
	{
		return shape->CanIntersect();
	}
	bool RefinedPrimitive::Intersect(const Ray &ray, Intersection *intersection) const
	{
		if(shape->Intersect(ray, intersection))
		{
			intersection->shape = shape;
			intersection->prim = parent;
			return true;
		}
		return false;
	}
	bool RefinedPrimitive::IntersectP(const Ray &ray) const
	{
		return shape->IntersectP(ray);
	}
	BSDF *RefinedPrimitive::GetBSDF(Intersection *intersection, MemoryArena &arena) const
	{
		return parent->GetBSDF(intersection, arena);
	}
	const AreaLight *RefinedPrimitive::GetAreaLight() const
	{
		return parent->GetAreaLight();
	}
	float RefinedPrimitive::Pdf(const Ray &rayWorld, const Ray &ray, const Transform *toWorld, Intersection *isect) const
	{
		return shape->Pdf(rayWorld, ray, toWorld, isect);
	}

	RefinablePrimitive::RefinablePrimitive(const Reference<Shape> &shape, const Reference<Material> &material, const Reference<Volume> &inner, const Reference<Volume> &outer)
		:	GeometricPrimitive(shape, material, inner, outer)
	{
		if(!shape->CanIntersect())
		{
			vector<const Shape *> shapes;
			shape->Refine(shapes);

			RefinedPrimitive *prims = AllocAligned<RefinedPrimitive>(shapes.size());
			for(size_t i = 0; i < shapes.size(); ++i)
			{
				new (&prims[i]) RefinedPrimitive(this, shapes[i]);
				refinedPrims.push_back(&prims[i]);
			}
		}
	}
	RefinablePrimitive::~RefinablePrimitive()
	{
		if(!refinedPrims.empty())
			FreeAligned(refinedPrims.front());
	}

	void RefinablePrimitive::Refine(vector<const Primitive *> &refined) const
	{
		refined.insert(refined.end(), refinedPrims.begin(), refinedPrims.end());
	}

	void RefinablePrimitive::Refine(vector<Primitive *> &refined) const
	{
		refined.insert(refined.end(), refinedPrims.begin(), refinedPrims.end());
	}
	
	AnimatedPrimitive::AnimatedPrimitive(const AnimatedTransform &toWorld, const Reference<Primitive> &primitive)
		:	toWorld(toWorld), primitive(primitive){}
	BBox AnimatedPrimitive::Bounds() const
	{
		return toWorld.MotionBounds(primitive->Bounds());
	}
	bool AnimatedPrimitive::Intersect(const Ray &ray, Intersection *intersection) const
	{
		Transform t;
		toWorld.Interpolate(ray.time, &t);
		Ray r;
		t(ray, &r, true);
		
		if(!primitive->Intersect(r, intersection))
			return false;

		intersection->toWorld = t * intersection->toWorld;
		ray.maxt = r.maxt;
		return true;
	}
	bool AnimatedPrimitive::IntersectP(const Ray &ray) const
	{
		return primitive->IntersectP(toWorld(ray.time, ray, true));
	}

	TransformedPrimitive::TransformedPrimitive(const Transform &toWorld, const Reference<Primitive> &primitive)
		:	toWorld(toWorld), primitive(primitive){}
	BBox TransformedPrimitive::Bounds() const
	{
		return toWorld(primitive->Bounds());
	}
	bool TransformedPrimitive::Intersect(const Ray &ray, Intersection *intersection) const
	{
		Ray r;
		toWorld(ray, &r, true);
		if(!primitive->Intersect(r, intersection))
			return false;

		intersection->toWorld = toWorld * intersection->toWorld;
		ray.maxt = r.maxt;
		return true;
	}
	bool TransformedPrimitive::IntersectP(const Ray &ray) const
	{
		return primitive->IntersectP(toWorld(ray, true));
	}

	float Aggregate::Pdf(const Ray &rayWorld, const Ray &ray, const Transform *toWorld, Intersection *isect) const
	{
		float pdf = 0.f;
		for(size_t i = 0; i < primitives.size(); ++i)
			pdf += primitives[i]->Pdf(rayWorld, ray, toWorld, isect);
		return pdf;
	}
}
