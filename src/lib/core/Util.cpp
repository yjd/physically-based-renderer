/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "Util.h"
#include <stdarg.h>

#define RENDERERROR "ERROR in %s::%s\n"
#define RENDERWARNING "WARNING\t"
#define RENDERINFO "INFO\t"
#define RENDERERRCODEFMT " (%d) "
namespace Render
{
	void ProcessMessage(FILE *file, const char *format, va_list &args, bool hasCode = false, int code = 0)
	{
		vfprintf(file, format, args);
		
		if(hasCode)
			fprintf(file, RENDERERRCODEFMT, code);

		fprintf(file, "\n\n");
	}

	void Severe(const char *str, const char *cName, const char *mName, int code, ...)
	{
		fprintf(stderr, RENDERERROR, cName, mName);

		va_list args;
		va_start(args, code);
		ProcessMessage(stderr, str, args, code != 0, code);
		va_end(args);

		throw code;
	}

	void Error(const char *str, const char *cName, const char *mName, int code, ...)
	{
		fprintf(stderr, RENDERERROR, cName, mName);

		va_list args;
		va_start(args, code);
		ProcessMessage(stderr, str, args, code != 0, code);
		va_end(args);
	}

	void Warning(const char *str, ...)
	{
		fprintf(stdout, RENDERWARNING);

		va_list args;
		va_start(args, str);
		ProcessMessage(stdout, str, args);
		va_end(args);
	}

	void Info(const char *str, ...)
	{
		fprintf(stdout, RENDERINFO);

		va_list args;
		va_start(args, str);
		ProcessMessage(stdout, str, args);
		va_end(args);
	}
}