/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "RNG.h"
#define CMW_PHI 0x9e3779b9

namespace Render
{
	RNG::RNG() : n(4096)
	{
	}

	void RNG::Seed(uint32_t seed = 5489UL)
	{
		Q[0] = seed;
		Q[1] = Q[0] + CMW_PHI;
		Q[2] = Q[1] + CMW_PHI;

		for( uint32_t i = 3; i < 4096; ++i )
		{
			Q[i] = Q[i - 3] ^ Q[i - 2] ^ CMW_PHI ^ i;
		}

		n = 4095;
		c = 362436;
	}

	float RNG::RandF()
	{
		float r = (Rand() & 0xffffff) / float(0x1000000);
		return r;
	}

	uint32_t RNG::Rand()
	{
		if(n == 4096)
			Seed();

		n = (n + 1) & 4095;

		uint64_t t = (18705ULL * Q[n]) + c;

		c = t >> 32;
		Q[n] = 0xfffffffe - t;

		return Q[n];
	}
}
