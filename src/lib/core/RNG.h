/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_RNG_H
#define RENDERLIB_CORE_RNG_H

#include "Global.h"

namespace Render
{
	//Complementary Multiply With Carry
	class RNG
	{
		uint32_t Q[4096];
		uint32_t n;
		uint32_t c;
	public:
		RNG();
		void Seed(uint32_t seed);
		uint32_t Rand();
		float RandF();
	};
}

#endif
