/* Copyright (C) 2014 by authors (see AUTHORS.txt)

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:

 *		Redistributions of source code must retain the above copyright
 *		notice, this list of conditions and the following disclaimer.

 *		Redistributions in binary form must reproduce the above copyright
 *		notice, this list of conditions and the following disclaimer in the
 *		documentation and/or other materials provided with the distribution.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_GLOBAL_H
#define RENDERLIB_CORE_GLOBAL_H

#include <stdio.h>
#include <float.h>
#include <limits.h>
#include <boost/asio/detail/atomic_count.hpp>
#include <fstream>
#include <stdint.h>
#include <vector>
#include <math.h>
#include <algorithm>
#include <xmmintrin.h>

#include "Util.h"
#include "Options.h"

#if defined(_WIN32) || defined(_WIN64)
	#define IS_WINDOWS
#elif defined(__linux__)
	#define IS_LINUX
#elif defined(__APPLE__)
	#define IS_APPLE
	#if !(defined(__i386__) || defined(__amd64__))
	#define IS_APPLE_PPC
	#else
	#define IS_APPLE_X86
	#endif
#elif defined(__OpenBSD__)
	#define IS_OPENBSD
#endif

#if defined(IS_WINDOWS)
#define isnan _isnan
#define isinf(f) (!_finite((f)))
#endif

#ifndef L1_CACHE_LINE_SIZE
#define L1_CACHE_LINE_SIZE 64
#endif

#ifndef POINTER_SIZE
#if defined(__amd64__) || defined(_M_X64)
#define POINTER_SIZE 8
#elif defined(__i386__) || defined(_M_IX86)
#define POINTER_SIZE 4
#endif
#endif

#ifndef HAS_64_BIT_ATOMICS
#if (POINTER_SIZE == 8)
#define HAS_64_BIT_ATOMICS
#endif
#endif // HAS_64_BIT_ATOMICS

//Constants - Add ifndefs as redefinitions occur accross OSes
#define STRNAMESPACE "Render"
#ifndef INFINITY
#define INFINITY FLT_MAX
#endif
#define EPSILON FLT_EPSILON
#define RAYEPSILON 1e-4f

#define isNaN isnan
#define ISNOT(expr, exptected) ((expr) != (expected))
#define ISNULL(expr) ((expr) == 0)
#define NOTNULL(expr) ((expr) != 0)

#define ONEMINUSEPSILON		0.9999999403953552f
#ifndef M_PI
#define M_PI			3.14159265358979323846f
#endif
#define TWOPI			6.28318530717958647693f
#define INV_PI			0.31830988618379067154f
#define INV_TWOPI		0.15915494309189533577f
#define INV_FOURPI		0.07957747154594766788f
#define LOG_TWO			0.30102999566398119521f
#define INV_LOG_TWO		3.32192809488736234787f
#define OPTIONFILE		"CONFIG"


//Macros
#define isnanorinf(f) isinf((f)) || isnan((f))
#define isNaNOrInf(f) isnanorinf(f)
#define ALLOCATE_OPTIONAL_MEMBER(member, param, length, type) \
	if(param) \
	{ \
		this->member = new type[length]; \
		memcpy(this->member, param, sizeof(type) * length); \
	} \
	else this->member = NULL

//Using
using namespace std;

//Forward declare boost thread objects.
namespace boost
{
	class thread;
	class mutex;
	class condition_variable;
	namespace interprocess
	{
		class interprocess_semaphore;
	}
}
	
namespace Render
{
	const float invMaxFloat = 1.f / 0xffffffffu;
	//Forward declarations
	
	enum ImageWrap : int;

	class RNG;

	class MemoryArena;

	class Renderer;
	class Scene;
	class Camera;

	struct Vector;
	struct Point;
	struct Normal;
	struct Mat4;
	struct DifferentialGeometry;
	class Transform;
	class AnimatedTransform;
	struct Intersection;

	class Integrator;

	class TriangleMesh;

	class Shape;
	class Primitive;

	class RGBSpectrum;
	class SampledSpectrum;

	class Film;

	struct CameraSample;
	struct Sample;
	class Sampler;

	class SurfaceIntegrator;
	class VolumeIntegrator;

	class BSDF;
	class Material;

	class Light;
	class AreaLight;

	class Volume;

	template<typename T> class KdTree;

	template<typename T, int logBlockSize = 2> class BlockedArray;
	template<typename T> class Reference;

	template<typename T> class Texture;


	typedef boost::asio::detail::atomic_count atomic_count;
	typedef RGBSpectrum Spectrum;
	//typedef SampledSpectrum Spectrum;

	class RenderInstance;

	extern Options opts;
	extern char rootdir[4096];

	inline int Float2Int(float v)
	{
		return int(v);
	}

	inline uint32_t RoundUpPow2(uint32_t v)
	{
		v--;
		v |= v >> 1; v|= v >> 2;
		v |= v >> 4; v|= v >> 8;
		v |= v >> 16;
		return v + 1;
	}

	inline float Radians(const float angle)
	{
		return angle * (M_PI / 180.f);
	}

	inline int Mod(int a, int b)
	{
		int n = int(a/b);
		a -= n*b;
		if(a < 0) a += b;
		return a;
	}

	inline float Lerp(float t, float v1, float v2)
	{
		return (1.0f - t) * v1 + t * v2;
	}

	inline double Lerp(double t, double v1, double v2)
	{
		return (1.0 - t) * v1 + t * v2;
	}

	template<typename T>
	inline T Clamp(T val, T low, T high)
	{
		if(val < low) return low;
		else if (val > high) return high;
		else return val;
	}

	inline bool Quadratic(float A, float B, float C, float *t0, float *t1)
	{
		float q = B*B - 4*A*C;
		if(q < 0)
			return false;

		q = sqrtf(q);
		if(B < 0)
			q = -0.5f * (B - q);
		else
			q = -0.5f * (B + q);

		*t0 = q / A;
		*t1 = C / q;

		if(*t0 > *t1) swap(*t0, *t1);

		return true;
	}
	inline int Ceil2Int(double d)
	{
		return int(ceil(d));
	}
	inline int Ceil2Int(float d)
	{
		return int(ceil(d));
	}

	inline int Floor2Int(double d)
	{
		return int(ceil(d));
	}
	inline int Floor2Int(float d)
	{
		return int(floor(d));
	}

	inline unsigned int Floor2UInt(double d)
	{
		return (unsigned int) (floor(d));
	}
	inline unsigned int Floor2UInt(float d)
	{
		return (unsigned int) (floor(d));
	}

	inline unsigned int Ceil2UInt(double d)
	{
		return (unsigned int) (ceil(d));
	}
	inline unsigned int Ceil2UInt(float d)
	{
		return (unsigned int) (ceil(d));
	}

	//Check if string 'in' ends with string 'search', ignoring case
	inline bool EndsWithIgnoreCase(const string &in, const string &search)
	{
		if(search.size() > in.size())
			return false;

		for(string::size_type i = 1; i <= search.size(); i++)
		{
			if(tolower(in.at(in.size() - i)) != tolower(search.at(search.size() - i)))
				return false;
		}

		return true;
	}

	inline float Log2(float x)
	{
		return log10f(x) * INV_LOG_TWO;
	}

	inline void ReadFile(string *str, const string &fileName)
	{
		ifstream fs(fileName.c_str());
		str->assign((istreambuf_iterator<char>(fs)), (istreambuf_iterator<char>()));
		str += '\0';
		fs.close();
	}
/*
	inline void SaveOptions()
	{
		ofstream fs(OPTIONFILE, ios::binary);
		fs.write((char *) &opts, (char *) &opts.eob - (char *) &opts);
		/* In case start file paths are needed.
		if(opts.startPathStringSize > 0)
			fs.write(opts.startPath, opts.startPathStringSize);
		*//*
		fs.close();
	}

	inline void LoadOptions()
	{
		string file;
		ifstream fs(OPTIONFILE, ios::binary);
		file.assign((istreambuf_iterator<char>(fs)), (istreambuf_iterator<char>()));
		fs.close();

		size_t optsStaticSize = (char *) &opts.eob - (char *) &opts;

		if(file.size() < optsStaticSize)
		{
			memset(&opts, 0, sizeof(Options));
			SaveOptions();
		}
		else
			memcpy(&opts, file.c_str(), optsStaticSize);
	}
*/
	//The source string should be null-terminated.
	inline void SetString(const char *source, char **dest, size_t *size = NULL)
	{
		if((*dest) != NULL)
			delete[] *dest;

		size_t length = strlen(source);

		if(size)
			*size = length;

		*dest = new char[length + 1];
		strcpy(*dest, source);
	}
}
#endif

