/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_TEXIMAGE_H
#define RENDERLIB_CORE_TEXIMAGE_H

#include "ImageIO.h"
#include "Memory.h"

namespace Render
{
	enum ImageFilter
	{
		BILINEAR,
		NEAREST
	};

	enum ImageWrap : int
	{
		BLACK,
		CLAMP,
		REPEAT
	};

	struct TexInfo
	{
		string filename;
		float gamma;
		float scale;
		bool operator<(const TexInfo &other) const
		{
			if(filename != other.filename) return filename < other.filename;
			if(gamma != other.gamma) return gamma < other.gamma;
			if(scale != other.scale) return scale < other.scale;
			return false;
		}

		static void Convert(const float s[3], float *out, float scale, float gamma = 1.f);
		static void Convert(const float s[3], RGBSpectrum *out, float scale, float gamma = 1.f);
	};

	//Currently we don't care about anisotropic filtering because supersampling will eliminate aliasing.  The camera ray is
	//treated like any other ray in the path unless it becomes clear that we only want to sample a limited number of camera rays.
	template<typename T>
	class TextureImage
	{
	protected:
		const BlockedArray<T> *data;
		ImageWrap wrapMode;
		ImageFilter filter;
	public:
		TextureImage(const BlockedArray<T> *data, ImageWrap wrapMode, ImageFilter filter)
			:	data(data), wrapMode(wrapMode), filter(filter){}
		virtual T Lookup(float s, float t) const
		{
			s *= data->uSize();
			t *= data->vSize();

			if(filter == NEAREST)
				return Texel(Floor2Int(s + 0.5f), Floor2Int(t + 0.5f));

			int s0 = Floor2Int(s);
			int t0 = Floor2Int(t);

			float ds = s - float(s0);
			float dt = t - float(t0);

			return	(1.f - ds) * (1.f - dt) * Texel(s0, t0) +
					(1.f - ds) * dt * Texel(s0, t0 + 1) +
					ds * (1.f - dt) * Texel(s0 + 1, t0) +
					ds * dt * Texel(s0 + 1, t0 + 1);
		}

		virtual T Texel(int s, int t) const
		{
			switch(wrapMode)
			{
			case BLACK:
				if(s < 0 || s >= (int) data->uSize() || t < 0 || t >= (int) data->vSize())
					return 0.f;
			case CLAMP:
				s = Clamp(s, 0, int(data->uSize() - 1));
				t = Clamp(t, 0, int(data->vSize() - 1));
				break;
			case REPEAT:
				s = Mod(s, data->uSize());
				t = Mod(t, data->vSize());
				break;
			}

			return data->operator()(s, t);
		}

		virtual T Avg() const
		{
			T total = 0.f;
			for(uint32_t v = 0; v < data->vSize(); ++v)
				for(uint32_t u = 0; u < data->uSize(); ++u)
					total += data->operator()(u,v);

			return total / float(data->uSize() * data->vSize());
		}
	};
}

#endif
