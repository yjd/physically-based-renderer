/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "BxDF.h"

namespace Render
{
	float FrDiel(float cosi, float cost, float etai, float etat)
	{
		float rParl =	(etat * cosi - etai * cost) /
						(etat * cosi + etai * cost);

		float rPerp =	(etai * cosi - etat * cost) /
						(etai * cosi + etat * cost);

		return (rParl * rParl + rPerp * rPerp) * 0.5f;
	}

	Spectrum FrDiel(float cosi, float cost, const Spectrum &etai, const Spectrum &etat)
	{
		Spectrum rParl =	(etat * cosi - etai * cost) /
							(etat * cosi + etai * cost);

		Spectrum rPerp =	(etai * cosi - etat * cost) /
							(etai * cosi + etat * cost);

		return (rParl * rParl + rPerp * rPerp) * 0.5f;
	}

	Spectrum FrCond(float cosi, const Spectrum &eta, const Spectrum &k)
	{
		Spectrum tmp = (eta * eta + k * k) * (cosi * cosi) + 1.f;
		Spectrum n = 2.f * eta * cosi;
		Spectrum rparl2 = (tmp - n) / (tmp + n);
		tmp = eta * eta + k * k + (cosi * cosi);
		Spectrum rperp2 = (tmp - n) / (tmp + n);
		return (rperp2 + rparl2) * 0.5f;
		/*
		float cosi2 = cosi * cosi;
		Spectrum tmp_f = (eta * eta + k * k);
		Spectrum tmp = tmp_f * cosi2;
		Spectrum tmp_2 = 2.f * cosi * eta;

		Spectrum rParl2 = (tmp - tmp_2 + 1.f) / (tmp + tmp_2 + 1.f);
		Spectrum rPerp2 = (tmp_f - tmp_2 + cosi2) / (tmp_f + tmp_2 + cosi2);

		return (rParl2 + rPerp2) * 0.5f;
		*/
	}
	float FrSchlick(float cosh, float r0)
	{
		return r0 + (1.f - r0) * pow((1.f - cosh), 5);
	}

	Spectrum FrSchlick(float cosh, const Spectrum &r0)
	{
		return r0 + (-r0 + 1.f) * pow((1.f - cosh), 5.f);
	}

	Fresnel::~Fresnel(){}

	FresnelDielectric::FresnelDielectric(float etai, float etat)
		:	etai(etai), etat(etat)
	{
	}
	Spectrum FresnelDielectric::Evaluate(float cosi) const
	{
		float ei = etai, et = etat;
		if(cosi < 0.f)
			swap(ei, et);

		float sint2 = pow(ei / et, 2) * (max(0.f, 1.f - cosi * cosi));

		if(sint2 > 1.f)
			return 1.f;

		float cost = sqrt(1.f - sint2);
		return FrDiel(abs(cosi), cost, etai, etat);
	}

	SchlickDielectric::SchlickDielectric(Spectrum r0)
		:	r0(r0){}

	Spectrum SchlickDielectric::Evaluate(float cosi) const
	{
		return FrSchlick(cosi, r0);
	}

	FresnelConductor::FresnelConductor(const Spectrum &eta, const Spectrum &k)
		:eta(eta), k(k)
	{
	}

	Spectrum FresnelConductor::Evaluate(float cosi) const
	{
		return FrCond(cosi, eta, k);
	}

//---------------------------------------------------------------------------------------------------------

	BxDF::BxDF(BxDFType type) : type(type){}
	BxDF::~BxDF(){}
	bool BxDF::MatchesFlags(BxDFType flags) const
	{
		return (type & flags) == type;
	}
	bool BxDF::Sample_f(const Vector &wo, Vector *wi, float u[2], float *pdf, Spectrum &s) const
	{
		*wi = CosineSampleHemisphere(u[0], u[1]);
		if(wo.z < 0)
			wi->z *= -1.f;

		*pdf = Pdf(wo, *wi);
		if(*pdf == 0)
			return false;

		s = 0.f;
		f(wo, *wi, s);
		return true;
	}

	float BxDF::Pdf(const Vector &wo, const Vector &wi) const
	{
		return CosineHemispherePdf(abs(wi.z));
	}

//---------------------------------------------------------------------------------------------------------

	SpecularReflection::SpecularReflection(const Spectrum &r, Fresnel *fr)
		:	BxDF(BxDFType(REFLECTION | SPECULAR)), r(r), fr(fr){}
	void SpecularReflection::f(const Vector &wo, const Vector &wi, Spectrum &s) const
	{
	}
	bool SpecularReflection::Sample_f(const Vector &wo, Vector *wi, float u[2], float *pdf, Spectrum &s) const
	{
		*wi = Vector(-wo.x, -wo.y, wo.z);
		*pdf = 1.f;
		s = fr->Evaluate(abs(wo.z)) * r / abs(wi->z);
		return true;
	}
	float SpecularReflection::Pdf(const Vector &wo, const Vector &wi) const
	{
		return 0.f;
	}

	SpecularTransmission::SpecularTransmission(const Spectrum &t, float etai, float etat)
		:	BxDF(BxDFType(TRANSMISSION | SPECULAR)), t(t), etai(etai), etat(etat){}
	void SpecularTransmission::f(const Vector &wo, const Vector &wi, Spectrum &s) const
	{

	}
	bool SpecularTransmission::Sample_f(const Vector &wo, Vector *wi, float u[2], float *pdf, Spectrum &s) const
	{
		float ei = etai, et = etat;
		bool entering = wo.z > 0;
		if(!entering)
			swap(ei, et);

		float eta = ei / et;
		float eta2 = eta * eta;
		float sint2 = eta2 * max(0.f, 1.f - wo.z * wo.z);

		if(sint2 >= 1.f)
			return false;

		float cost = sqrt(1.f - sint2);
		if(entering)
			cost = -cost;
		*wi = Vector(-eta * wo.x, -eta * wo.y, cost);

		*pdf = 1.f;

		s = ((1.f - FrDiel(abs(wo.z), abs(cost), ei, et)) * eta2 / abs(cost)) * t;

		return true;
	}
	float SpecularTransmission::Pdf(const Vector &wo, const Vector &wi) const
	{
		return 0.f;
	}

	NullTransmission::NullTransmission(const Spectrum &t)
		:	BxDF(BxDFType(TRANSMISSION | SPECULAR)), t(t){}
	void NullTransmission::f(const Vector &wo, const Vector &wi, Spectrum &s) const
	{
	}
	bool NullTransmission::Sample_f(const Vector &wo, Vector *wi, float u[2], float *pdf, Spectrum &s) const
	{
		*wi = Vector(-wo.x, -wo.y, -wo.z);
		s = t / abs(wi->z); 
		*pdf = 1.f;
		return true;
	}
	float NullTransmission::Pdf(const Vector &wo, const Vector &wi) const
	{
		return 0.f;
	}

//---------------------------------------------------------------------------------------------------------

	Lambertian::Lambertian(const Spectrum &r)
		:	BxDF(BxDFType(REFLECTION | DIFFUSE)), r(r){}

	void Lambertian::f(const Vector &wo, const Vector &wi, Spectrum &s) const
	{
		s.AddWeighted(INV_PI, r);
	}

//---------------------------------------------------------------------------------------------------------

	OrenNayar::OrenNayar(const Spectrum &r, float sigma)
		:	BxDF(BxDFType(REFLECTION | DIFFUSE)), r(r)
	{
		float sig2 = sigma * sigma;
		A = 1.f - sig2 / (2.f * (sig2 + 0.33f));
		B = 0.45f * sig2 / (sig2 + 0.09f);
	}

	void OrenNayar::f(const Vector &wo, const Vector &wi, Spectrum &s) const
	{
		float sini = sqrt(max(0.f, 1.f - wi.z * wi.z));
		float sino = sqrt(max(0.f, 1.f - wo.z * wo.z));

		float maxcos = 0.f;
		if(sini > 1e-4 && sino > 1e-4)
		{
			float invsini = 1.f / sini, invsino = 1.f / sino;
			float sinpi = wi.y * invsini, sinpo = wo.y * invsino;
			float cospi = wi.x * invsini, cospo = wo.x * invsino;

			maxcos = max(0.f, cospo * cospi + sinpo * sinpi);
		}

		float sinalpha, tanbeta;
		if(abs(wi.z) > abs(wo.z))
		{
			sinalpha = sino;
			tanbeta = sini / abs(wi.z);
		}
		else
		{
			sinalpha = sini;
			tanbeta = sino / abs(wo.z);
		}

		s.AddWeighted(INV_PI * (A + B * maxcos * sinalpha * tanbeta), r);
	}
//---------------------------------------------------------------------------------------------------------

	SchlickBRDF::SchlickBRDF(const Spectrum &rd, const Spectrum &rs, float sigma, float anisotropy, const Spectrum &alpha, float depth, bool multiBounce)
		:	BxDF(BxDFType(BxDFType::DIFFUSE | BxDFType::REFLECTION)), rd(rd), rs(rs), sigma(sigma), anisotropy(anisotropy), alpha(alpha), depth(depth > 0.f ? depth : 0.f), multiBounce(multiBounce){}
	float SchlickBRDF::G(float cos) const
	{
		return cos / (sigma - sigma * cos + cos);
	}
	float SchlickBRDF::A(float cosphi2) const
	{
		float p = 1.f - abs(anisotropy);
		float p2 = p * p;
		return sqrt(p / (p2 - p2 * cosphi2 + cosphi2));
	}
	float SchlickBRDF::Z(float costheta2) const
	{
		float denom = (1.f + sigma * costheta2 - costheta2);
		return sigma / (denom * denom);
	}
	float SchlickBRDF::AZ(const Vector &wh) const
	{
		float ds = 1.f - wh.z * wh.z;
		float a = ds > 0.f ? A((anisotropy > 0.f ? wh.x * wh.x : wh.y * wh.y) / ds) : 1.f;
		return Z(wh.z * wh.z) * a;
	}
	float SchlickBRDF::Phi(float u, float p) const
	{
		return M_PI * 0.5f * sqrt(u * p / (1.f - u + u * p));
	}
	void SchlickBRDF::f(const Vector &wo, const Vector &wi, Spectrum &s) const
	{
		float coso = abs(wo.z), cosi = abs(wi.z);

		if(rs.IsBlack())
		{
			if(depth > 0 && !alpha.IsBlack())
				s.AddWeighted(INV_PI, rd * Exp(alpha * (-depth * (coso + cosi) / (coso * cosi))));
			else
				s.AddWeighted(INV_PI, rd);
			return;
		}

		Vector wh = wo + wi;
		if(wh.x == 0 && wh.y == 0 && wh.z == 0)
			return;
		wh.Normalize();

		Spectrum spec = FrSchlick(AbsDot(wi, wh), rs);
		if(depth > 0 && !alpha.IsBlack())
			s.AddWeighted(INV_PI, rd * (Spectrum(1.f) - spec) * Exp(alpha * (-depth * (coso + cosi) / (coso * cosi))));
		else
			s.AddWeighted(INV_PI, rd * (Spectrum(1.f) - spec));

		float g = G(coso) * G(cosi);
		float az = AZ(wh);
		float den = 4.f * M_PI * coso * cosi;
		s.AddWeighted(multiBounce ?	((g * az + Clamp(1.f - g, 0.f, den)) / den) : 
									(g * az / den),
									spec);
	}
	bool SchlickBRDF::Sample_f(const Vector &wo, Vector *wi, float u[2], float *pdf, Spectrum &s) const
	{
		if(rs.IsBlack())
		{
			*wi = CosineSampleHemisphere(u[0], u[1]);
			if(wo.z < 0.f) wi->z *= -1.f;
			f(wo, *wi, s = 0.f);
			*pdf = CosineHemispherePdf(abs(wi->z));
			return *pdf > 0;
		}

		u[0] *= 2.f;
		if(u[0] < 1.f)
		{
			*wi = CosineSampleHemisphere(u[0], u[1]);
			if(wo.z < 0.f) wi->z *= -1.f;
			*pdf = Pdf(wo, *wi);
		}
		else
		{
			u[0] -= 1.f;
			float p = 1.f - abs(anisotropy);
			float phi;
			switch((unsigned int)(u[1] * 4.f))
			{
			case 0:
				u[1] *= 4.f;
				phi = Phi(u[1] * u[1], p * p);
				break;
			case 1:
				u[1] = 4.f * (0.5f - u[1]);
				phi = M_PI - Phi(u[1] * u[1], p * p);
				break;
			case 2:
				u[1] = 4.f * (u[1] - 0.5f);
				phi = M_PI + Phi(u[1] * u[1], p * p);
				break;
			default:
				u[1] = 4.f * (1.f - u[1]);
				phi = TWOPI - Phi(u[1] * u[1], p * p);
				break;
			}
			if(anisotropy > 0.f)
				phi += M_PI * 0.5f;

			float cos = u[0] / (sigma - sigma * u[0] + u[0]);
			float sin = sqrt(max(0.f, 1.f - cos));
			cos = sqrt(cos);
			Vector wh = SphericalDirection(sin, cos, phi);
			if(wo.z < 0.f)
				wh.z *= -1.f;

			float cosh = Dot(wo, wh);
			*wi = -wo + 2.f * cosh * wh;
			*pdf = 0.5f * (CosineHemispherePdf(abs(wi->z)) + AZ(wh) / (4.f * M_PI * cosh));
		}
		if(!SameHemisphere(wo, *wi))
			return false;
		
		f(wo, *wi, s = 0.f);

		return *pdf > 0.f;
	}
	float SchlickBRDF::Pdf(const Vector &wo, const Vector &wi) const
	{
		if(!SameHemisphere(wo, wi))
			return 0.f;
		if(rs.IsBlack())
			return CosineHemispherePdf(abs(wi.z));

		Vector wh = wo + wi;
		if(wh.x == 0.f && wh.y == 0.f && wh.z == 0.f)
			return 0.f;

		wh.Normalize();

		return 0.5f * (CosineHemispherePdf(abs(wi.z)) + AZ(wh) / (4.f * M_PI * AbsDot(wo, wh)));
	}

//---------------------------------------------------------------------------------------------------------

	MicrofacetDistribution::~MicrofacetDistribution(){}
	float MicrofacetDistribution::G(const Vector &wo, const Vector &wi, const Vector &wh) const
	{
		float ndh = abs(wh.z);
		float ndo = abs(wo.z);
		float ndi = abs(wi.z);

		float odh2 = 2.f * ndh / AbsDot(wo, wh);
		return min( 1.f, odh2 * min(ndo, ndi) );
	}
	Microfacet::Microfacet(const Spectrum &r, Fresnel *fr, MicrofacetDistribution *md)
		:	BxDF(BxDFType(REFLECTION | GLOSSY)), r(r), fr(fr), md(md)
	{
	}
	void Microfacet::f(const Vector &wo, const Vector &wi, Spectrum &s) const
	{
		if(wo.z == 0 || wi.z == 0)
			return;

		Vector wh = wo + wi;
		if(wh.x == 0 && wh.y == 0 && wh.z == 0) return;
		wh.Normalize();

		s.AddWeighted(md->D(wh) * md->G(wo, wi, wh) / (abs(wo.z) * abs(wi.z) * 4.f), fr->Evaluate(Dot(wi, wh)) * r);
	}

	bool Microfacet::Sample_f(const Vector &wo, Vector *wi, float u[2], float *pdf, Spectrum &s) const
	{
		Vector wh;
		float d = md->Sample_w(wo, wi, &wh, u, pdf);
		if(d == 0.f)
			return false;

		s = (d * md->G(wo, *wi, wh) / (abs(wo.z) * abs(wi->z) * 4.f)) * fr->Evaluate(Dot(*wi, wh)) * r;

		return true;
	}

	float Microfacet::Pdf(const Vector &wo, const Vector &wi) const
	{
		if(!SameHemisphere(wo, wi)) return 0.f;
		return md->Pdf(wo, wi);
	}

//---------------------------------------------------------------------------------------------------------

	Blinn::Blinn(float e)
		:	e(isinf(e) || isnan(e) ? 10000.f : e){}
	float Blinn::Sample_w(const Vector &wo, Vector *wi, Vector *wh, float u[2], float *pdf) const
	{
		float cos = pow(u[0], 1.f / (e + 1.f));
		float sin = sqrt(max(0.f, 1.f - cos * cos));
		float phi = TWOPI * u[1];
		*wh = SphericalDirection(sin, cos, phi);

		if(!SameHemisphere(wo, *wh))
			*wh *= -1.f;

		float cosi = Dot(wo, *wh);
		if(cosi < 0.f)
			return 0.f;

		*wi = -wo + 2.f * cosi * *wh;
		
		//Note: (cos)^(e + 1) = u[0]; thus cos^e = u[0] / cos.
		float cosPowE = u[0] / cos;
		*pdf = (e + 1.f) * cosPowE / (TWOPI * 4.f * cosi);
		return (e + 2.f) * INV_TWOPI * cosPowE;
	}
	float Blinn::D(const Vector &wh) const
	{
		return (e + 2.f) * INV_TWOPI * pow(abs(wh.z), e);
	}
	float Blinn::Pdf(const Vector &wo, const Vector &wi) const
	{
		if(!SameHemisphere(wo, wi))
			return 0.f;
		Vector wh = Normalize(wo + wi);
		float cosh = Dot(wo, wh);
		return (e + 1.f) * pow(abs(wh.z), e) / (TWOPI * 4.f * cosh);
	}

//---------------------------------------------------------------------------------------------------------

	Anisotropic::Anisotropic(float x, float y)
		:	x(isinf(x) || isnan(x) ? 10000.f : x), y(isinf(y) || isnan(y) ? 10000.f : y),
			sd(sqrt((this->x + 2.f) * (this->y + 2.f))), sp(sqrt((this->x + 1.f) * (this->y + 1.f))){}

	void Anisotropic::sampleFirstQuadrant(float u[2], float *phi, float *cosh) const
	{
		if(x == y)
			*phi = M_PI * 0.5f * u[0];
		else
			*phi = atan(sqrt((x + 1.f) / (y + 1.f)) * tan(M_PI * u[0] * 0.5f));

		float cosphi = cos(*phi), sinphi = sin(*phi);
		*cosh = pow(u[1], 1.f / (x * cosphi * cosphi + y * sinphi * sinphi + 1.f));
	}

	float Anisotropic::Sample_w(const Vector &wo, Vector *wi, Vector *wh, float u[2], float *pdf) const
	{
		float phi, cos;
		switch((unsigned int)(u[0] * 4.f))
		{
		case 0:
			u[0] *= 4.f;
			sampleFirstQuadrant(u, &phi, &cos);
			break;
		case 1:
			u[0] = 4.f * (0.5f - u[0]);
			sampleFirstQuadrant(u, &phi, &cos);
			phi = M_PI - phi;
			break;
		case 2:
			u[0] = 4.f * (u[0] - 0.5f);
			sampleFirstQuadrant(u, &phi, &cos);
			phi += M_PI;
			break;
		default:
			u[0] = 4.f * (1.f - u[0]);
			sampleFirstQuadrant(u, &phi, &cos);
			phi = TWOPI - phi;
			break;
		}
		float sin = sqrt(max(0.f, 1.f - cos * cos));
		*wh = SphericalDirection(sin, cos, phi);
		if(!SameHemisphere(*wh, wo)) *wh *= -1.f;

		float cosi = Dot(wo, *wh);
		if(cosi < 0.f)
			return 0.f;

		*wi = -wo + 2.f * cosi * *wh;

		float cosh = abs(wh->z);
		float ds = 1.f - cosh * cosh;

		if(ds <= 0.f)
			return 0.f;

		float d = INV_TWOPI * pow(cosh, (x * wh->x * wh->x + y * wh->y * wh->y) / ds);

		*pdf = sp * d / (4.f * cosi);
		return sd * d;
	}

	float Anisotropic::D(const Vector &wh) const
	{
		float sintheta2 = 1.f - wh.z * wh.z;
		if(sintheta2 <= 0.f)
			return 0.f;

		return sd * INV_TWOPI * pow(abs(wh.z), (x * wh.x * wh.x + y * wh.y * wh.y) / sintheta2);
	}

	float Anisotropic::Pdf(const Vector &wo, const Vector &wi) const
	{
		if(!SameHemisphere(wo, wi))
			return 0.f;

		Vector wh = Normalize(wo + wi);
		float cosi = Dot(wo, wh);

		float cosh = abs(wh.z);
		float ds = 1.f - cosh * cosh;

		if(ds <= 0.f)
			return 0.f;

		return	sp * INV_TWOPI * 
				pow(cosh, (x * wh.x * wh.x + y * wh.y * wh.y) / ds) /
				(4.f * cosi);
	}

//---------------------------------------------------------------------------------------------------------

	SchlickDistribution::SchlickDistribution(float sigma, float anisotropy)
		:	sigma(sigma), anisotropy(anisotropy)
	{
	}

	float SchlickDistribution::G(float cos) const
	{
		return cos / (sigma - sigma * cos + cos);
	}
	float SchlickDistribution::A(float cosphi2) const
	{
		float p = 1.f - abs(anisotropy);
		float p2 = p * p;
		return sqrt(p / (p2 - p2 * cosphi2 + cosphi2));
	}
	float SchlickDistribution::Z(float costheta2) const
	{
		float denom = (1.f + sigma * costheta2 - costheta2);
		return sigma / (denom * denom);
	}
	float SchlickDistribution::AZ(const Vector &wh) const
	{
		float ds = 1.f - wh.z * wh.z;
		float a = ds > 0.f ? A((anisotropy > 0.f ? wh.x * wh.x : wh.y * wh.y) / ds) : 1.f;
		return Z(wh.z * wh.z) * a;
	}
	float SchlickDistribution::Phi(float u, float p) const
	{
		return M_PI * 0.5f * sqrt(u * p / (1.f - u + u * p));
	}
	float SchlickDistribution::G(const Vector &wo, const Vector &wi, const Vector &wh) const
	{
		return G(abs(wo.z)) * G(abs(wi.z));
	}
	float SchlickDistribution::Sample_w(const Vector &wo, Vector *wi, Vector *wh, float u[2], float *pdf) const
	{
		float p = 1.f - abs(anisotropy);
		float phi;
		switch((unsigned int)(u[0] * 4.f))
		{
		case 0:
			u[0] *= 4.f;
			phi = Phi(u[0] * u[0], p * p);
			break;
		case 1:
			u[0] = 4.f * (0.5f - u[0]);
			phi = M_PI - Phi(u[0] * u[0], p * p);
			break;
		case 2:
			u[0] = 4.f * (u[0] - 0.5f);
			phi = M_PI + Phi(u[0] * u[0], p * p);
			break;
		default:
			u[0] = 4.f * (1.f - u[0]);
			phi = TWOPI - Phi(u[0] * u[0], p * p);
			break;
		}
		if(anisotropy > 0.f)
			phi += M_PI * 0.5f;

		float cos = u[1] / (sigma - sigma * u[1] + u[1]);
		float sin = sqrt(max(0.f, 1.f - cos));
		cos = sqrt(cos);
		*wh = SphericalDirection(sin, cos, phi);

		if(!SameHemisphere(wo, *wh))
			*wh *= -1.f;

		float cosi = Dot(wo, *wh);
		
		if(cosi < 0.f)
			return 0.f;

		*wi = -wo + 2.f * cosi * *wh;

		float d = AZ(*wh) * INV_PI;

		*pdf = d / (4.f * cosi);

		return d;
	}
	float SchlickDistribution::D(const Vector &wh) const
	{
		return AZ(wh) * INV_PI;
	}
	float SchlickDistribution::Pdf(const Vector &wo, const Vector &wi) const
	{
		if(!SameHemisphere(wo, wi))
			return 0.f;
		Vector wh = Normalize(wo + wi);
		float cosh = AbsDot(wo, wh);
		return AZ(wh) / (4.f * M_PI * cosh);
	}

//---------------------------------------------------------------------------------------------------------

	FresnelBlend::FresnelBlend(const Spectrum &rd, const Spectrum &rs, const Spectrum &alpha, float depth, MicrofacetDistribution *md)
		:	BxDF(BxDFType(BxDFType::GLOSSY | BxDFType::REFLECTION)), rd(rd), rs(rs), alpha(alpha), depth(depth), md(md){}
	void FresnelBlend::f(const Vector &wo, const Vector &wi, Spectrum &s) const
	{
		Vector wh = wi + wo;
		if(wh.x == 0 && wh.y == 0 && wh.z == 0)
			return;

		wh.Normalize();
		
		float coso = abs(wo.z); float cosi = abs(wi.z);

		if(depth > 0 && !alpha.IsBlack())
			s.AddWeighted(	28.f / (23.f * M_PI) * 
							(1.f - pow(1.f - coso * 0.5f, 5)) *
							(1.f - pow(1.f - cosi * 0.5f, 5)),
							rd * (-rs + 1.f) * Exp(alpha * (-depth * (coso + cosi) / (coso * cosi))));
		else
			s.AddWeighted(	28.f / (23.f * M_PI) * 
							(1.f - pow(1.f - coso * 0.5f, 5)) *
							(1.f - pow(1.f - cosi * 0.5f, 5)),
							rd * (-rs + 1.f));

		float cosh = Dot(wi, wh);
		s.AddWeighted(md->D(wh) / (4.f * abs(cosh) * max(coso, cosi)), FrSchlick(cosh, rs));
	}
	bool FresnelBlend::Sample_f(const Vector &wo, Vector *wi, float u[2], float *pdf, Spectrum &s) const
	{
		u[0] *= 2.f;
		Vector wh;
		float d;
		if(u[0] < 1.f)
		{
			*wi = CosineSampleHemisphere(u[0], u[1]);
			if(wo.z < 0.f) wi->z *= -1.f;
			wh = Normalize(*wi + wo);
			d = md->D(wh);
			*pdf = Pdf(wo, *wi);
		}
		else
		{
			u[0] -= 1.f;
			if((d = md->Sample_w(wo, wi, &wh, u, pdf)) == 0.f)
				return false;

			*pdf += CosineHemispherePdf(abs(wi->z));
			*pdf *= 0.5f;
		}
		
		float coso = abs(wo.z); float cosi = abs(wi->z);

		if(depth > 0 && !alpha.IsBlack())
			s = 28.f / (23.f * M_PI) * 
				(1.f - pow(1.f - coso * 0.5f, 5)) *
				(1.f - pow(1.f - cosi * 0.5f, 5)) *
				rd * (-rs + 1.f) * Exp(alpha * (-depth * (coso + cosi) / (coso * cosi)));
		else
			s = 28.f / (23.f * M_PI) * 
				(1.f - pow(1.f - coso * 0.5f, 5)) *
				(1.f - pow(1.f - cosi * 0.5f, 5)) *
				rd * (-rs + 1.f);

		float cosh = Dot(*wi, wh);
		s.AddWeighted(d / (4.f * abs(cosh) * max(coso, cosi)), FrSchlick(cosh, rs));

		return true;
	}
	float FresnelBlend::Pdf(const Vector &wo, const Vector &wi) const
	{
		if(!SameHemisphere(wo, wi)) return 0.f;
		return (CosineHemispherePdf(abs(wi.z)) + md->Pdf(wo, wi)) * 0.5f;
	}

//---------------------------------------------------------------------------------------------------------

	BSDF::BSDF(bool specularOnly) : specularOnly(specularOnly){}
	BSDF::~BSDF(){}

	SingleBxDF::SingleBxDF(BxDF *bxdf, const DifferentialGeometry &dg, const Normal &ng, bool specularOnly)
		:	BSDF(specularOnly), bxdf(bxdf), ns(dg.n), ss(Normalize(dg.dpdu)), ts(Cross(ns, ss)), ng(ng){}
	Vector SingleBxDF::ToWorld(const Vector &w) const
	{
		return Vector(	w.x * ss.x + w.y * ts.x + w.z * ns.x,
						w.x * ss.y + w.y * ts.y + w.z * ns.y,
						w.x * ss.z + w.y * ts.z + w.z * ns.z);
	}
	Vector SingleBxDF::ToLocal(const Vector &w) const
	{
		return Vector(Dot(w, ss), Dot(w, ts), Dot(w, ns));
	}
	unsigned int SingleBxDF::NumComponents() const
	{
		return 1;
	}
	unsigned int SingleBxDF::NumComponents(BxDFType flags) const
	{
		return bxdf->MatchesFlags(flags) ? 1 : 0;
	}
	bool SingleBxDF::Sample_f(const Vector &woW, Vector *wiW, float u[3], float *pdf, BxDFType flags, Spectrum &s, BxDFType *sampledType) const
	{
		if(!bxdf->MatchesFlags(flags))
			return false;

		Vector wo = ToLocal(woW);
		Vector wi;
		if(!bxdf->Sample_f(wo, &wi, u, pdf, s))
			return false;

		if(sampledType)
			*sampledType = bxdf->type;

		*wiW = ToWorld(wi);
		
		if(Dot(ng, *wiW) * Dot(ng, woW) > 0)
			flags = BxDFType(flags & ~TRANSMISSION);
		else
			flags = BxDFType(flags & ~REFLECTION);

		if(!bxdf->MatchesFlags(flags))
			return false;
		else s *= abs(wi.z);

		return true;
	}
	Spectrum SingleBxDF::f(const Vector &woW, const Vector &wiW, BxDFType flags) const
	{
		if(Dot(ng, wiW) * Dot(ng, woW) > 0)
			flags = BxDFType(flags & ~TRANSMISSION);
		else
			flags = BxDFType(flags & ~REFLECTION);

		if(!bxdf->MatchesFlags(flags))
			return 0.f;
		
		Vector wo = ToLocal(woW);
		Vector wi = ToLocal(wiW);

		if(wi.z == 0)
			return 0.f;

		Spectrum r;
		bxdf->f(wo, wi, r);
		r *= abs(wi.z);
		return r;
	}
	float SingleBxDF::Pdf(const Vector &woW, const Vector &wiW, BxDFType flags) const
	{
		if(!bxdf->MatchesFlags(flags))
			return 0.f;

		Vector wo = ToLocal(woW);
		Vector wi = ToLocal(wiW);
		return bxdf->Pdf(wo, wi);
	}

	float SingleBxDF::PdfWtoA(const Vector &w, float dist2) const
	{
		//ng is used rather than ns because the probability is ultimately related to subtended angle and that's wrong!
		//The point shaded is subtended by the shaded normal.  This prevents fireflies in bump maps.
		return AbsDot(w, ns) / dist2;
	}

	float SingleBxDF::PdfWtoA(const Vector &w) const
	{
		return AbsDot(w, ns);
	}

	MultiBxDF::MultiBxDF(BxDF **bxdfs, unsigned int nBxDFs, const DifferentialGeometry &dg, const Normal &ng, bool specularOnly)
		:	BSDF(specularOnly), bxdfs(bxdfs), nBxDFs(nBxDFs), ns(dg.n), ss(Normalize(dg.dpdu)), ts(Cross(ns, ss)), ng(ng){}
	Vector MultiBxDF::ToWorld(const Vector &w) const
	{
		return Vector(	w.x * ss.x + w.y * ts.x + w.z * ns.x,
						w.x * ss.y + w.y * ts.y + w.z * ns.y,
						w.x * ss.z + w.y * ts.z + w.z * ns.z);
	}
	Vector MultiBxDF::ToLocal(const Vector &w) const
	{
		return Vector(Dot(w, ss), Dot(w, ts), Dot(w, ns));
	}
	unsigned int MultiBxDF::NumComponents() const
	{
		return nBxDFs;
	}
	unsigned int MultiBxDF::NumComponents(BxDFType flags) const
	{
		unsigned int n = 0;
		for(unsigned int i = 0; i < nBxDFs; ++i)
			if(bxdfs[i]->MatchesFlags(flags)) ++n;

		return n;
	}
	bool MultiBxDF::Sample_f(const Vector &woW, Vector *wiW, float u[3], float *pdf, BxDFType flags, Spectrum &s, BxDFType *sampledType) const
	{
		unsigned int n = NumComponents(flags);
		if(n == 0)
			return false;

		const BxDF *bxdf;
		for(unsigned int i = 0, 
			which = min((unsigned int)(u[2] * n), n - 1); 
			i < nBxDFs; ++i)
			if(bxdfs[i]->MatchesFlags(flags) && which-- == 0)
			{
				bxdf = bxdfs[i];
				break;
			}

		Vector wo = ToLocal(woW);
		Vector wi;
		//Consider the case of a highly specular Blinn microfacet combined with a
		//more diffuse material.  Even if Sample_f returns false for the
		//microfacet, the more diffuse material may still be able to contribute. 
		if(!bxdf->Sample_f(wo, &wi, u, pdf, s))
		{
			*pdf = 0.f;
			s = 0.f;
		}

		if(sampledType)
			*sampledType = bxdf->type;

		*wiW = ToWorld(wi);
		if(!(bxdf->type & SPECULAR))
		{
			if(Dot(ng, *wiW) * Dot(ng, woW) > 0)
				flags = BxDFType(flags & ~TRANSMISSION);
			else
				flags = BxDFType(flags & ~REFLECTION);

			if(!bxdf->MatchesFlags(flags))
				s = 0.f;

			if(n > 1)
			{
				for(unsigned int i = 0; i < nBxDFs; ++i)
					if(bxdfs[i]->MatchesFlags(bxdf->type) && bxdfs[i] != bxdf)
					{
						*pdf += bxdfs[i]->Pdf(wo, wi);
						if(bxdfs[i]->MatchesFlags(flags))
							bxdfs[i]->f(wo, wi, s);
					}

				*pdf /= float(n);
			}
		}
		else if(n > 1)
			*pdf /= float(n);

		s *= abs(wi.z);

		return *pdf > 0.f;
	}

	Spectrum MultiBxDF::f(const Vector &woW, const Vector &wiW, BxDFType flags) const
	{
		if(Dot(ng, wiW) * Dot(ng, woW) > 0)
			flags = BxDFType(flags & ~TRANSMISSION);
		else
			flags = BxDFType(flags & ~REFLECTION);
		
		Vector wo = ToLocal(woW);
		Vector wi = ToLocal(wiW);
		
		if(wi.z == 0)
			return 0.f;

		Spectrum r;
		for(unsigned int i = 0; i < nBxDFs; ++i)
			if(bxdfs[i]->MatchesFlags(flags))
				bxdfs[i]->f(wo, wi, r);
		
		r *= abs(wi.z);
		return r;
	}
	float MultiBxDF::Pdf(const Vector &woW, const Vector &wiW, BxDFType flags) const
	{
		float pdf = 0.f;
		float n = 0.f;
		Vector wo = ToLocal(woW);
		Vector wi = ToLocal(wiW);
		for(unsigned int i = 0; i < nBxDFs; ++i)
			if(bxdfs[i]->MatchesFlags(flags))
			{
				pdf += bxdfs[i]->Pdf(wo, wi);
				++n;
			}

		if(n > 1)
			pdf /= float(n);

		return pdf;
	}

	float MultiBxDF::PdfWtoA(const Vector &w, float dist2) const
	{
		return AbsDot(w, ns) / dist2;
	}

	float MultiBxDF::PdfWtoA(const Vector &w) const
	{
		return AbsDot(w, ns);
	}

	MixedBSDF::MixedBSDF(BSDF **bsdfs, unsigned int nBSDFs, float *weights, float totalWeight, bool specularOnly)
		:	BSDF(specularOnly), bsdfs(bsdfs), nBSDFs(nBSDFs), weights(weights), totalWeight(totalWeight), invTotalWeight(1.f / totalWeight)
	{
	}
	unsigned int MixedBSDF::NumComponents() const
	{
		unsigned int n = 0;
		for(unsigned int i = 0; i < nBSDFs; ++i)
			n += bsdfs[i]->NumComponents();

		return n;
	}
	unsigned int MixedBSDF::NumComponents(BxDFType flags) const
	{
		unsigned int n = 0;
		for(unsigned int i = 0; i < nBSDFs; ++i)
			n += bsdfs[i]->NumComponents(flags);

		return n;
	}
	bool MixedBSDF::Sample_f(const Vector &woW, Vector *wiW, float u[3], float *pdf, BxDFType flags, Spectrum &s, BxDFType *sampledType) const
	{
		float chosenWeight;
		u[2] *= totalWeight;
		const BSDF *bsdf = NULL;
		for(unsigned int i = 0; i < nBSDFs; ++i)
		{
			if(u[2] < weights[i])
			{
				bsdf = bsdfs[i];
				chosenWeight = weights[i];
				//If the mixed BxDFs are thought of as being aligned linearly on a line, this represents the 'proportion' of u going into the child bsdf.
				u[2] /= chosenWeight;
				break;
			}
			u[2] -= weights[i];
		}
		if(!bsdf)
		{
			bsdf = bsdfs[nBSDFs - 1];
			chosenWeight = weights[nBSDFs - 1];
			u[2] /= chosenWeight;
		}

		//If one bsdf doesn't return true, doesn't mean another won't either.  And
		//all samples must be taken into consideration.
		if(!bsdf->Sample_f(woW, wiW, u, pdf, flags, s, sampledType))
		{
			s = 0.f;
			*pdf = 0.f;
		}
		else 
		{
			s *= chosenWeight;
			*pdf *= chosenWeight;
		}

		if((*sampledType & SPECULAR) == BxDFNULL)
		{
			for(unsigned int i = 0; i < nBSDFs; ++i)
			{
				if(bsdfs[i] == bsdf || bsdfs[i]->NumComponents(flags) == 0)
					continue;

				s.AddWeighted(weights[i], bsdfs[i]->f(woW, *wiW, flags));
				*pdf += weights[i] * bsdfs[i]->Pdf(woW, *wiW, flags);
			}
		}

		s *= invTotalWeight;
		*pdf *= invTotalWeight;

		return *pdf > 0.f;
	}

	Spectrum MixedBSDF::f(const Vector &wo, const Vector &wi, BxDFType flags) const
	{
		Spectrum s;
		for(unsigned int i = 0; i < nBSDFs; ++i)
		{
			if(bsdfs[i]->NumComponents(flags) == 0)
				continue;

			s.AddWeighted(weights[i], bsdfs[i]->f(wo, wi, flags));
		}
		s *= invTotalWeight;
		return s;
	}

	float MixedBSDF::Pdf(const Vector &wo, const Vector &wi, BxDFType flags) const
	{
		float pdf = 0;
		for(unsigned int i = 0; i < nBSDFs; ++i)
		{
			if(bsdfs[i]->NumComponents(flags) == 0)
				continue;

			pdf += weights[i] * bsdfs[i]->Pdf(wo, wi, flags);
		}
		pdf *= invTotalWeight;
		return pdf;
	}

	float MixedBSDF::PdfWtoA(const Vector &w, float dist2) const
	{
		return bsdfs[0]->PdfWtoA(w, dist2);
	}

	float MixedBSDF::PdfWtoA(const Vector &w) const
	{
		return bsdfs[0]->PdfWtoA(w);
	}
}
