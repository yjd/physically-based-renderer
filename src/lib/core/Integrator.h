/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_INTEGRATOR_H
#define RENDERLIB_CORE_INTEGRATOR_H

#include "BxDF.h"
#include "Scene.h"
#include "Camera.h"
#include "Film.h"
#include "Renderer.h"

namespace Render
{
	struct PathVertex
	{
		Spectrum alpha;
		Point p;
		Vector w;
		BSDF *bsdf;
		//Holds the latest factor for vertex connections.
		float misConnect;
		float misFactors;
		//Is when the previous vertex sampled was specular
		bool specular;
		PathVertex(const Spectrum &alpha, const Point &p, const Vector &w, BSDF *bsdf, float misFactors, float misConnect, bool specular = false);
	};

	class Integrator
	{
	protected:
		virtual void requiredSamples(const Sampler *sampler, const Scene *scene, unsigned int *n, unsigned int *nDecorrelated) = 0;
		virtual void setSampleOffsets(unsigned int n, unsigned int nDecorrelated) = 0;
	public:
		static void RequestSamples(Sampler *sampler, Integrator *surf, Integrator *vol, const Scene *scene);
		virtual ~Integrator();
	};

	class SurfaceIntegrator : public Integrator
	{
	public:
		virtual void Li(const Renderer *renderer, Scene *scene, Camera *camera, Sample *sample, Intersection *isect, BSDF *bsdf, Ray &ray, Spectrum &L, MemoryArena &arena) const = 0;
	};

	class VolumeIntegrator : public Integrator
	{
	public:
		virtual void Li(const Renderer *renderer, const Scene *scene, Sample *sample, const Ray &ray, Spectrum &L, Spectrum &T, MemoryArena &arena) const = 0;
		virtual void Transmittance(const Renderer *renderer, const Scene *scene, Sample *sample, const Ray &ray, Spectrum &L, MemoryArena &arena, bool useSample = false) const = 0;
	};

	bool SampleOneLight(const Renderer *renderer, const Scene *scene, 
						Sample *sample, uint32_t offset, const BSDF *bsdf,
						const Point &p, Ray &ray, Intersection &isect, Spectrum &s, MemoryArena &arena);
}

#endif