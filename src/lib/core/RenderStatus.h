/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_RENDERERSTATUS_H
#define RENDERLIB_CORE_RENDERERSTATUS_H

#include "Global.h"

namespace Render
{
	class RenderStatus
	{
	public:
		virtual ~RenderStatus(){}
		/* Renderers should stop if update returns false.  Updates the number of passes. */
		virtual bool Update(unsigned int passes) = 0;
		virtual void RenderStart() = 0;
	};
}

#endif
