/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_QUATERNION_H
#define RENDERLIB_CORE_QUATERNION_H

#include "Algebra.h"

namespace Render
{
	struct Quaternion
	{
		Vector v;
		float w;
		Quaternion() : v(), w(1.f) {}
		Quaternion(float x, float y, float z, float w) : v(x, y, z), w(w) {}
		Quaternion(const Vector &v, float w) : v(v), w(w) {}
		Quaternion(const Transform &t);
		Quaternion &operator+=(const Quaternion &q)
		{
			v += q.v;
			w += q.w;
			return *this;
		}
		Quaternion operator+(const Quaternion &q) const
		{
			return Quaternion(v + q.v, w + q.w);
		}
		Quaternion &operator-=(const Quaternion &q)
		{
			v -= q.v;
			w -= q.w;
			return *this;
		}
		Quaternion operator-(const Quaternion &q) const
		{
			return Quaternion(v - q.v, w - q.w);
		}
		Quaternion operator-() const
		{
			return Quaternion(-v, -w);
		}
		Quaternion operator*(float f) const
		{
			return Quaternion(v * f, w * f);
		}
		Quaternion &operator*=(float f)
		{
			v *= f;
			w *= f;
			return *this;
		}
		Quaternion operator*(const Quaternion &q) const
		{
			return Quaternion(Cross(v, q.v) + w * q.v + q.w * v, w * q.w - Dot(v, q.v));
		}
		Quaternion &operator*=(const Quaternion &q)
		{
			float tempW = w;
			w = w * q.w - Dot(v, q.v);
			v = Cross(v, q.v) + tempW * q.v + q.w * v;
			return *this;
		}
		Quaternion operator/(float f) const
		{
			float fInv = 1.f / f;
			return Quaternion(v * fInv, w * fInv);
		}
		Quaternion &operator/=(float f)
		{
			float fInv = 1.f / f;
			v *= fInv;
			w *= fInv;
			return *this;
		}

		Transform ToTransform() const;
	};

	inline Quaternion operator*(float d, const Quaternion &q)
	{
		return q * d;
	}

	inline float Dot(const Quaternion &q1, const Quaternion &q2)
	{
		return Dot(q1.v, q2.v) + q1.w * q2.w;
	}

	inline Quaternion Normalize(const Quaternion &q)
	{
		return q / sqrt(Dot(q, q));
	}

	Quaternion Slerp(float t, const Quaternion &q1, const Quaternion &q2);
}

#endif