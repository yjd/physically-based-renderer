/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_API_H
#define RENDERLIB_CORE_API_H

#include "Scene.h"

#include "Parallel.h"

#include "RNG.h"

#include "accelerators/BinnedKdTree.h"
#include "accelerators/BVH.h"

#include "cameras/Perspective.h"
#include "core/ProgressiveStatus.h"

#include "films/ImageFilm.h"

#include "integrators/Bidirectional.h"
#include "integrators/Path.h"
#include "integrators/Single.h"

#include "lights/Diffuse.h"
#include "lights/Directional.h"
#include "lights/Infinite.h"
#include "lights/Point.h"

#include "materials/Glass.h"
#include "materials/Glossy.h"
#include "materials/Matte.h"
#include "materials/Metal.h"
#include "materials/Mirror.h"
#include "materials/Mixed.h"
#include "materials/Null.h"
#include "materials/Plastic.h"
#include "materials/Substrate.h"

#include "parsers/LuaParser.h"

#include "renderers/Progressive.h"

#include "samplers/Random.h"
#include "samplers/Sobol.h"

#include "shapes/TriangleMesh.h"

#include "textures/Bilerp.h"
#include "textures/ImageMap.h"

#include "volumes/Homogeneous.h"

namespace Render
{
	enum AccelType
	{
		NONE,
		BINNEDKDTREE,
		BVH
	};

	//Setting these to negative gives parsers the flexibility to allow for more varied input styles.
	enum SpectrumColorType
	{
		RGB = -1,
		XYZ = -2
	};

	struct GraphicsState
	{
		vector<Reference<Primitive>> primitives;
		vector<Reference<Light>> lights;
		vector<Reference<LightCollection>> lightCollections;
	};

	class AnimatedNode
	{
	protected:
		typedef map<float, Transform *> TimedTransforms;
		TimedTransforms transforms;
	public:
		virtual ~AnimatedNode();
		/* If a new item is added, then returns true.  Otherwise the transform is updated and this will return false. */
		virtual bool AddTransform(const Transform *transform, float time);
		virtual bool RemoveTransform(float time);
		void Fill(vector<const Transform *> *transforms, vector<float> *times) const;
		void Swap(AnimatedNode *other);
	};

	class SceneNode : public ReferenceCounted
	{
	public:
		virtual ~SceneNode();
		virtual void GetAggregate(GraphicsState &gs) = 0;
	};

	class ShapeNode : public ReferenceCounted
	{
		Reference<Shape> shape;
	public:
		ShapeNode(const Reference<Shape> &shape);
		const Reference<Shape> &GetShape() const;
	};

	class GeometricNode : public SceneNode
	{
		Reference<Primitive> primitive;
		Reference<Primitive> accelerator;
		Reference<Light> light;
		AccelType accelType;
		//AccelType areaLightAccelType;
	public:
		struct AreaLightData
		{
			RSpectrumTexture emit;
			virtual AreaLight *GetAreaLight(const Reference<Shape> &shape, const Reference<Primitive> &accel) const = 0;
		};
		GeometricNode(const Reference<ShapeNode> &shape, const Reference<Material> &material, const AreaLightData *areaLightData, const Reference<Volume> &inner, const Reference<Volume> &outer);
		void SetAccelType(AccelType type);
		void GetAggregate(GraphicsState &gs);
	};

	struct DiffuseLightData : public GeometricNode::AreaLightData
	{
		AreaLight *GetAreaLight(const Reference<Shape> &shape, const Reference<Primitive> &accel) const;
	};

	class LightNode : public SceneNode
	{
	protected:
		Reference<Light> light;
	public:
		LightNode(const Reference<Light> &light);
		virtual void GetAggregate(GraphicsState &gs);
	};

	class AnimatedSceneNode : public SceneNode, public AnimatedNode
	{
	protected:
		bool traversed;
		vector<Reference<SceneNode>> children;
		Reference<Primitive> aggregate;
		vector<Reference<LightCollection>> lightCollections;
		vector<Reference<Light>> lights;
		AccelType accelType;
	public:
		AnimatedSceneNode(AccelType accelType = AccelType::BVH);
		AnimatedSceneNode(vector<Reference<SceneNode>> *nodes, AccelType accelType = AccelType::BVH);
		void SetAccelType(AccelType type);
		void GetAggregate(GraphicsState &gs);
	};

	class CameraNode : public AnimatedNode
	{
	protected:
		float sOpen, sClose, focald, lensr;
	public:
		CameraNode(float sOpen, float sClose, float focald, float lensr);
		virtual ~CameraNode();
		virtual Camera *MakeCamera(Film *film, float aspect = 0.f) const = 0;
	};

	class PerspectiveCamNode : public CameraNode
	{
		float fov;
	public:
		PerspectiveCamNode(float fov, float sOpen, float sClose, float focald, float lensr);
		Camera *MakeCamera(Film *film, float aspect = 0.f) const;
	};

	class SamplerNode
	{
	public:
		virtual ~SamplerNode();
		virtual Sampler *MakeSampler(const Film *film) const;
	};

	class SobolNode : public SamplerNode
	{
	public:
		Sampler *MakeSampler(const Film *film) const;
	};

	class RenderInstance : public AnimatedSceneNode
	{
		map<TexInfo, BlockedArray<RGBSpectrum> *> imgCache;
		map<TexInfo, BlockedArray<float> *> intensityCache;

		Film *film;
		Camera *camera;
		Sampler *sampler;
		Scene *scene;
		Renderer *renderer;
		SurfaceIntegrator *surf;
		VolumeIntegrator *vol;

		CameraNode *camNode;
		SamplerNode *samplerNode;
		
		bool isValid;

		LuaParser luaParser;

		template<typename T> 
		BlockedArray<T> *getTex(map<TexInfo, BlockedArray<T> *> &cache, const TexInfo &info);
	public:
		RenderInstance();
		~RenderInstance();

		Renderer *GetRenderer();
		bool IsValid() const;
		BlockedArray<float> *GetFloatImage(const TexInfo &info);
		BlockedArray<RGBSpectrum> *GetSpectrumImage(const TexInfo &info);
		void ImageFilm(int width, int height);
		void UseRandomSampling();
		void UseSobolSequence();
		void PerspectiveCamera(float fov, float sOpen, float sClose, float focald, float lensr);
		void BidirIntegrator(uint32_t lightBounces, uint32_t eyeBounces);
		void PathIntegrator(uint32_t bounces);
		void SingleIntegrator(float stepSize);
		void AddCameraTransform(const Transform *transform, float time);
		void Parse(const char *filename);
		void AddChild(const Reference<SceneNode> &node);
		void SceneInit();
		void SceneFinish();
		void ClearSceneGraph();
		void ClearScene();
	};
}
#endif
