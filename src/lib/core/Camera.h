/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_CAMERA_H
#define RENDERLIB_CORE_CAMERA_H

#include "Transform.h"
#include "Film.h"

namespace Render
{
	class Camera
	{
	public:
		static Transform RasterToScreen(float width, float height, float aspect);

		const AnimatedTransform toWorld;
		const Transform rasterToCamera;
		float sOpen, sClose, lensr, focald;
		Film *const film;

		Camera(const AnimatedTransform &toWorld, const Transform &persp, float sOpen, float sClose, float focald, float lensr, Film *film, float aspect = 0.f);
		virtual ~Camera();
		float GetTime(float timeU) const;
		virtual void GenerateRay(const CameraSample *sample, Ray *ray) const = 0;
		virtual void GenerateRayDifferential(const CameraSample *sample, RayDifferential *ray) const = 0;
		virtual bool SampleRay(const Point &p, Sample *sample, uint32_t offset, Ray *ray, float *x, float *y, float *pdf) const = 0;
		virtual float Pdf(const Ray &ray) const = 0;
	};
}

#endif