/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_PARALLEL_H
#define RENDERLIB_CORE_PARALLEL_H

#include "Memory.h"

#if defined(IS_WINDOWS)
#include <windows.h>
#endif

namespace Render
{
	inline int32_t AtomicCompareAndSwap(volatile int32_t *v, int32_t newVal, int32_t oldVal)
	{
	#if defined(IS_WINDOWS)
		return InterlockedCompareExchange((long *)v, newVal, oldVal);
	#elif defined(IS_APPLE_PPC)
		return OSAtomicCompareAndSwap32Barrier(oldVal, newVal, v);
	#else
		return __sync_val_compare_and_swap(v, oldVal, newVal);
	#endif
	}

	inline int64_t AtomicCompareAndSwap(volatile int64_t *v, int64_t newVal, int64_t oldVal)
	{
	#if defined(IS_WINDOWS)
		return InterlockedCompareExchange64(v, newVal, oldVal);
	#elif defined(IS_APPLE_PPC)
		return OSAtomicCompareAndSwap64Barrier(oldVal, newVal, v);
	#else
		return __sync_val_compare_and_swap(v, oldVal, newVal);
	#endif
	}

	union AtomicBits32 { float f; int32_t i; };
	inline float AtomicAdd(volatile float *ptr, float val)
	{
		AtomicBits32 oldVal, newVal;
		do
		{
			// On IA32/x64, adding a PAUSE instruction in compare/exchange loops
			// is recommended to improve performance.  (And it does!  Probably allows the next thread to pass through.)
			#if (defined(__i386__) || defined(__amd32__))
				__asm__ __volatile__ ("pause\n");
			#endif

			oldVal.f = *ptr;
			newVal.f = oldVal.f + val;
		} while(AtomicCompareAndSwap( ((volatile int32_t *) ptr), newVal.i, oldVal.i) != oldVal.i);

		return newVal.f;
	}

	union AtomicBits64 { double f; int64_t i; };
	inline double AtomicAdd(volatile double *ptr, double val)
	{
		AtomicBits64 oldVal, newVal;
		do
		{
			// On IA32/x64, adding a PAUSE instruction in compare/exchange loops
			// is recommended to improve performance.  (And it does!  Probably allows the next thread to pass through.)
			#if (defined(__i386__) || defined(__amd64__))
				__asm__ __volatile__ ("pause\n");
			#endif

			oldVal.f = *ptr;
			newVal.f = oldVal.f + val;
		} while(AtomicCompareAndSwap( ((volatile int64_t *) ptr), newVal.i, oldVal.i ) != oldVal.i);

		return newVal.f;
	}

	unsigned int NumThreads();

	class Task
	{
	public:
		virtual ~Task();
		virtual void Run(unsigned int threadIndex) = 0;
	};

	class ThreadPool
	{
		vector<Task *> tasks;
		unsigned int unfinishedTasks;

		boost::thread *threads;

		//Wait for the task queue to fill up with tasks.
		boost::interprocess::interprocess_semaphore *taskWait;
		//Lock the queue when popping off a task.
		boost::mutex *queueLock;
		//Lock for unfinishedTasks.
		boost::mutex *tasksRunning;
		//Alert task manager when all the tasks have finished.
		boost::condition_variable *tasksWaiting;

		MemoryArena arena;

		unsigned int nThreads;
	public:
		ThreadPool(unsigned int nThreads = 0);
		~ThreadPool();
		void Enqueue(vector<Task *> &tasks);
		void WaitForAllThreads();

		friend class ThreadPoolCallable;
	};

	class TaskSet
	{
		Task **tasks;

		unsigned int nUnfinished;
		unsigned int taskIndex;
		unsigned int nTasks;
		unsigned int nThreads;

		boost::mutex *taskWait;
		boost::condition_variable *hasTask;
		boost::mutex *tasksRunning;
		boost::condition_variable *tasksWaiting;

		boost::thread *threads;
	public:
		TaskSet(Task **tasks, unsigned int nTasks, unsigned int nThreads = 0);
		~TaskSet();
		void Work();
		void WaitForAllThreads();

		friend class TaskSetCallable;
	};
}

#endif
