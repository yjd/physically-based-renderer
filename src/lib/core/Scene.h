/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_SCENE_H
#define RENDERLIB_CORE_SCENE_H

#include "Light.h"
#include "Volume.h"

namespace Render
{
	class Scene
	{
		float r;
		Point c;
	public:
		const Reference<Primitive> aggregate;
		const Reference<LightCollection> lightRoot;
		TransformedLight *lights;
		vector<const TransformedLight *> environmentLights;
		Volume *volume;
		Scene(const Reference<Primitive> &aggregate, Volume *volume, const Reference<LightCollection> &lightRoot);
		~Scene();
		bool Intersect(const Ray &ray, Intersection *isect) const;
		bool IntersectP(const Ray &ray) const;
		bool Le(const Ray &ray, Spectrum &s) const;
		BBox Bounds() const;
		const float &Radius() const;
		const Point &Center() const;
	};
}

#endif
