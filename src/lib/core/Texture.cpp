/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "Texture.h"
#include "TexImage.h"

namespace Render
{
	TextureMapping2D::~TextureMapping2D(){}

	UVMapping2D::UVMapping2D(float su, float sv, float du, float dv)
		:	su(su), sv(sv), du(du), dv(dv){}

	void UVMapping2D::Map(const DifferentialGeometry &dg, float *s, float *t) const
	{
		*s = su * dg.u + du;
		*t = sv * dg.v + dv;
	}

	SphericalMapping2D::SphericalMapping2D(const Transform &toTexture, float su, float sv, float du, float dv)
		:	toTexture(toTexture), su(INV_TWOPI * su), sv(INV_PI * sv), du(du), dv(dv){}

	void SphericalMapping2D::Map(const DifferentialGeometry &dg, float *s, float *t) const
	{
		Vector v( Normalize((Vector) toTexture(dg.p)) );
		*s = SphericalPhi(v) * su + du;
		*t = SphericalTheta(v) * sv + dv;
	}

	CylindricalMapping2D::CylindricalMapping2D(const Transform &toTexture, float su, float sv, float du, float dv)
		:	toTexture(toTexture), su(su * INV_TWOPI), sv(sv), du(du), dv(dv){}
	void CylindricalMapping2D::Map(const DifferentialGeometry &dg, float *s, float *t) const
	{
		Vector v(toTexture(dg.p));
		*s = SphericalPhi(v) * su + du;
		*t = 0.5f - 0.5f * v.z * sv + dv;
	}

	PlanarMapping2D::PlanarMapping2D(const Vector &vu, const Vector &vv, float du, float dv)
		:	vu(vu), vv(vv), du(du), dv(dv){}

	void PlanarMapping2D::Map(const DifferentialGeometry &dg, float *s, float *t) const
	{
		*s = du + Dot((Vector) dg.p, vu);
		*t = dv + Dot((Vector) dg.p, vv);
	}

	EnvironmentMapping::~EnvironmentMapping(){}
	ImageWrap EnvironmentMapping::GetWrapMode() const
	{
		return ImageWrap::CLAMP;
	}
	void EnvironmentMapping::GetMapExtent(uint32_t width, uint32_t height, uint32_t *nu, uint32_t *nv) const
	{
		*nu = width;
		*nv = height;
	}
	void LatLongMapping::Map(float s, float t, float *sm, float *tm, Vector *wh, float *pdf) const
	{
		float phi = s * TWOPI;
		float theta = t * M_PI;
		float sintheta = sin(theta);
		*wh = SphericalDirection(sintheta, cos(theta), phi);
		*sm = s;
		*tm = t;
		if(pdf)
			*pdf = sintheta > 0.f ? 1.f / (TWOPI2 * sintheta) : 0.f;
	}
	void LatLongMapping::Map(const Vector &wh, float *s, float *t, float *pdf) const
	{
		float theta = SphericalTheta(wh);
		*s = SphericalPhi(wh) * INV_TWOPI;
		*t = theta * INV_PI;
		if(pdf)
		{
			float sintheta = sin(theta);
			*pdf = sintheta > 0.f ? 1.f / (TWOPI2 * sintheta) : 0.f;
		}
	}
	float LatLongMapping::Pdf(float s, float t, float *sm, float *tm) const
	{
		float sintheta = sin(t * M_PI);
		*sm = s;
		*tm = t;
		return sintheta > 0.f ? 1.f / (TWOPI2 * sin(t * M_PI)) : 0.f;
	}

	void VerticalCrossMapping::Map(float s, float t, float *sm, float *tm, Vector *wh, float *pdf) const
	{
		/* Although we can use the texture as-is, sampling becomes wasteful due to the black spots where faces don't occur.
		 * Instead, pretend the bottom two faces of the cross are actually adjacent to the top face and set sm and tm accordingly.
		 * Honestly though, any arrangement that works will work (duh).  Just make sure the pdf does the same thing.
		 */
		float sc = s * 3.f;
		float tc = t * 2.f;
		unsigned int so = min(2u, (unsigned int)(sc));
		unsigned int to = min(1u, (unsigned int)(tc));
		unsigned int face = to * 3 + so;
		switch(face)
		{
		case 0:
			*sm = s + 0.33333333f;
			*tm = t + 1.f;
			break;
		case 2:
			*sm = s - 0.33333333f;
			*tm = t + 1.5f;
			break;
		default:
			*sm = s;
			*tm = t;
		}
		*tm *= 0.5f;

		sc -= so;
		tc -= to;
		sc = 2.f * sc - 1.f;
		tc = 2.f * tc - 1.f;
		switch(face)
		{
		case 0:
			//-z
			*wh = Vector(sc,-tc,-1.f);
			break;
		case 1:
			//+z
			*wh = Vector(sc,tc,1.f);
			break;
		case 2:
			//-y
			*wh = Vector(sc,-1.f,tc);
			break;
		case 3:
			//-x
			*wh = Vector(-1.f,sc,-tc);
			break;
		case 4:
			//+y
			*wh = Vector(sc,1.f,-tc);
			break;
		case 5:
			//+x
			*wh = Vector(1.f,-sc,-tc);
			break;
		}

		float invma = 1.f / sqrt(sc * sc + tc * tc + 1.f);
		*wh *= invma;
		if(pdf)
			*pdf = invma * invma * invma * 0.04166667f; //1 / 24
	}

	void VerticalCrossMapping::Map(const Vector &wh, float *s, float *t, float *pdf) const
	{
		unsigned int axis = 0;
		float ma = abs(wh.x);
		float invma = abs(wh.y);
		if(invma > ma)
		{
			ma = invma;
			axis = 1;
		}
		invma = abs(wh.z);
		if(invma > ma)
		{
			ma = invma;
			axis = 2;
		}
		invma = 1.f / ma;

		switch(axis)
		{
		case 0:
			if(wh.x > 0)
			{
				*s = -wh.y * invma + 4.995f;//This seems like the best way to fix a weird shift in this face.
				*t = -wh.z * invma + 3.f;
			}
			else
			{
				*s = wh.y * invma + 1.f;
				*t = -wh.z * invma + 3.f;
			}
			break;
		case 1:
			if(wh.y >= 0)
			{
				*s = wh.x * invma + 3.f;
				*t = -wh.z * invma + 3.f;
			}
			else
			{
				*s = wh.x * invma + 3.f;
				*t = wh.z * invma + 7.f;
			}
			break;
		case 2:
			if(wh.z > 0)
			{
				*s = wh.x * invma + 3.f;
				*t = wh.y * invma + 1.f;
			}
			else
			{
				*s = wh.x * invma + 3.f;
				*t = -wh.y * invma + 5.f;
			}
			break;
		}
		*s *= 0.16666666f;
		*t *= 0.125f;

		if(pdf)
			*pdf = invma * invma * invma * 0.04166667f; //1 / 24
	}
	float VerticalCrossMapping::Pdf(float s, float t, float *sm, float *tm) const
	{
		float sc = s * 3.f;
		float tc = t * 2.f;
		unsigned int so = min(2u, (unsigned int)(sc));
		unsigned int to = min(1u, (unsigned int)(tc));
		switch(to * 3 + so)
		{
		case 0:
			*sm = s + 0.33333333f;
			*tm = t + 1.f;
			break;
		case 2:
			*sm = s - 0.33333333f;
			*tm = t + 1.5f;
			break;
		default:
			*sm = s;
			*tm = t;
		}
		*tm *= 0.5f;
		sc -= so;
		tc -= to;
		sc = 2.f * sc - 1.f;
		tc = 2.f * tc - 1.f;

		float invma = 1.f / sqrt(sc * sc + tc * tc + 1.f);

		return invma * invma * invma * 0.04166667f;
	}
	void VerticalCrossMapping::GetMapExtent(uint32_t width, uint32_t height, uint32_t *nu, uint32_t *nv) const
	{
		*nu = width;
		*nv = height / 2u;
	}
}