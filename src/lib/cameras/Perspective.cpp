/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "cameras/Perspective.h"
#include "Sampler.h"
#include "MonteCarlo.h"

namespace Render
{
	Transform PerspectiveCamera::Perspective(float fov)
	{
		float tanFov = tan(fov * 0.5f);
		return Transform(Mat4(	tanFov, 0, 0, 0,
								0, tanFov, 0, 0,
								0, 0, 1.f, -1.f,
								0, 0, 0, 1.f));
	}
	PerspectiveCamera::PerspectiveCamera(const AnimatedTransform &toWorld, float fov, float sOpen, float sClose, float focald, float lensr, Film *film, float aspect)
		:	Camera(toWorld, Perspective(fov), sOpen, sClose, focald, lensr, film, aspect)
	{
		if(aspect == 0.f)
			aspect = float(film->width) / float(film->height);

		if(aspect > 0.f)
			imgPlaneDist = float(film->width) / (2.f * aspect * tan(fov * 0.5f));
		else
			imgPlaneDist = float(film->height) * aspect / (2.f * tan(fov * 0.5f));

		Point o, dp;
		rasterToCamera(Point(), &o);
		rasterToCamera(Point(1.f, 0, 0), &dp);
		dx = dp - o;
		rasterToCamera(Point(0, 1.f, 0), &dp);
		dy = dp - o;
	}

	void PerspectiveCamera::GenerateRay(const CameraSample *sample, Ray *ray) const
	{
		Ray r;
		rasterToCamera(Point(sample->x, sample->y, 0.f), (Point *)(&r.d));

		if(lensr > 0.f)
		{
			ConcentricSampleDisc(sample->u, sample->v, &r.o.x, &r.o.y);
			r.o.x *= lensr;
			r.o.y *= lensr;
			r.d = Vector(focald * r.d.x - r.o.x, focald * r.d.y - r.o.y, -focald);
		}

		r.d.Normalize();
		r.time = ray->time;
		toWorld(r.time, r, ray);
	}

	void PerspectiveCamera::GenerateRayDifferential(const CameraSample *sample, RayDifferential *ray) const
	{
		RayDifferential r;
		rasterToCamera(Point(sample->x, sample->y, 0.f), (Point *)(&r.d));
		r.rxDirection = Normalize(r.d + dx);
		r.ryDirection = Normalize(r.d + dy);

		if(lensr > 0.f)
		{
			ConcentricSampleDisc(sample->u, sample->v, &r.o.x, &r.o.y);
			r.o.x *= lensr;
			r.o.y *= lensr;

			r.d = Normalize(Vector(focald * r.d.x - r.o.x, focald * r.d.y - r.o.y, -focald));
		}
		
		r.d.Normalize();
		r.rxOrigin = r.ryOrigin = r.o;
		r.hasDifferentials = true;

		toWorld(r.time, r, ray);
	}

	bool PerspectiveCamera::SampleRay(const Point &p, Sample *sample, uint32_t offset, Ray *ray, float *x, float *y, float *pdf) const
	{
		Point pCam;
		toWorld(ray->time, p, &pCam, true);

		//Right handed coordinate system, camera looks down at z = -1
		if(pCam.z >= 0)
			return false;

		if(lensr > 0)
		{
			Ray rCam;
			rCam.time = ray->time;
			ConcentricSampleDisc(	sample->sampler->GetSample(sample, offset),
									sample->sampler->GetSample(sample, offset),
									&rCam.o.x, &rCam.o.y);

			rCam.o *= lensr;
			rCam.d = pCam - rCam.o;

			//Set the length of the ray so that o + d ends up on the focal plane.
			rCam.d *= focald / -rCam.d.z;

			//Find the point on the focal plane relative to origin.
			pCam = rCam.o + rCam.d;
			//Project the point onto the image plane z = -1 and then find raster coordinates.
			pCam /= -pCam.z;

			pCam = rasterToCamera(pCam, true);
			if(pCam.x < 0 || pCam.y < 0 || pCam.x >= film->width || pCam.y >= film->height)
				return false;

			*x = pCam.x;
			*y = pCam.y;

			rCam.d.Normalize();
			toWorld(ray->time, rCam, ray);

			float cosCamera = -rCam.d.z;
			//Using more steps is probably more numerically stable.
			*pdf = cosCamera / imgPlaneDist;
			*pdf *= *pdf * cosCamera;
		}
		else
		{
			//Project the point onto the image plane z = -1 and then find raster coordinates.
			pCam /= -pCam.z;
			pCam = rasterToCamera(pCam, true);
			if(pCam.x < 0 || pCam.y < 0 || pCam.x >= film->width || pCam.y >= film->height)
				return false;

			*x = pCam.x;
			*y = pCam.y;
			Ray rCam;
			rCam.time = ray->time;
			rasterToCamera(pCam, (Point *)(&rCam.d));
			rCam.d.Normalize();
			toWorld(ray->time, rCam, ray);
		
			float cosCamera = -rCam.d.z;
			//Using more steps is probably more numerically stable.
			*pdf = cosCamera / imgPlaneDist;
			*pdf *= *pdf * cosCamera;
		}

		ray->mint = 0.f;
		ray->maxt = Distance(p, ray->o) * (1.f - RAYEPSILON);

		return *pdf > 0.f;
	}

	float PerspectiveCamera::Pdf(const Ray &ray) const
	{
		float cosCamera = -ray.d.z;
		float wToImage = cosCamera / imgPlaneDist;

		return cosCamera * wToImage * wToImage;
	}
}