/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CAMERAS_PERSPECTIVE_H
#define RENDERLIB_CAMERAS_PERSPECTIVE_H

#include "Camera.h"

namespace Render
{
	class PerspectiveCamera : public Camera
	{
		Vector dx, dy;
		float imgPlaneDist;
	public:
		static Transform Perspective(float fov);
		PerspectiveCamera(const AnimatedTransform &toWorld, float fov, float sOpen, float sClose, float focald, float lensr, Film *film, float aspect = 0.f);
		void GenerateRay(const CameraSample *sample, Ray *ray) const;
		void GenerateRayDifferential(const CameraSample *sample, RayDifferential *ray) const;
		bool SampleRay(const Point &p, Sample *sample, uint32_t offset, Ray *ray, float *x, float *y, float *pdf) const;
		float Pdf(const Ray &ray) const;
	};
}

#endif