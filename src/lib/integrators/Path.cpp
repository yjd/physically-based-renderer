/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "integrators/Path.h"

namespace Render
{
	PathIntegrator::PathIntegrator(uint32_t pathLength)
		:	pathLength(pathLength)
	{
	}

	
	void PathIntegrator::requiredSamples(const Sampler *sampler, const Scene *scene, unsigned int *n, unsigned int *nDecorrelated)
	{
		*n = 7 * pathLength + 3 * (pathLength - 1);
		lightPdf = 1.f / scene->lightRoot->Size();
	}
	void PathIntegrator::setSampleOffsets(unsigned int n, unsigned int nDecorrelated)
	{
		sampleOffset = n;
	}

	void PathIntegrator::Li(const Renderer *renderer, Scene *scene, Camera *camera, Sample *sample, Intersection *isect, BSDF *bsdf, Ray &ray, Spectrum &L, MemoryArena &arena) const
	{
		if(!bsdf)
			return;

		uint32_t offset = sampleOffset;
		uint32_t n = 1;
		Vector wo = -ray.d, wi;
		Point p = isect->dg.p;
		Spectrum throughput = 1.f;

		Spectrum ld, f;
		float pdf;
		BxDFType sampledType;
		float u[3];
		while(true)
		{
			if(SampleOneLight(renderer, scene, sample, offset, bsdf, p, ray, *isect, ld, arena))
				L += throughput * ld;

			offset += 7;

			if(n >= pathLength)
				break;
			
			sample->sampler->GetSamples(sample, offset, u, 3);
			if(!bsdf->Sample_f(wo, &wi, u, &pdf, ALL, f, &sampledType) || f.IsBlack())
				break;

			offset += 3;

			bool specular = (sampledType & SPECULAR) != BxDFNULL;

			ray = Ray(p, wi, RAYEPSILON, INFINITY, ray.time);
			throughput.MultWeighted(1.f / pdf, f);
			if(!scene->Intersect(ray, isect))
			{
				if(specular && scene->Le(ray, ld = 0.f))
				{
					//renderer->vol->Transmittance(renderer, scene, sample, ray, throughput, arena);
					L += throughput * ld;
				}
				break;
			}
			
			//renderer->vol->Transmittance(renderer, scene, sample, ray, throughput, arena);

			//GetBSDF also gets shading differentials.
			bsdf = isect->GetBSDF(ray, arena);

			if(specular && isect->Le(wo, ld))
				L += throughput * ld;

			if(!bsdf)
				break;

			wo = -ray.d;

			p = isect->dg.p;

			++n;
		}
	}
}
