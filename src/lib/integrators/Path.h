/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_INTEGRATORS_PATH_H
#define RENDERLIB_INTEGRATORS_PATH_H

#include "Integrator.h"

namespace Render
{
	class PathIntegrator : public SurfaceIntegrator
	{
		uint32_t pathLength;
		uint32_t sampleOffset;
		float lightPdf;
		void requiredSamples(const Sampler *sampler, const Scene *scene, unsigned int *n, unsigned int *nDecorrelated);
		void setSampleOffsets(unsigned int n, unsigned int nDecorrelated);
	public:
		PathIntegrator(uint32_t pathLength);
		void Li(const Renderer *renderer, Scene *scene, Camera *camera, Sample *sample, Intersection *isect, BSDF *bsdf, Ray &ray, Spectrum &L, MemoryArena &arena) const;
	};
}

#endif