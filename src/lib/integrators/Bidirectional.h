/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_INTEGRATORS_BIDIRECTIONAL_H
#define RENDERLIB_INTEGRATORS_BIDIRECTIONAL_H

#include "Integrator.h"

namespace Render
{
	class BidirIntegrator : public SurfaceIntegrator
	{
		uint32_t maxs;
		uint32_t maxt;
		uint32_t sampleOffset;
		uint32_t sampleOffsetDec;
		float lightPdf;
		float nLightSubPaths;
		void requiredSamples(const Sampler *sampler, const Scene *scene, unsigned int *n, unsigned int *nDecorrelated);
		void setSampleOffsets(unsigned int n, unsigned int nDecorrelated);
		void connect(const Renderer *renderer, Scene *scene, Camera *camera, Sample *sample, uint32_t offset, PathVertex **eyeVerts, PathVertex *lightVert, Ray &ray, uint32_t s, uint32_t t, Spectrum &L) const;
		bool directLighting(const Renderer *renderer, Scene *scene, Camera *camera, Sample *sample, uint32_t offset, PathVertex *eyeVert, Ray &ray, Intersection &isect, uint32_t t, Spectrum &L) const;
		float mis(float f) const;
	public:
		BidirIntegrator(uint32_t lightBounces, uint32_t eyeBounces);
		void Li(const Renderer *renderer, Scene *scene, Camera *camera, Sample *sample, Intersection *isect, BSDF *bsdf, Ray &ray, Spectrum &L, MemoryArena &arena) const;
	};
}

#endif