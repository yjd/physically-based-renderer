/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_INTEGRATORS_SINGLE_H
#define RENDERLIB_INTEGRATORS_SINGLE_H

#include "Integrator.h"

namespace Render
{
	class SingleIntegrator : public VolumeIntegrator
	{
		float stepSize;
		uint32_t sampleOffset;
		void requiredSamples(const Sampler *sampler, const Scene *scene, unsigned int *n, unsigned int *nDecorrelated);
		void setSampleOffsets(unsigned int n, unsigned int nDecorrelated);
	public:
		SingleIntegrator(float stepSize);
		void Li(const Renderer *renderer, const Scene *scene, Sample *sample, const Ray &ray, Spectrum &L, Spectrum &T, MemoryArena &arena) const;
		void Transmittance(const Renderer *renderer, const Scene *scene, Sample *sample, const Ray &ray, Spectrum &L, MemoryArena &arena, bool useSample) const;
	};
}

#endif