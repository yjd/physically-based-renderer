/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "integrators/Bidirectional.h"

namespace Render
{
	BidirIntegrator::BidirIntegrator(uint32_t lightBounces, uint32_t eyeBounces)
		:	maxs(max(1u, lightBounces)), maxt(max(1u, eyeBounces)), lightPdf(1.f)//I'd rather make specialized integrators than add extra if-statements for purely path tracing and light tracing cases.
	{
	}
	void BidirIntegrator::requiredSamples(const Sampler *sampler, const Scene *scene, unsigned int *n, unsigned int *nDecorrelated)
	{
		*n =	3 * (maxt - 1) + //Sample the BSDF, first camera edge is sampled by camera
				4 * maxt; //Randomly pick a light and do direct lighting estimation

		*nDecorrelated =	6 + //Randomly pick a light and sample it (5 samples max for area lights)
							3 * (maxs - 1) + //Sample the BSDF, first light edge is sampled by light
							2 * maxs; //Camera sampling from light tracing

		nLightSubPaths = sampler->nPixels;
		lightPdf = 1.f / float(scene->lightRoot->Size());
	}
	void BidirIntegrator::setSampleOffsets(unsigned int n, unsigned int nDecorrelated)
	{
		sampleOffset = n;
		sampleOffsetDec = nDecorrelated;
	}
	float BidirIntegrator::mis(float f) const
	{
		return f * f;
	}

	void BidirIntegrator::connect(const Renderer *renderer, Scene *scene, Camera *camera, Sample *sample, uint32_t offset, PathVertex **eyeVerts, PathVertex *lightVert, Ray &ray, uint32_t s, uint32_t t, Spectrum &L) const
	{
		//If the light vertex has only specular components, connection is pointless.
		if(lightVert->bsdf->specularOnly)
			return;

		Spectrum f;
		Spectrum li;
		Vector wo;
		float x, y, pdf;
		if(camera->SampleRay(lightVert->p, sample, offset, &ray, &x, &y, &pdf))
		{
			wo = -ray.d;
			f = lightVert->bsdf->f(lightVert->w, wo, ALL);
			if(!f.IsBlack() && !scene->IntersectP(ray))
			{
				//Note that this is the inversion of the camera's pdf.
				pdf = 1.f / (nLightSubPaths * pdf * Distance2(ray.o, lightVert->p));
				
				float weight = 1.f + mis(lightVert->bsdf->PdfWtoA(ray.d) * pdf) * (lightVert->misConnect + (lightVert->misFactors > 0 ? (lightVert->misFactors * mis(lightVert->bsdf->Pdf(wo, lightVert->w, ALL))) : 0.f));
				camera->film->SplatSample(x, y, f * lightVert->alpha * (pdf / weight));
			}
		}
		
		for(uint32_t i = 0; i < t; ++i)
		{
			PathVertex *eyeVert = eyeVerts[i];
			//If the eye vertex has only specular components, connection is pointless.
			if(eyeVert->bsdf->specularOnly)
				continue;

			Vector w = lightVert->p - eyeVert->p;
			float dist2 = w.Length2();
			
			if(dist2 == 0)
				continue;

			float dist = sqrt(dist2);
			ray = Ray(eyeVerts[i]->p, w / dist, dist * RAYEPSILON, dist * (1.f - RAYEPSILON), ray.time);

			wo = -ray.d;

			f = eyeVert->bsdf->f(eyeVert->w, ray.d, ALL);
			
			if(f.IsBlack())
				continue;

			li = lightVert->bsdf->f(lightVert->w, wo, ALL);
			if(li.IsBlack())
				continue;

			if(scene->IntersectP(ray))
				continue;

			float weight =	1.f +
							mis(lightVert->bsdf->Pdf(lightVert->w, wo, ALL) * eyeVert->bsdf->PdfWtoA(ray.d, dist2)) *
							(eyeVert->misConnect + (eyeVert->misFactors > 0.f ? (eyeVert->misFactors * mis(eyeVert->bsdf->Pdf(ray.d, eyeVert->w, ALL))) : 0.f)) +
							mis(eyeVert->bsdf->Pdf(eyeVert->w, ray.d, ALL) * lightVert->bsdf->PdfWtoA(wo, dist2)) *
							(lightVert->misConnect + (lightVert->misFactors > 0.f ? (lightVert->misFactors * mis(lightVert->bsdf->Pdf(wo, lightVert->w, ALL))) : 0.f));

			//The cosine terms are all accounted for by the bsdfs/pdfs.  Must still divide by the squared distance to convert to probabilities wrt solid angle for the connected path.
			L.AddWeighted(1.f / (dist2 * weight), eyeVert->alpha * f * li * lightVert->alpha);
		}
	}
	bool BidirIntegrator::directLighting(const Renderer *renderer, Scene *scene, Camera *camera, Sample *sample, uint32_t offset, PathVertex *eyeVert, Ray &ray, Intersection &isect, uint32_t t, Spectrum &L) const
	{
		const BSDF *bsdf = eyeVert->bsdf;
		if(!bsdf->specularOnly)
		{
			//Sample the light uniformly.  Lights are instanced, so calculating light pdfs based on relative power will require a linear search in the case of MIS.
			unsigned int which = min((unsigned int)(sample->sampler->GetSample(sample, offset++) * scene->lightRoot->Size()), scene->lightRoot->Size() - 1);
			const TransformedLight *tLight = &scene->lights[which];
			const Light *light = tLight->light;
			const Transform &toWorld = tLight->toWorld;

			const Vector &wo = eyeVert->w;
			Vector wi;
			float pdfL;
			Spectrum li;
			if(light->SampleL(&toWorld, eyeVert->p, sample, offset, &wi, li, &pdfL, &isect, &ray) && !li.IsBlack())
			{
				Spectrum f = eyeVert->bsdf->f(wo, wi, NON_SPECULAR);
				if(!f.IsBlack() && !scene->IntersectP(ray))
				{
					/* One of the components is the ratio between sampling via bsdf and direct lighting (pdfL).
					 * For those, the path and PdfWToA parts cancel out, leaving pdfF / (pdfL * lightPdf).

					 * One of the coefficients is the ratio between emissionPdfA / directPdfA.
					 * emissionPdfA	= (emissionPdfW * lightPickProbability * cosAtP(wi)) / distance^2)
					 * directPdfA	= (pdfL * lightPickProbability * cosAtLight(-wi)) / distance^2)
					 * emissionPdfA / directPdfA = emissionPdfW * cosAtP(wi) / (pdfL * cosAtLight(-wi))
					 */
					float emissionPdfW = light->Pdf(&toWorld, scene, isect.dg.p, -wi);
					float weight =	1.f +
									( light->IsDelta() ? 0.f : mis(bsdf->Pdf(wo, wi, ALL) / (lightPdf * pdfL)) ) +
									mis( (emissionPdfW * bsdf->PdfWtoA(wi)) / (pdfL * AbsDot(isect.dg.n, -wi)) ) *
									( eyeVert->misConnect + (eyeVert->misFactors == 0.f ? 0.f : (eyeVert->misFactors * mis(bsdf->Pdf(wi, wo, ALL)))) );

					L = f * li / (weight * pdfL * lightPdf);
					return true;
				}
			}
		}

		return false;
	}
	void BidirIntegrator::Li(const Renderer *renderer, Scene *scene, Camera *camera, Sample *sample, Intersection *isect, BSDF *bsdf, Ray &ray, Spectrum &L, MemoryArena &arena) const
	{
		//Vertex weights are updated iteratively.  All methods of this class depend on this assumption.  Thus all vertices hold the necessary weight information.
		uint32_t offset = sampleOffset;
		uint32_t t = 0;
		Vector wo = -ray.d, wi;

		PathVertex **eyeVerts = NULL;
		PathVertex *vertex = NULL;

		Spectrum ld, f;
		float pdf;
		BxDFType sampledType;
		float u[3];
		//Unlike the path integrator, light paths can still be traced
		if(bsdf != NULL)
		{
			eyeVerts = (PathVertex **) arena.Alloc(sizeof(PathVertex *) * (maxt));
			vertex = ARENA_ALLOC(arena, PathVertex)(1.f, isect->dg.p, wo, bsdf, 0.f, bsdf->specularOnly ? 0.f :	mis(nLightSubPaths * camera->Pdf(ray) / 
																					 bsdf->PdfWtoA(wo, Distance2(isect->dg.p, ray.o))));
			eyeVerts[t++] = vertex;
			//Trace eye vertices first to reuse objects passed as object parameters.
			while(true)
			{
				if(directLighting(renderer, scene, camera, sample, offset, vertex, ray, *isect, t, ld))
					L += vertex->alpha * ld;
			
				offset += 4;

				if(t >= maxt)
					break;
			
				sample->sampler->GetSamples(sample, offset, u, 3);
				if(!bsdf->Sample_f(wo, &wi, u, &pdf, ALL, f, &sampledType) || f.IsBlack())
					break;

				offset += 3;

				bool specular = (sampledType & SPECULAR) != BxDFNULL;

				ray = Ray(vertex->p, wi, RAYEPSILON, INFINITY, ray.time);
				float pdfFRev = t > 1 && !specular ? mis(vertex->bsdf->Pdf(ray.d, vertex->w, ALL)) : 1.f;
				wo = -ray.d;

				pdf = 1.f / pdf;
				f.MultWeighted(pdf, vertex->alpha);
				float vertexRevCosW = vertex->bsdf->PdfWtoA(ray.d);

				if(!scene->Intersect(ray, isect))
				{
					if(scene->environmentLights.size() > 0)
					{
						for(auto i = scene->environmentLights.begin(); i != scene->environmentLights.end(); ++i)
						{
							float pdfL;
							float emissionPdfW;
							if((*i)->light->Le(&(*i)->toWorld, scene, ray, ld, &pdfL, &emissionPdfW, &isect->ng))
							{
								float weight = 1.f;
								if(!specular)
								{
									weight +=	mis( lightPdf * pdfL * pdf ) +
												mis( emissionPdfW * lightPdf * vertexRevCosW * pdf / AbsDot(isect->ng, wo) ) *
												(vertex->misConnect + vertex->misFactors * pdfFRev);
								}
								else if(vertex->misFactors != 0)
									weight +=	mis( lightPdf * emissionPdfW * vertexRevCosW / AbsDot(isect->ng, wo)) * (vertex->misFactors);

								ld /= weight;
								L += f * ld;
							}
						}
					}
					break;
				}
			
				//GetBSDF also gets shading differentials.
				bsdf = isect->GetBSDF(ray, arena);

				if(isect->Le(wo, ld))
				{
					float weight = 1.f;

					if(!specular)
					{
						const Light *light = isect->prim->GetAreaLight();
						float pdfL = light->Pdf(&isect->toWorld, vertex->p, ray.d);
						float emissionPdfW = light->Pdf(&isect->toWorld, scene, isect->dg.p, wo);
						weight +=	mis( lightPdf * pdfL * pdf ) +
									mis( emissionPdfW * lightPdf * vertexRevCosW * pdf / AbsDot(isect->ng, wo) ) *
									( vertex->misConnect + vertex->misFactors * pdfFRev );
					}
					else if(vertex->misFactors != 0)
					{
						//In the specular case, it's still possible that light tracing samples the new ray.  This time we don't multiply by pdf because the dirac components should cancel out.
						const Light *light = isect->prim->GetAreaLight();
						float emissionPdfW = light->Pdf(&isect->toWorld, scene, isect->dg.p, wo);
						weight +=	mis( lightPdf * emissionPdfW * vertexRevCosW / AbsDot(isect->ng, wo)) * (vertex->misFactors);
					}

					ld /= weight;
					L += f * ld;
				}

				if(!bsdf)
					break;
			
				if(specular)
					vertex = ARENA_ALLOC(arena, PathVertex)(f, isect->dg.p, wo, bsdf, vertex->misFactors == 0.f ? 0.f : (vertex->misFactors * mis(vertexRevCosW / bsdf->PdfWtoA(wo))), 0.f, true);
				else
				{
					pdf = pdf / bsdf->PdfWtoA(wo);
					vertex = ARENA_ALLOC(arena, PathVertex)(f, isect->dg.p, wo, bsdf,
															(vertex->misConnect + vertex->misFactors * pdfFRev) * mis(pdf * vertexRevCosW), mis(pdf * Distance2(isect->dg.p, vertex->p)),
															false);
				}

				eyeVerts[t++] = vertex;
			}
		}
		offset = sampleOffsetDec;
		unsigned int which = min((unsigned int)(sample->sampler->GetSample(sample, offset++) * scene->lightRoot->Size()), scene->lightRoot->Size() - 1);
		
		const TransformedLight *tLight = &scene->lights[which];

		if(!tLight->light->SampleL(&tLight->toWorld, scene, sample, offset, &ray, isect, ld, &pdf))
			return;

		offset += 5;

		float cosLight = AbsDot(isect->ng, ray.d);

		if(!scene->Intersect(ray, isect))
			return;

		bsdf = isect->GetBSDF(ray, arena);
		if(!bsdf)
			return;

		wo = -ray.d;

		float cosBsdf = bsdf->PdfWtoA(wo);

		vertex = ARENA_ALLOC(arena, PathVertex)(ld / (pdf * lightPdf), isect->dg.p, wo, bsdf,
					tLight->light->IsDelta() ? 0.f : mis(cosLight / (pdf * lightPdf * cosBsdf)),
					bsdf->specularOnly ? 0.f : mis(tLight->light->Pdf(&tLight->toWorld, isect->dg.p, wo) * cosLight / (pdf * cosBsdf)));

		uint32_t s = 1;
		while(true)
		{
			connect(renderer, scene, camera, sample, offset, eyeVerts, vertex, ray, s, t, L);

			offset += 2;
			if(s == maxs)
				break;

			sample->sampler->GetSamples(sample, offset, u, 3);
			if(!bsdf->Sample_f(wo, &wi, u, &pdf, ALL, f, &sampledType) || f.IsBlack())
				break;

			offset += 3;

			ray = Ray(vertex->p, wi, RAYEPSILON, INFINITY, ray.time);

			if(!scene->Intersect(ray, isect))
				break;

			bsdf = isect->GetBSDF(ray, arena);
			if(!bsdf)
				break;

			bool specular = (sampledType & SPECULAR) != BxDFNULL;
			
			pdf = 1.f / pdf;
			f.MultWeighted(pdf, vertex->alpha);
			float vertexRevCosW = vertex->bsdf->PdfWtoA(ray.d);
			wo = -ray.d;
			//It's important to use placement new here rather than assignment.  The optimizer causes problems with the latter related to the BSDF and results in black pixels.  At least in vc++.
			if(specular)
				new (vertex) PathVertex(f, isect->dg.p, wo, bsdf, vertex->misFactors == 0.f ? 0.f : (vertex->misFactors * mis(vertexRevCosW / bsdf->PdfWtoA(wo))), 0.f, true);
			else
			{
				float pdfFRev = mis(vertex->bsdf->Pdf(ray.d, vertex->w, ALL));
				pdf = pdf / bsdf->PdfWtoA(wo);
				new (vertex) PathVertex(f, isect->dg.p, wo, bsdf,
									(vertex->misConnect + vertex->misFactors * pdfFRev) * mis(pdf * vertexRevCosW), mis(pdf * Distance2(isect->dg.p, vertex->p)),
									false);
			}

			++s;
		}
	}
}
