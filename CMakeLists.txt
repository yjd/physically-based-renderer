#Renderer Project
cmake_minimum_required(VERSION 2.8)
project(Renderer)

#Check c++11 / c++0x.
include(CheckCXXCompilerFlag)

CHECK_CXX_COMPILER_FLAG("-std=c++11" CXX11)
CHECK_CXX_COMPILER_FLAG("-std=c++0x" CXX0X)
CHECK_CXX_COMPILER_FLAG("-std=gnu++0x" GXX0X)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread")

if(CXX11)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
elseif(CXX0X)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
elseif(GXX0X)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++0x")
else()
	message(WARNING "The compiler ${CMAKE_CXX_COMPILER} does not seem to support any c++11 flags.  Defaulting to c++11.")
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11") 
endif()

set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -D_DEBUG")

find_package(OpenEXR REQUIRED)
if(NOT OPENEXR_FOUND)
	message(FATAL_ERROR "OpenEXR was not found.")
endif(NOT OPENEXR_FOUND)

#set(Boost_USE_STATIC_LIBS ON)
find_package(
	Boost 1.47.0
	COMPONENTS
		thread
		date_time
		filesystem
	REQUIRED
)
if(NOT Boost_FOUND)
	message(FATAL_ERROR "Boost was not found.")
endif(NOT Boost_FOUND)

include_directories(${OPENEXR_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})
link_directories(${OPENEXR_LIBRARIES} ${Boost_LIBRARY_DIRS})

add_subdirectory(${CMAKE_SOURCE_DIR}/src)

MESSAGE( STATUS "CMAKE_BUILD_TYPE: " ${CMAKE_BUILD_TYPE} )

MESSAGE( STATUS "CMAKE_CXX_FLAGS: ${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_RELEASE} " )
