Film:Image(1920, 1080)

Camera:Perspective(50)
--Camera:SetTransform(Transform:New():LookAt({0,10,0},{0,0,0},{0,0,-1}))
Camera:SetTransform(Transform:New():LookAt({-3,2,4.5},{-0.5,0.75,2},{0,1,0}))

Scene:AddChildren(SceneNode:AnimatedNode
({
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh
		(
			Transform:New():Scale(25,1,25),
		{
			-0.5, 0.0, 0.5,
			0.5, 0.0, 0.5,
			0.5, 0.0, -0.5,
			-0.5, 0.0, -0.5
		},
		{
			0, 1, 2,
			0, 3, 2
		},
		{
			0,0,
			1,0,
			1,1,
			0,1
		}),
		Material:Matte({0.02,0.12,0.2})
	),
	SceneNode:AnimatedNode(
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh(Transform:New():Translate(1,0.85,3):RotateY(math.rad(250)):RotateX(math.rad(90)):Scale(0.0015,0.0015,0.0015), "geometry/lucy.ply"), Material:Plastic(0.5, 0.25, 0.001)
	)):SetAccelerator(Accelerator.BINNEDKDTREE),
	SceneNode:AnimatedNode(
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh(Transform:New():Translate(2.5,0,3):RotateY(math.rad(180)):RotateX(math.rad(90)):Scale(0.5,0.5,0.5), "geometry/teapot.ply"), Material:Plastic(1, 0.75, 0.0001)
	)):SetAccelerator(Accelerator.BINNEDKDTREE),
	SceneNode:AnimatedNode(
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh(Transform:New():Translate(0,0.8,0):RotateY(math.rad(180)):Scale(0.02,0.02,0.02), "geometry/xyzrgb_dragon.ply"), Material:Metal(1, 1, 0.001)
	)):SetAccelerator(Accelerator.BINNEDKDTREE),--]]
	SceneNode:AnimatedNode(
		SceneNode:InfiniteAreaLight(TextureInfo:New("textures/envmaps/plainfield.exr"),500)
	):SetTransform(Transform:New():RotateY(math.rad(90)):RotateX(math.rad(90)))--]]
	--[[
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh
		(
			Transform:New():Translate(-2, 10, 0):Scale(5, 1, 5),
		{
			-0.5, 0.0, 0.5,
			0.5, 0.0, 0.5,
			0.5, 0.0, -0.5,
			-0.5, 0.0, -0.5
		},
		{
			0, 2, 1,
			0, 3, 2
		}),
		AreaLight:Diffuse(10)
	)--]]
}))