Film:Image(3840, 1080)

Camera:Perspective(30, 0, 0, 12, 0.1)
Camera:SetTransform(Transform:New():LookAt({6,4,5},{-3,2,-2},{0,1,0}))
Integrator:Path(4)
pyramid = 
{
	-0.5, 0, 0.5,
	0.5, 0, 0.5,
	0.5, 0, -0.5,
	-0.5, 0, -0.5,
	0, 1, 0
}
			
pyramidIndices =
{
	0, 1, 2,
	0, 2, 3,
	0, 1, 4,
	1, 2, 4,
	2, 3, 4,
	0, 3, 4
}

function sierpinskiTranslate(verts, indices, vCopy, nv, ni, startV, startI, x, y, z)
	for i=1,nv,3 do
		verts[startV + i]		= vCopy[i] +		x;
		verts[startV + i + 1] 	= vCopy[i + 1] +	y;
		verts[startV + i + 2] 	= vCopy[i + 2] +	z;
	end
	if(startI == 0) then
		return
	end
		
	for i=1,ni do
		indices[startI + i] = indices[i] + startV / 3
	end
end

--This applies a fractal iteration ONCE to a set of vertices.  Square base.  Assumes the center of the base is the origin.
--Note that although vertices can be generated using transforms and shape nodes, new shape nodes will be created each time, which will slow down the deletion process when the scene closes.
function sierpinskiGasketSquare(verts, indices, height)
	local nv = #verts
	local ni = #indices
	local vCopy = {}
	for i,v in pairs(verts) do
		vCopy[i] = v
	end
	
	for i,v in pairs(vCopy) do
		vCopy[i] = v * 0.5
	end
	
	sierpinskiTranslate(verts, indices, vCopy, nv, ni, 0, 0, 
						0, height * 0.5, 0)
	
	sierpinskiTranslate(verts, indices, vCopy, nv, ni, nv, ni,
						-0.25, 0, 0.25)			
						
	sierpinskiTranslate(verts, indices, vCopy, nv, ni, nv * 2, ni * 2,
						-0.25, 0, -0.25)
						
	sierpinskiTranslate(verts, indices, vCopy, nv, ni, nv * 3, ni * 3,
						0.25, 0, 0.25)
						
	sierpinskiTranslate(verts, indices, vCopy, nv, ni, nv * 4, ni * 4,
						0.25, 0, -0.25)
end--]]

dx = -1
dz = 7

x = -6 - dx * 5
z = 5 - dz * 5

pyramidTransform = Transform:New():RotateY(math.rad(5)):Scale(5,5,5)

sierpinskiGasketSquare(pyramid, pyramidIndices, 1)
sierpinskiGasketSquare(pyramid, pyramidIndices, 1)

for i = 1,6 do
sierpinskiGasketSquare(pyramid, pyramidIndices, 1)
Scene:AddChildren(SceneNode:AnimatedNode(
	SceneNode:GeometricNode(Shape:TriangleMesh(Transform:New(Transform:New():Translate(x,0,z), pyramidTransform), pyramid, pyramidIndices), Material:Metal(1, 1, 0.005))
))
x = x + dx;
z = z + dz;
end


Scene:AddChildren(SceneNode:AnimatedNode
({
	SceneNode:AnimatedNode(
		SceneNode:InfiniteAreaLight(TextureInfo:New("textures/envmaps/Helipad.exr"))
	):SetTransform(Transform:New():RotateY(math.rad(-90)):RotateX(math.rad(130))),--]]
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh
		(
			Transform:New():Translate(0,0,0):Scale(500,0,500),
		{
			-0.5, 0.0, 0.5,
			0.5, 0.0, 0.5,
			0.5, 0.0, -0.5,
			-0.5, 0.0, -0.5
		},
		{
			0, 1, 2,
			0, 2, 3
		}),
		Material:Matte(1)
	)
}))