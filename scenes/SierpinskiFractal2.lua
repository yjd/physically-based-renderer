Film:Image(1920, 1080)

Camera:Perspective(60,0,0,1.8,0.005)
--Camera:SetTransform(Transform:New():LookAt({10,2,5},{0,2,0},{0,1,0}))
Camera:SetTransform(Transform:New():LookAt({-0.7,1.5,1},{0.75,0,0.25},{1,0.75,0}))

pyramid = 
{
	-0.5, 0, 0.5,
	0.5, 0, 0.5,
	0.5, 0, -0.5,
	-0.5, 0, -0.5,
	0, 1, 0
}
			
pyramidIndices =
{
	0, 1, 2,
	0, 2, 3,
	0, 1, 4,
	1, 2, 4,
	2, 3, 4,
	0, 3, 4
}
Integrator:BidirectionalPath(3,3)
function sierpinskiTranslate(verts, indices, vCopy, nv, ni, startV, startI, x, y, z)
	for i=1,nv,3 do
		verts[startV + i]		= vCopy[i] +		x;
		verts[startV + i + 1] 	= vCopy[i + 1] +	y;
		verts[startV + i + 2] 	= vCopy[i + 2] +	z;
	end
	if(startI == 0) then
		return
	end
		
	for i=1,ni do
		indices[startI + i] = indices[i] + startV / 3
	end
end

--This applies a fractal iteration ONCE to a set of vertices.  Square base.  Assumes the center of the base is the origin.
--Note that although vertices can be generated using transforms and shape nodes, new shape nodes will be created each time, which will slow down the deletion process when the scene closes.
function sierpinskiGasketSquare(verts, indices, height)
	local nv = #verts
	local ni = #indices
	local vCopy = {}
	for i,v in pairs(verts) do
		vCopy[i] = v
	end
	
	for i,v in pairs(vCopy) do
		vCopy[i] = v * 0.5
	end
	
	sierpinskiTranslate(verts, indices, vCopy, nv, ni, 0, 0, 
						0, height * 0.5, 0)
	
	sierpinskiTranslate(verts, indices, vCopy, nv, ni, nv, ni,
						-0.25, 0, 0.25)			
						
	sierpinskiTranslate(verts, indices, vCopy, nv, ni, nv * 2, ni * 2,
						-0.25, 0, -0.25)
						
	sierpinskiTranslate(verts, indices, vCopy, nv, ni, nv * 3, ni * 3,
						0.25, 0, 0.25)
						
	sierpinskiTranslate(verts, indices, vCopy, nv, ni, nv * 4, ni * 4,
						0.25, 0, -0.25)
end--]]

for i = 1,9 do
sierpinskiGasketSquare(pyramid, pyramidIndices, 1)
end

pyramidTransform = Transform:New():Scale(2.5,2.5,2.5)
pyramidShape = Shape:TriangleMesh(pyramidTransform, pyramid, pyramidIndices)
silvery = Material:Metal(1,1.2,0.001)
goldy = Material:Plastic({0.4,0.12,0.1},{1,0.6,0.15},0.0025)
Scene:AddChildren(SceneNode:AnimatedNode
({
	SceneNode:AnimatedNode(
		SceneNode:GeometricNode(pyramidShape, goldy)
	):SetTransform(Transform:New():Translate(1.25,0,1.25)),
	SceneNode:AnimatedNode(
		SceneNode:GeometricNode(pyramidShape, silvery)
	):SetTransform(Transform:New():Translate(1.25,0,-1.25)),
	SceneNode:AnimatedNode(
		SceneNode:GeometricNode(pyramidShape, silvery)
	):SetTransform(Transform:New():Translate(-1.25,0,1.25)),
	SceneNode:AnimatedNode(
		SceneNode:GeometricNode(pyramidShape, goldy)
	):SetTransform(Transform:New():Translate(-1.25,0,-1.25)),
	SceneNode:AnimatedNode(
		SceneNode:InfiniteAreaLight(TextureInfo:New("textures/envmaps/gcanyon.exr",1.5,0.5),2000)
	):SetTransform(Transform:New():RotateY(math.rad(45)):RotateX(math.rad(-200)))--[[,
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh
		(
			Transform:New():Translate(0,0,0):Scale(7.5,0,7.5),
		{
			-0.5, 0.0, 0.5,
			0.5, 0.0, 0.5,
			0.5, 0.0, -0.5,
			-0.5, 0.0, -0.5
		},
		{
			0, 1, 2,
			0, 2, 3
		}),
		Material:Matte(1)
	)--]]
}))