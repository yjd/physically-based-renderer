Film:Image(1920, 1080)

Camera:Perspective(40)
Camera:SetTransform(Transform:New():LookAt({4,8,5},{-0.5,0,-0.5},{0,1,0}))
			
require("geometry/dragon")

Scene:AddChildren(SceneNode:AnimatedNode
({
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh
		(
			Transform:New():Scale(300,1,300),
		{
			-0.5, 0.0, 0.5,
			0.5, 0.0, 0.5,
			0.5, 0.0, -0.5,
			-0.5, 0.0, -0.5
		},
		{
			0, 1, 2,
			0, 2, 3
		}),
		Material:Matte(0.7)
	),
	SceneNode:AnimatedNode(
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh(Transform:New():Translate(0,-1.2,1):Scale(20,20,20), dragonPts, dragonVert), Material:Metal({Color.RGB, {1.2, 0.6, 0.2}}, 1, 0.01)
	)):SetAccelerator(Accelerator.BINNEDKDTREE),
	SceneNode:AnimatedNode(
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh(Transform:New():Translate(0,-1.2,-1):RotateY(math.rad(180)):Scale(20,20,20), dragonPts, dragonVert), Material:Metal(0.8, 1, 0.01)
	)):SetAccelerator(Accelerator.BINNEDKDTREE),
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh
		(
			Transform:New():Translate(4, 10, 4):Scale(5, 1, 5),
		{
			-0.5, 0.0, 0.5,
			0.5, 0.0, 0.5,
			0.5, 0.0, -0.5,
			-0.5, 0.0, -0.5
		},
		{
			0, 2, 1,
			0, 3, 2
		}),
		AreaLight:Diffuse(15)
	)--]]
}))