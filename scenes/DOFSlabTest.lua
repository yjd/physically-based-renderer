Film:Image(512, 512)

Camera:Perspective(60, 0, 0, 5.5, 1)
Camera:SetTransform(Transform:New():LookAt({0,0,4},{0,0,0},{0,1,0}))
Integrator:Path()
zCurrent = 0;
zIncrement = -0.2
xCurrent = -2
slabs = {}
for i = 1,20 do
	local color
	if -zCurrent < 1 then
		color = {1 + zCurrent, -zCurrent, 0}
	elseif -zCurrent < 2 then
		color = {0, 2 + zCurrent, -1 - zCurrent}
	else
		color = {1, 1, 1}
	end
	slabs[i] = SceneNode:GeometricNode
	(
		Shape:TriangleMesh
		(
			Transform:New():Translate(xCurrent,0,zCurrent):Scale(1,1,0.1),
			{
				-0.5, -0.5, 0.5,
				0.5, -0.5, 0.5,
				-0.5, 0.5, 0.5,
				0.5, 0.5, 0.5,
				
				-0.5, -0.5, -0.5,
				0.5, -0.5, -0.5,
				-0.5, 0.5, -0.5,
				0.5, 0.5, -0.5
			},
			{
				2, 3, 7, 2, 7, 6, --top
				4, 5, 7, 4, 7, 6, --back
				0, 1, 3, 0, 3, 2, --front
				0, 1, 5, 0, 5, 4, --bottom
				0, 4, 6, 0, 6, 2, --left
				1, 5, 7, 1, 7, 3, --Right
			}
		),
		Material:Matte(color)
	)
	zCurrent = zCurrent + zIncrement
	xCurrent = -2 + i * 4 / 20
end
Scene:AddChildren(slabs)
Scene:AddChildren(SceneNode:AnimatedNode
({
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh
		(
			Transform:New():Translate(0,0,-2.5):Scale(5,5,10),
			{
				-0.5, -0.5, 0.5,
				0.5, -0.5, 0.5,
				-0.5, 0.5, 0.5,
				0.5, 0.5, 0.5,
				
				-0.5, -0.5, -0.5,
				0.5, -0.5, -0.5,
				-0.5, 0.5, -0.5,
				0.5, 0.5, -0.5
			},
			{
				2, 3, 7, 2, 7, 6, --top
				4, 5, 7, 4, 7, 6, --back
				0, 1, 5, 0, 5, 4, --bottom
				0, 4, 6, 0, 6, 2, --left
				1, 5, 7, 1, 7, 3  --Right
			}
		),
		Material:Matte(1)
	),
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh
		(
			Transform:New():Translate(0, 2.49, 0),
			{
				-0.5, 0.0, 0.5,
				0.5, 0.0, 0.5,
				0.5, 0.0, -0.5,
				-0.5, 0.0, -0.5
			},
			{
				0, 2, 1,
				0, 3, 2
			}
		),
		AreaLight:Diffuse(10)
	)
}))